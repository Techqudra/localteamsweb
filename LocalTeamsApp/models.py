from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
import datetime

#Masters
class Roles(models.Model):
	""" Type of users """
	role = models.CharField(max_length=255,blank=True, null=True)
	class Meta:
		verbose_name_plural = 'User Role / Profile'
	def __unicode__(self):
		return str(self.role)

class BackImages(models.Model):
	""" Background Images """
	image_name = models.CharField(max_length=255,blank=True, null=True)
	image_url = models.CharField(max_length=255,blank=True, null=True)
	rel_url = models.CharField(max_length=255,blank=True, null=True)
	locale = models.CharField(max_length=255,default='DEFAULT')
	hide = models.BooleanField(default=False)
	
	def image_tag(self):
		''' Load images on Django admin panel '''
		return mark_safe('<img src="%s" width="150" height="150" />' % (self.image_url))
	
	image_tag.short_description = 'Image'
	
	class Meta:
		verbose_name_plural = 'BackGround Images'
	def __unicode__(self):
		return str(self.image_name)

class AgeGroups(models.Model):
	age_group = models.CharField(max_length=30)
	description = models.CharField(max_length = 100, blank=True, null=True)
	class Meta:
		verbose_name_plural = 'Age Group'
	def __unicode__(self):
		return str(self.age_group)

class Status(models.Model):
	status = models.CharField(max_length = 25, blank = True, null = True)
	class Meta:
		verbose_name_plural = 'status'
	def __unicode__(self):
		return str(self.status)

class Country(models.Model):
	iso = models.CharField(max_length=4, blank=True, null=True, unique=True)
	country_name = models.CharField(max_length=200, null=True)
	class Meta:
		verbose_name_plural = 'Country'
	def __unicode__(self):
		return str(self.country_name)

class County(models.Model):
	county_name = models.CharField(max_length=200, null=True)
	country_name = models.ForeignKey(Country, null=True)  
	class Meta:
		verbose_name_plural = 'County'
	def __unicode__(self):
		return str(self.county_name)

class City(models.Model):
	city_name = models.CharField(max_length=100 , null=False)
	county_name = models.ForeignKey(County, null=True)
	country_name = models.ForeignKey(Country, null=True)
	class Meta:
		verbose_name_plural = 'City'
	def __str__(self):
		return self.city_name
		# return type(self.city_name)

class FootballPlayerPositions(models.Model):
	pos_abbv = models.CharField(max_length=3, null=True, blank=True)
	position = models.CharField(max_length=200, null=True)
	class Meta:
		verbose_name_plural = 'Football Players Positions'
	def __unicode__(self):
		return str(self.position)

class Gender(models.Model):
	gen_abbv = models.CharField(max_length=3, null=True, blank=True)
	gender = models.CharField(max_length=15, null=True)
	class Meta:
		verbose_name_plural = 'Gender'
	def __unicode__(self):
		return str(self.gender)

class PostalCodes(models.Model):
	postcode = models.CharField(max_length=10)
	district = models.CharField(max_length=5)
	postal_town = models.CharField(max_length=100, null=True)
	county = models.CharField(max_length=100, null=True)
	country = models.CharField(max_length=100, null=True)
	grid_reference = models.CharField(max_length=20, null=True)
	easting = models.CharField(max_length=15, null=True)
	northing = models.CharField(max_length=15, null=True)
	latitude = models.CharField(max_length=20, null=True)
	longitude = models.CharField(max_length=20, null=True)
	local_government_area = models.CharField(max_length=100, null=True)
	region = models.CharField(max_length=50, null=True)
	country_id = models.ForeignKey(Country, null=True)
	county_id = models.ForeignKey(County, null=True)
	city_id = models.ForeignKey(City, null=True)
	type = models.CharField(max_length=100, null=True)
	class Meta:
		verbose_name_plural = 'Postal Codes'
	def __unicode__(self):
		return str(self.postcode)


#Others
class Kits(models.Model):
	shirt_colour = models.CharField(max_length=100, null=True, blank=True)
	sleeve_colour = models.CharField(max_length=100, null=True, blank=True)
	pattern_colour = models.CharField(max_length=100, default='#ffffff')
	pattern_mode = models.CharField(max_length=50,null=True)###
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )
  	class Meta:
		verbose_name_plural = 'Kits'
	def __unicode__(self):
		return str(self.pattern_mode)

class Grounds(models.Model):
	ground_name = models.CharField(max_length=250, null=True)
	street = models.CharField(max_length=250, null=True, blank=True)
	town = models.ForeignKey(City, null=True, on_delete=models.CASCADE)
	county = models.ForeignKey(County, null=True, on_delete=models.CASCADE)
	postcode = models.ForeignKey(PostalCodes, null=True, on_delete=models.CASCADE)
	phone = models.CharField(max_length=250, null=True, blank=True)
	map_url = models.CharField(max_length=500, null=True, blank=True)
	created_by = models.IntegerField(null=True, blank=True, default = None )
	created_at = models.CharField(max_length = 40,null=True, blank=True, default = None )
	updated_by = models.IntegerField(null=True, blank=True, default = None )
	updated_at = models.CharField(max_length = 40,null=True, blank=True, default = None )
	class Meta:
		verbose_name_plural = 'Grounds'
	def __unicode__(self):
		return (self.ground_name)

class Users(models.Model):
	ip_address = models.CharField(max_length=15, null=True)
	username = models.CharField(max_length=254, null=True)
	password = models.CharField(max_length=255, null=True)
	mobile_no = models.CharField(max_length=15, null=True, blank=True)
	salt = models.CharField(max_length=255, null=True, blank=True)
	email = models.CharField(max_length=254, null=True)
	activation_code = models.CharField(max_length=40, null=True, blank=True)
	forgotten_password_code = models.CharField(max_length=40, null=True, blank=True)
	forgotten_password_time = models.IntegerField(null=True, blank=True)
	remember_code = models.CharField(max_length=40, null=True, blank=True)
	created_on = models.DateField(null=True)
	last_login = models.DateField(null=True)
	active = models.BooleanField(default=False)
	first_name = models.CharField(max_length=50, null=True, blank=True)
	last_name = models.CharField(max_length=50, null=True, blank=True)
	country = models.ForeignKey(Country, null=True, on_delete=models.CASCADE)
	usertype = models.ForeignKey(Roles, null=True, on_delete=models.CASCADE)
	browser_token = models.CharField(max_length=254, null=True) #new
	source_device = models.CharField(max_length = 10,null=True,default = 'W' )	# A-android|i-iOS|W-website
	email_verified = models.BooleanField(default=False)
	update_notifications = models.BooleanField(default=False)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )
	class Meta:
		verbose_name_plural = 'Users'
	def __unicode__(self):
		return str(self.username)

class Clubs(models.Model):
	club_name = models.CharField(max_length=250)
	strip_colour = models.CharField(max_length=50,null=True,blank=True)
	logo_photo = models.FileField(blank=True, upload_to='club_logos', null=True)
	established = models.CharField(max_length=4, null=True)
	gender = models.ForeignKey(Gender, null=True, on_delete=models.CASCADE)
	num_teams = models.IntegerField(null=True, blank=True)
	league_table_url = models.CharField(max_length=250, null=True, blank=True)
	fixture_table_url = models.CharField(max_length=250, null=True, blank=True)
	website_url = models.CharField(max_length=250, null=True, blank=True)
	cover_photo_url = models.FileField(blank=True, upload_to='club_covers', null=True)
	ground = models.ForeignKey(Grounds, null=True, blank=True, on_delete=models.CASCADE)
	training_text = models.TextField(null=True, blank=True)
	history_text = models.TextField(null=True, blank=True)
	home_kit = models.ForeignKey(Kits, null=True,blank=True,related_name="home_kit", on_delete=models.CASCADE)
	away_kit = models.ForeignKey(Kits, null=True,blank=True,related_name="away_kit", on_delete=models.CASCADE)
	team_photo = models.FileField(blank=True, upload_to='club_team', null=True)
	age_groups = models.TextField(null=True)###########
	users = models.ForeignKey(Users, on_delete=models.CASCADE)
	country_name = models.ForeignKey(Country, null=True, on_delete=models.CASCADE)
	city_name = models.ForeignKey(City, null=True, on_delete=models.CASCADE)
	county_name = models.ForeignKey(County, null=True, on_delete=models.CASCADE)
	latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
	longitude = models.DecimalField(max_digits=10,decimal_places=6, null=True)
	set_background = models.BooleanField(default=False)
	created_by = models.IntegerField(null=True,default = None,blank=True)
	created_at = models.CharField(max_length = 40,null=True,default = None,blank=True)
	updated_by = models.IntegerField(null=True,default = None,blank=True)
	updated_at = models.CharField(max_length = 40,null=True,default = None,blank=True)

	class Meta:
		verbose_name_plural = 'Clubs'
	def __unicode__(self):
		return str(self.club_name)

class Fixtures(models.Model):
	club = models.ForeignKey(Clubs, null =True, on_delete=models.CASCADE)
	fixture = models.CharField(max_length = 200,null = True)
	fixture_date = models.DateField(null = True)
	fixture_time = models.TimeField(null = True)
	fixture_venue = models.TextField(default='', null=True)
	fixture_visibility = models.BooleanField(default=True)
	fixture_confirm = models.BooleanField(default=False)
	resend = models.BooleanField(default= False)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None)
	resend_at = models.CharField(max_length = 40,null=True,default = None)
	created_by_m = models.IntegerField(null=True,default = None )
	class Meta:
		verbose_name_plural = 'Fixtures'
	def __unicode__(self):
		return str(self.club)

class ClubManager(models.Model):
	manager_name = models.CharField(max_length = 100,null= True, blank = True)
	manager_mob_no = models.CharField(max_length = 20, null = True)
	manager_img = models.FileField(upload_to ='pictures',null = True)
	is_active = models.IntegerField(null=True, default=1)
	created_by = models.CharField(max_length = 50,null = True)
	created_at = models.CharField(max_length = 50,null = True)
	updated_by = models.CharField(max_length = 50,null = True)
	updated_at = models.CharField(max_length = 50,null = True)
	class Meta:
		verbose_name_plural = "Club Manager"
	def __unicode__(self):
		return str(self.manager_name)

class ClubTeams(models.Model):
	team_name = models.CharField(max_length = 100, null = True, blank = True)
	club = models.ForeignKey(Clubs, null=True, on_delete=models.CASCADE)
	agegroup = models.ForeignKey(AgeGroups,null=True, on_delete=models.CASCADE)
	gender = models.CharField(max_length = 15, null=True)
	suffix = models.CharField(max_length = 5, null=True)
	club_manager = models.ForeignKey(ClubManager, null = True, on_delete=models.CASCADE)
	is_active = models.IntegerField(null=True, default=1)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )

	class Meta:
		verbose_name_plural = "Clubs Teams"
	def __unicode__(self):
		return str(self.team_name)

class Players(models.Model):
	player_name = models.CharField(max_length = 100,null= True, blank = True)
	player_position = models.ForeignKey(FootballPlayerPositions,null = True, blank=True, on_delete=models.CASCADE)
	player_mob_no = models.CharField(max_length = 20, null = True)
	player_img = models.FileField(upload_to ='pictures',null = True)
	club_team = models.ForeignKey(ClubTeams,null = True, on_delete=models.CASCADE)
	is_active = models.IntegerField(null = True, default=1)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )

	class Meta:
		verbose_name_plural = "Players"
	# def __unicode__(self):
		# return str(self.player_name)
	def __str__(self):
		return self.player_name

class UsersToken(models.Model):
	device_token = models.CharField(max_length = 254,null = True, blank = True)
	usertype = models.CharField(max_length = 2, null = True)
	users = models.ForeignKey(Users,null=True,blank=True, on_delete=models.CASCADE)
	player = models.ForeignKey(Players,null=True,blank=True, on_delete=models.CASCADE)
	manager = models.ForeignKey(ClubManager,null = True,blank=True, on_delete=models.CASCADE)
	source_device = models.CharField(max_length=10,null=True,blank=True)
	app_device_token = models.CharField(max_length = 20,null = True, blank = True)
	is_notification_block = models.IntegerField(null=True, default=0)
	notification_tone = models.CharField(max_length = 254, null = True, blank = True)
	badge_count = models.IntegerField(null=True, default=0)
	av_count = models.IntegerField(null=True, default=0)
	rv_count = models.IntegerField(null=True, default=0)
	# created_at = models.CharField(max_length = 50,null = True)
	# updated_at = models.CharField(max_length = 50,null = True)
	# created_by = models.CharField(max_length = 50,null = True)
	# updated_by = models.CharField(max_length = 50,null = True)
	class Meta:
		verbose_name_plural = 'UsersToken'
	def __unicode__(self):
		return str(self.usertype)


class Squad(models.Model):
	club_team = models.ForeignKey(ClubTeams, null = True, on_delete=models.CASCADE)
	fixture = models.ForeignKey(Fixtures, null = True, on_delete=models.CASCADE)
	player = models.ForeignKey(Players, null = True, on_delete=models.CASCADE)
	status = models.ForeignKey(Status, blank=True, null=True,default= 3, on_delete=models.CASCADE)
	resend = models.BooleanField(default= False)
	notification = models.BooleanField(default= False)
	squad_confirm = models.BooleanField(default=False)
	hide_fix = models.BooleanField(default=False)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )
	class Meta:
		verbose_name_plural = "Squad"
	def __unicode__(self):
		return str(self.fixture)

class StreetData(models.Model):
	postcode = models.CharField(max_length = 15,null = True)
	organisation_name = models.TextField(default = "")
	department_name = models.TextField(default = "")
	line_1 = models.TextField(default = "")
	line_2 = models.TextField(default = "")
	postal_county = models.TextField(default = "")
	traditional_county = models.TextField(default = "")
	town = models.TextField(default = "")
	street_name = models.TextField(default = "")

#----- Web Notifications -----#
class Notifications(models.Model):
	users = models.ForeignKey(Users,null=True,blank=True, on_delete=models.CASCADE)
	fixture = models.ForeignKey(Fixtures, null = True, on_delete=models.CASCADE)
	player = models.ForeignKey(Players,null=True,blank=True, on_delete=models.CASCADE)
	# click_action = models.TextField(default = "")
	player_status = models.CharField(max_length = 30, null = True, default = None)
	read = models.BooleanField(default=0)
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )
	is_visible = models.BooleanField(default=1)
#----- Web Notifications -----#


class GroupInfo(models.Model):
	group_url = models.CharField(max_length = 255, null = True, default = None) # channel_url
	group_name = models.CharField(max_length = 255, null = True, default = None)
	created_by = models.IntegerField(null=True,default = None )
	created_at = models.CharField(max_length = 40,null=True,default = None )
	updated_by = models.IntegerField(null=True,default = None )
	updated_at = models.CharField(max_length = 40,null=True,default = None )
	clubteam = models.ForeignKey(ClubTeams,null = True, blank = True, on_delete=models.CASCADE)
	fixture = models.ForeignKey(Fixtures,null = True, blank = True, on_delete=models.CASCADE)
	class Meta:
		verbose_name_plural = "Group Information"
	def __unicode__(self):
		return str(self.group_name)
		
class GroupMembers(models.Model):
	group = models.ForeignKey(GroupInfo, null = True)
	member_name = models.CharField(max_length = 255, null=True, default=None, blank=True)
	member_id = models.IntegerField(null = True,default = None)
	group_type = models.IntegerField(null = True,default = None) # 1 - clubGroup | 2 - TeamGroup | 3 - FixtureGroup
	is_admin = models.BooleanField(default=False)
	added_by = models.IntegerField(null=True,default = None )
	added_at = models.CharField(max_length = 40,null=True,default = None )
	class Meta:
		verbose_name_plural = "Group Members"
	def __unicode__(self):
		return str(self.member_name)

'''
class MessageData(models.Model):
	msg_text = models.TextField(default = "")
	msg_from = models.IntegerField(null = True,default=None)
	msg_from_type = models.IntegerField(null = True,default=None) # 1 - clubAdmin | 2 - manager | 3 - player
	msg_to = models.IntegerField(null = True,default=None)
	msg_to_type = models.IntegerField(null = True,default=None) # 1 - clubAdmin | 2 - manager | 3 - player
	msg_is_in = models.IntegerField(null = True, default=None) # 1 - personal | 2 - Group
	msg_status = models.IntegerField(null = True, default=None) # 1 - unread | 2 - read
	msg_date_time = models.CharField(max_length = 100,null = True, default=None)
'''