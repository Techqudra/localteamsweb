from django.conf.urls import url, include
from . import views
from . import views_web
from LocalTeamsApp import views
# from . import adminviews
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.views.decorators.csrf import csrf_protect, csrf_exempt
admin.autodiscover()

urlpatterns = [
	url(r'^$', views.index),
	url(r'^admin/', admin.site.urls),
	url(r'^index/', views.index),
	url(r'^signUpUser/', views.signUpUser),
	url(r'^login/', views.login),
	url(r'^firebase-messaging-sw.js', views.firebase_messaging_sw_js),
	url(r'^postcodeAutofilled/', views.postcodeAutofilled),
	url(r'^is_country/', views.validCountry, name='validCountry'),
	url(r'^is_postcode/', views.validPostcode, name='validPostcode'),
	url(r'^email_avail/', views.emailAvail, name='emailAvail'),
	url(r'^user_avail/', views.userAvail, name='userAvail'),
	url(r'^countryData/', views.autocompleteCountry, name='autocompleteCountry'),
	url(r'^postcodes/', views.autocompletePostCodes, name='autocompletePostCodes'),
	url(r'^ageData/', views.autocompleteAge, name='autocompleteAge'),
	url(r'^autoCity/', views.autocompleteCity, name='autocompleteCity'),
	url(r'^autoStreet/', views.autocompleteStreet, name='autocompleteStreet'),
	url(r'^loginMobile/', views.loginMobile, name='loginMobile'),
	url(r'^clubProfile/', views.clubProfile, name='clubProfile'),
	url(r'^clubReg/', views.club_registration, name='club_registration'),
	url(r'^clubNotif/$', views.clubNotif, name='clubNotif'),
	url(r'^clubNotif/(?P<pk>[0-9]+)$', views.clubNotif, name='clubNotif'),
	url(r'^clubFix/', views.clubFix, name='clubFix'),
	url(r'^clubSquad/', views.clubSquad, name='clubSquad'),
	url(r'^clubRegistration/', views.clubRegistration, name='clubRegistration'),
	url(r'^clubEdit/', views.clubEdit, name='clubEdit'),
	url(r'^clubSave/', views.clubSave, name='clubSave'),
	url(r'^createFix/', views.createFix, name='createFix'),
	url(r'^base/', views.base, name='base'),
	url(r'^updatemanager/', views.updatemanager, name='updatemanager'),
	url(r'^deletemanager/', views.deletemanager, name='deletemanager'),
	url(r'^XmobileP/', views.XmobileP, name='XmobileP'),
	url(r'^squadForFixture/', views.squadForFixture),
	url(r'^updateUserToken/', views.updateUserToken, name='updateUserToken'),
	url(r'^push_notification_web/', views.push_notification_web, name='push_notification_web'),
	url(r'^XmobileM/', views.XmobileM, name='XmobileM'),
	url(r'^eXManSave/', views.eXManSave, name='eXManSave'),
	url(r'^eXAdminM/', views.eXAdminM, name='eXAdminM'),
	url(r'^notificationHide/', views.notificationHide, name='notificationHide'),
	url(r'^resend_msg/', views.resend_msg, name='resend_msg'),
	url(r'^FinalSquad/', views.finalSquad, name='finalSquad'),
	url(r'^matchDaySqd/', views.matchDaySqd, name='matchDaySqd'),
	url(r'^notificationsList/', views.notifications_list, name='notifications_list'),
	url(r'^notificationsRead/', views.notifications_read, name='notifications_read'),
	url(r'^Test/', views.Test, name='Test'),
	url(r'^emailV/', views.emailV, name='emailV'),
	url(r'^verifyEmail/', views.verifyEmail, name='verifyEmail'),
	url(r'^resendEmail/', views.resendEmail, name='resendEmail'),
	url(r'^send_emails/', views.send_emails, name='send_emails'),
	
	
	url(r'^reset_pass_mail/', views.reset_pass_mail, name='reset_pass_mail'),
	url(r'^reset_pass/(?P<q>[A-Za-z0-9]+$)', views.reset_pass, name='reset_pass'),
	url(r'^setPass/', views.setPass, name='setPass'),
	
	url(r'^contact_us_mail/', views.contact_us_mail, name='contact_us_mail'),
	
	# On Website
	url(r'^about_us', views.about_us, name='about_us'),
	url(r'^contact_us/', views.contact_us, name='contact_us'),
	url(r'^privacy_policy', views.privacy_policy, name='privacy_policy'),
	url(r'^cookie_policy', views.cookie_policy, name='cookie_policy'),
	url(r'^terms_and_conditions', views.terms_and_conditions, name='terms_and_conditions'),
	# For Mobile
	url(r'^Privacy_policy', views.Privacy_policy, name='Privacy_policy'),
	url(r'^Cookie_policy', views.Cookie_policy, name='Cookie_policy'),
	url(r'^Terms_and_conditions', views.Terms_and_conditions, name='Terms_and_conditions'),
	url(r'^template', views.template, name='template'),
	
		
	#'''Dhananjay Start'''
	url(r'^distance_search/', views.distance_search),
	url(r'^advanceSearch/', views.searchAdvance),
	url(r'^adv_search/', views.AdvancedSearch),
	url(r'^getplayers/', views.getplayers),
	url(r'^addmanager/', views.addmanager), 
	url(r'^addplayer/', views.addplayer), 
	url(r'^deleteplayer/', views.deleteplayer),
	url(r'^updateplayer/', views.updateplayer),
	url(r'^popcountry/', views.popcountry),
	url(r'^getsquad/', views.getsquad),
	url(r'^showmap/', views.showmap),
	url(r'^logout/', views.logout),
	url(r'^viewProfile/(?P<club_id>\d+)/$', views.viewProfile, name='viewProfile'),
	#''' Dhananjay end '''
	url(r'^getudata/',views.get_user_data),

	#---------uttara_web---------#
	url(r'^country/', views_web.country_web),
	url(r'^mobileValidation/', views_web.mobileValidation_web),
	url(r'^notification/', views_web.notification_web),
	url(r'^playerResponseClub/', views_web.playerResponseClub_web),
	url(r'^resendMsg/', views_web.resendMsg_web),
	url(r'^playerNotification/', views_web.playerNotification_web),
	url(r'^fixDetails/', views_web.fixDetails_web),
	url(r'^finalPlayersList/', views_web.get_final_players_web),
	url(r'^notificationBlock/', views_web.notificationBlock_web),
	url(r'^fixtureCreate/', views_web.fixtureCreate_web),
	url(r'^playerResponse/', views_web.playerResponse_web),
	url(r'^clubTeams/', views_web.clubTeams_web),
	url(r'^userClubs/', views_web.userClubs_web),
	url(r'^teamPlayers/', views_web.teamPlayers_web),
	url(r'^clubTeamsManager/', views_web.clubTeamsManager_web),
	url(r'^deleteClubTeams/', views_web.deleteClubTeams_web),
	url(r'^fixtureDelete/', views_web.fixtureDelete_web),
	url(r'^addPlayer/', views_web.addPlayer_web),
	url(r'^updatePlayer/', views_web.updatePlayer_web),
	url(r'^deleteTeamPlayer/', views_web.deleteTeamPlayer_web),
	url(r'^teamPlayerSquad/', views_web.teamPlayerSquad_web),
	url(r'^playerPossition/', views_web.playerPossition_web),
	url(r'^push_notification_ios/', views_web.push_notification_ios_web),
	url(r'^LogOut/', views_web.logOut_web),
	url(r'^notificationTone/', views_web.notificationTone_web),
	url(r'^resetBadgecount/', views_web.reset_badge_count_web),
	url(r'^availablityHide/', views_web.availablityHide_web),
	url(r'^finalSquad/', views_web.final_squad_web),
	url(r'^resetDeviceToken/', views_web.reset_device_token_web),
	#---------end----------------#
	
	url(r'^defaultsite/', views.defaultsite),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) #initial