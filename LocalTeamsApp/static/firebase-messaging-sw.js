importScripts('https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js');


var config = {
	apiKey: "AIzaSyDGm3GWAgQz7UNxlbVACpSkTBvwYjlGl1E",
	authDomain: "local-teams89.firebaseapp.com",
	databaseURL: "https://local-teams89.firebaseio.com",
	projectId: "local-teams89",
	storageBucket: "local-teams89.appspot.com",
	messagingSenderId: "89668080115"
};
/*var config = {
	apiKey: "AIzaSyBYLibZsxS0SU2nioDWey7ZCseXmuZmtsk",
	authDomain: "localteams-10d87.firebaseapp.com",
	databaseURL: "https://localteams-10d87.firebaseio.com",
	projectId: "localteams-10d87",
	storageBucket: "localteams-10d87.appspot.com",
	messagingSenderId: "717002291244"
};*/
/*
var config = {
	apiKey: "AIzaSyB0n-7WoJ5up7K9oiDh29MlmAvmNoRvlN0",
	authDomain: "localteams-10d87.firebaseapp.com",
	databaseURL: "https://localteams-10d87.firebaseio.com",
	projectId: "localteams-10d87",
	storageBucket: "localteams-10d87.appspot.com",
	messagingSenderId: "717002291244"
};
*/
firebase.initializeApp(config);

const messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler
messaging.setBackgroundMessageHandler(function(payload) {
	console.log('[firebase-messaging-sw.js] Received localteams message ', payload);
	const notificationTitle = 'LocalTeams';
	const notificationOptions = {
		body: 'LocalTeams Message body.',
		icon: payload.data.icon
	};
	return self.registration.showNotification(notificationTitle,notificationOptions);
});
// END background_handler]
