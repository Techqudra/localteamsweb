$(document).ready(function(){
	var d = new Date();
	var n = d.getFullYear();
	document.getElementById("demo").innerHTML = n;
		// document.getElementById("demo1").innerHTML = n;
	
	// $('.selectpicker').selectpicker();	
	$("#email").click(function(){
		$("#o-fld,#o-fld1").slideDown("slow");
	});
	$("#forget_pass").click(function(){
		$("#forget_div").toggle("slow");
	});	
	
	/* Sectrum color for header*/	
	
	if($('#header-colorpicker').length) {
		$('#header-colorpicker').spectrum({
			preferredFormat: "hex",
			showInput: true,
			showPalette: true,
			palette: [ ],
			localStorageKey: "spectrum.ks",
			maxSelectionSize: 3,
			clickoutFiresChange: true,
			allowEmpty: true,
			containerClassName: "header-colorpicker-cp",
			change: function(color) {	
			
				if(!color) {
					color = new tinycolor("rgb(147,148,150)");
				}
				
				$('#club-reg-strip').css('background-color', color.toHexString());
				$('#header-color').val(color.toHexString());
				
			}
		});
	}	
	/* End	*/

});
	
	/*** ***/
	$('.icon').click(function () {
		if($('#password1').attr('type') == "text"){
			$('#password1').attr('type', 'password');
			$('#glyph').attr('class', 'glyphicon glyphicon-eye-close o-fld');
			
		}
		else if($('#password1').attr('type') == "password"){
			$('#password1').attr('type', 'text');
			$('#glyph').attr('class', 'glyphicon glyphicon-eye-open o-fld');
		}
	});
	$('.iconC').click(function () {
		if($('#password2').attr('type') == "text"){
			$('#password2').attr('type', 'password');
			$('#glyphC').attr('class', 'glyphicon glyphicon-eye-close o-fld');
		}
		else if($('#password2').attr('type') == "password"){
			$('#password2').attr('type', 'text');
			$('#glyphC').attr('class', 'glyphicon glyphicon-eye-open o-fld');
		}
	});
	$('.iconL').click(function () {
		if($('.passwordL').attr('type') == "text"){
			$('.passwordL').attr('type', 'password');
			$('#glyphL').attr('class', 'glyphicon glyphicon-eye-close');
		}
		else if($('.passwordL').attr('type') == "password"){
			$('.passwordL').attr('type', 'text');
			$('#glyphL').attr('class', 'glyphicon glyphicon-eye-open');
		}
	});
	/*** ***/
	
	$('.carousel').carousel({
        interval: 15000 
    });
	
	/*** Club City Autocomplete ***/
	$("#top_search").autocomplete({
		source: '/autoCity/',
	});
	/*** Club City Autocomplete (mobile view) ***/
	$("#top_search_mobile").autocomplete({
		source: '/autoCity/',
	});
	
	var pc = true;
	var emailL = passL = lenBool = false;
	var emailPtrn = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,3}$/;
	// var passPtrn = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!+&$£@*]).{8,20}$/;
	var passPtrn = /^(?=.*\d)(?=.*[A-Z])(?=.*[!+&$£@*]).{8,50}$/;
	$("#inputEmail").focusout(function(){
		if($(this).val() == ''){
			window.emailL = false;
			$("#emailLOK").hide();
			$("#emailLRE").show();
			$("#emailLM").hide();
			$("#emailLMT").show();
		}
		else{
			window.emailL = emailPtrn.test($(this).val());
			len = $(this).val().length;
			if (window.emailL){
				$("#emailLOK").show();
				$("#emailLRE").hide();
				$("#emailLM").hide();
				$("#emailLMT").hide();
			}
			else{
				$("#emailLOK").hide();
				$("#emailLRE").show();
				$("#emailLM").show();
				$("#emailLMT").hide();
			}
			if (len > 254){
				window.lenBool = false;
				$("#emailLMlen").show();
			}
			else{
				window.lenBool = true;
				$("#emailLMlen").hide();
			}
		}
	});
	$("#passwordL").focusout(function(){
		if($(this).val() == ''){
			window.passL = false;
			$("#passLOK").hide();
			$("#passLRE").show();
			$("#passLMT").show();
			$("#passLM").hide();
		}
		else{
			window.passL = passPtrn.test($(this).val());
			if (passL){
				$("#passLOK").show();
				$("#passLRE").hide();
				$("#passLM").hide();
				$("#passLMT").hide();
			}
			else{
				$("#passLOK").hide();
				$("#passLRE").show();
				$("#passLMT").hide();
				$("#passLM").show();
			}
		}
	});
	$("#passwordL").keypress(function (e) {
		var key = e.which;
		if(key == 13){	// the enter key code
			window.passL = passPtrn.test($(this).val());
			if (passL){
				$("#passLM").hide();
				$("#userLEM").hide();
				$("#loginFormBtn").click();
			}
			else{
				$("#passLM").show();
				$("#userLEM").hide();
			}
		}
	});

	$("#loginFormBtn").click(function(){
		if($("#inputEmail").val() == ''){
			window.emailL = false;
			$("#emailLOK").hide();
			$("#emailLRE").show();
			$("#emailLM").hide();
			$("#emailLMT").show();
		}
		else{
			window.emailL = emailPtrn.test($("#inputEmail").val());
			len = $("#inputEmail").val().length;
			if (window.emailL){
				$("#emailLOK").show();
				$("#emailLRE").hide();
				$("#emailLM").hide();
				$("#emailLMT").hide();
			}
			else{
				$("#emailLOK").hide();
				$("#emailLRE").show();
				$("#emailLM").show();
				$("#emailLMT").hide();
			}
			if (len > 254){
				window.lenBool = false;
				$("#emailLMlen").show();
			}
			else{
				window.lenBool = true;
				$("#emailLMlen").hide();
			}
		}
		if($("#passwordL").val() == ''){
			window.passL = false;
			$("#passLOK").hide();
			$("#passLRE").show();
			$("#passLMT").show();
			$("#passLM").hide();
		}
		else{
			window.passL = passPtrn.test($("#passwordL").val());
			if (passL){
				$("#passLOK").show();
				$("#passLRE").hide();
				$("#passLM").hide();
				$("#passLMT").hide();
			}
			else{
				$("#passLOK").hide();
				$("#passLRE").show();
				$("#passLMT").hide();
				$("#passLM").show();
			}
		}
		
		if(window.emailL && window.passL && window.lenBool && true){
			$.ajax({
				type: "POST",
				url: "/user_avail/",
				data:{
						usernameLD:$("#inputEmail").val(),
						passwordLD:$("#passwordL").val(),
					},
				success: function(response) {
					console.log(response);
					if (response.valid){
						$("#userLEM").hide();
						/*email = false;*/
						$("#loginForm").submit();
					}
					else{
						$("#userLEM").show();
						/*email = true;*/
					}
				}
			});
		}
		else{
			if($("#inputEmail").val() == ''){
				emailL = false;
				$("#emailLOK").hide();
				$("#emailLRE").show();
				$("#emailLM").hide();
				$("#emailLMT").show();
			}
			else{
				if(window.emailL){
					$("#emailLOK").show();
					$("#emailLRE").hide();
					$("#emailLM").hide();
					$("#emailLMT").hide();
				}
				else{
					$("#emailLOK").hide();
					$("#emailLRE").show();
					$("#emailLM").show();
					$("#emailLMT").hide();
				}
				if ($("#inputEmail").val().length > 254){
					window.lenBool = false;
					$("#emailLMlen").show();
				}
				else{
					window.lenBool = true;
					$("#emailLMlen").hide();
				}
			}
			if($("#passwordL").val() == ''){
				passL = false;
				$("#passLOK").hide();
				$("#passLRE").show();
				$("#passLMT").show();
				$("#passLM").hide();
			}
			else{
				if(window.passL){
					$("#passLM").hide();
					$("#userLEM").hide();
					$("#passLMT").hide();
				}
				else{
					$("#passLM").show();
					$("#userLEM").hide();
					$("#passLMT").hide();
				}
			}
		}
	});
	
	function viewProfile(r){
		// console.log(r);
		// l = parseInt(r).toString();
		if(parseInt(r).toString() == "NaN"){
			if(r != "#"){
				/* window.open(r,'_blank'); */
				window.open(r);
			}
		}
		else{
			/* window.open('/viewProfile/'+parseInt(r),'_blank'); */
			window.open('/viewProfile/'+parseInt(r),'_blank');
		}
	}
	
	$('#top_submit').click(function(){
		document.getElementById("top_form").submit();
	});   
	$('#top_submit_mobile').click(function(){
		$("#top_form_mobile").submit();
	});