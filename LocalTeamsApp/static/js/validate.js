$("#submitSignUpForm").click(function() {
    $('#signUpForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z0-9]{2,3}$/,
                        message: 'Email must be valid'
                    },
					stringLength:{
						max:254
					}
                }
            },
			pass:{
				validators:{
					notEmpty:{
						message:'The Password field is required and cannot be empty'
					},
                    regexp: {
                        regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$#!*&@]).{8,20}$/,
                        message: 'Password Must be 8 to 20 characters containing at least,<br/>&nbsp;&nbsp;One digit - 1,2,3,4...,9,0 ,<br/>&nbsp;&nbsp;One uppercase - A,B,C,...,X,Y,Z ,<br/>&nbsp;&nbsp;One lowercase letter - a,b,c,...,x,y,z ,<br/>&nbsp;&nbsp;One special symbol - $,#,!,*,&,@'
                    }
				}
			},
            passC: {
				validators: {
					identical: {
						field: 'pass',
						message: 'The password and its confirm are not the same'
					}
				}
            },
            usertype: {
                validators: {
                    notEmpty: {
                        message: 'The UserType is required and cannot be empty'
                    }
                }
            }
        }
    });

	// var password1 = $("#password1").val();
	// var password2 = $("#password2").val();
	// if(password1 == '' && password2 == ''){
		// $("#passW").show();
		// $("#passCW").show();
		// $("#passO").hide();
		// $("#passCO").hide();
		// $("#submitSignUpForm").prop('disabled','true');
	// }else{
		// if(password1 == password2){
			// $("#passW").hide();
			// $("#passCW").hide();
			// $("#passW").show();
			// $("#passCW").show();
			// $("#submitSignUpForm").prop('disabled','false');
		// }else{
		// $("#passW").show();
		// $("#passCW").show();
		// $("#passO").hide();
		// $("#passCO").hide();
		// $("#submitSignUpForm").prop('disabled','true');
		// }
	// }
});
$("#country").focusout(function(){debugger;
	$.ajax({
		type: "POST",
		url: "/is_country/",
		data:{
				country:$("#country").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
			},
		success: function(response, jqXHR) {
				// console.log(response);
				if (response.valid){
					$("#countryField").attr('class','form-group has-feedback has-success');
					$("#countryField").append('<i class="form-control-feedback bv-no-label glyphicon glyphicon-ok" data-bv-icon-for="country" style="display: block;"></i>');
					$("#submitSignUpForm").prop('disabled','false');
				}else{
					$("#countryField").attr('class','form-group has-feedback has-error')
					$("#countryField").append('<i class="form-control-feedback bv-no-label glyphicon glyphicon-remove" data-bv-icon-for="country" style="display: block;"></i>');
					$("#submitSignUpForm").prop('disabled','true');
				}
			}
	});
});