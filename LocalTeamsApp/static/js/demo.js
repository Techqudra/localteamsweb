$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var MSG_xsmall = "Lorem ipsum dolor sit amet hears farmer byron.";
var MSG_small = "Lorem ipsum dolor sit amet byron frown tumult minstrel wicked clouded bows columbine full.";
var MSG_medium = "Lorem ipsum dolor sit amet hears farmer indemnity inherent, youngest nestor serene horse, already, ipsum unplanted trace line. Making queries worketh game unplanted trace how erring poles.";
var MSG_large = "Lorem ipsum dolor sit amet byron frown tumult minstrel wicked clouded bows columbine full. Panther nascetur estimation, croaked translations brood sharply federal basket. Yet virtues replies pans croaked org feelest, redden chicadeedee wipe, columbine humanity flood mood. Stayed frown ponderous shares bubbles skilled mood federal, shamed robe roll feathered life. Notifies life bows joys bubbles, clouded frown. Skilled wished sportive moved, shamed, year frown sank, universe, wove within. Infirm dames croaked sharply estimation wipe ponderous climb, shamed once basket oracle, smite frown stayed. Sharply bows basket minstrel skilled virtues, panther life. Dames notifies laid, willow listened frankincense croaked potenti. Minstrel since rowed frown, wipe shares, dames wished heaving potenti estimation panther columbine mighty flood.";
$(function () {
    var IMG_PREFIX = 'demo/img/';
    (function () {
        Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, {
            iconSource: 'fontAwesome'
        });
        Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, {
            iconSource: 'fontAwesome'
        });

        (function () {
            $('#popupYesNoBasic').click(function () {
                Lobibox.confirm({
                    msg: "Are you sure you want to delete this user?"
                });
            });
            $('#popupErrorBasic').click(function () {
                Lobibox.alert('error', {
                    msg: MSG_small
                });
            });
            $('#popupSuccessBasic').click(function () {
                Lobibox.alert('success', {
                    msg: MSG_small
                });
            });
            $('#popupInfoBasic').click(function () {
                Lobibox.alert('info', {
                    msg: MSG_small
                });
            });
            $('#popupWarningBasic').click(function () {
                Lobibox.alert('warning', {
                    msg: MSG_small
                });
            });
            $('#popupPromptBasic').click(function () {
                Lobibox.prompt('text', {
                    title: 'Please enter username',
                    shown: function(box){
                        console.log(box.$input[0]);
                    },
                    attrs: {
                        placeholder: "Username"
                    }
                });
            });
            $('#popupProgressBasic').click(function () {
                var inter;
                Lobibox.progress({
                    title: 'Please wait',
                    label: 'Uploading files...',
                    onShow: function ($this) {
                        var i = 0;
                        inter = setInterval(function () {
                            window.console.log(i);
                            if (i > 100) {
                                inter = clearInterval(inter);
                            }
                            i = i + 0.1;
                            $this.setProgress(i);
                        }, 10);
                    },
                    closed: function () {
                        inter = clearInterval(inter);
                    }
                });
            });
            $('#popupProgressBootstrap').click(function () {
                var inter;
                Lobibox.progress({
                    title: 'Please wait',
                    label: 'Uploading files...',
                    progressTpl: '<div class="progress lobibox-progress-outer">\n\
                    <div class="progress-bar progress-bar-danger progress-bar-striped lobibox-progress-element" data-role="progress-text" role="progressbar"></div>\n\
                    </div>',
                    progressCompleted: function () {
                        Lobibox.notify('success', {
                            msg: 'Files were successfully uploaded'
                        });
                    },
                    onShow: function ($this) {
                        var i = 0;
                        inter = setInterval(function () {
                            window.console.log(i);
                            if (i > 100) {
                                clearInterval(inter);
                            }
                            i = i + 0.2;
                            $this.setProgress(i);
                        }, 1000 / 30);
                    },
                    closed: function () {
                        inter = clearInterval(inter);
                    }
                });
            });
            $('#popupWindowBasic').click(function () {
                Lobibox.window({
                    title: 'Window title',
                    content: [
                        '<p>'+MSG_large+'</p>',
                        '<p>'+MSG_large+'</p>',
                        '<p>'+MSG_large+'</p>'
                    ].join("")
                });
            });
        })();
        (function () {
            $('#popupYesNoCallback').click(function () {
                Lobibox.confirm({
                    msg: "Are you sure you want to delete this user?",
                    callback: function ($this, type) {
                        if (type === 'yes') {
                            Lobibox.notify('success', {
                                msg: 'You have clicked "Yes" button.'
                            });
                        } else if (type === 'no') {
                            Lobibox.notify('info', {
                                msg: 'You have clicked "No" button.'
                            });
                        }
                    }
                });
            });
        })();
        (function () {
            function processData(params) {
                if (params.width === "") {
                    delete params.width;
                }
                if (params.title === "") {
                    delete params.title;
                }
                if (params.iconClass === "") {
                    delete params.iconClass;
                }
                var checks = ['closeButton', 'draggable', 'modal', 'closeOnEsc', 'showProgressLabel'];
                for (var i in checks) {
                    params[checks[i]] = !!params[checks[i]];
                }
                if (params.placeholder) {
                    params.attrs = {
                        placeholder: params.placeholder
                    };
                }
                return params;
            }

            var $form = $('#lobibox-popup-demo-form');

            var $popupType = $form.find('[name="popupType"]');
            $popupType.change(function () {
                var $this = $(this);
                $form.find('.alert-fieldset').attr('disabled', true);
                $form.find('.prompt-fieldset').attr('disabled', true);
                $form.find('.confirm-fieldset').attr('disabled', true);
                $form.find('.progress-fieldset').attr('disabled', true);

                if ($this.val() === 'alert') {
                    $form.find('.alert-fieldset').removeAttr('disabled');
                    $form.find('[href="#alert-options"]').trigger('click');
                } else if ($this.val() === 'prompt') {
                    $form.find('.prompt-fieldset').removeAttr('disabled');
                    $form.find('[href="#prompt-options"]').trigger('click');
                } else if ($this.val() === 'confirm') {
                    $form.find('.confirm-fieldset').removeAttr('disabled');
                    $form.find('[href="#confirm-options"]').trigger('click');
                } else if ($this.val() === 'progress') {
                    $form.find('.progress-fieldset').removeAttr('disabled');
                    $form.find('[href="#progress-options"]').trigger('click');
                }
            });
            $form.submit(function (ev) {
                ev.preventDefault();
                var inter;
                var params = $form.serializeObject();
                params = processData(params);
                if (params.popupType === 'confirm') {
                    Lobibox.confirm(params);
                } else if (params.popupType === 'progress') {
                    params.onShow = function ($this) {
                        var i = 0;
                        inter = setInterval(function () {
                            if (i > 100) {
                                inter = clearInterval(inter);
                            }
                            i = i + 0.1;
                            $this.setProgress(i);
                        }, 10);
                    };
                    params.closed = function () {
                        inter = clearInterval(inter);
                    };
                    Lobibox.progress(params);
                } else {
                    Lobibox[params.popupType](params.type, params);
                }
            });
        })();
        (function () {
            $('#popupProgressErrorButtons').click(function () {
                Lobibox.alert('error', {
                    msg: 'This is an error message',
                    //                    buttons: ['ok', 'cancel', 'yes', 'no'],
                    //Or more powerfull way
                    buttons: {
                        ok: {
                            'class': 'btn btn-info',
                            closeOnClick: false
                        },
                        cancel: {
                            'class': 'btn btn-danger',
                            closeOnClick: false
                        },
                        yes: {
                            'class': 'btn btn-success',
                            closeOnClick: false
                        },
                        no: {
                            'class': 'btn btn-warning',
                            closeOnClick: false
                        },
                        custom: {
                            'class': 'btn btn-default',
                            text: 'Custom'
                        }
                    },
                    callback: function (lobibox, type) {
                        var btnType;
                        if (type === 'no') {
                            btnType = 'warning';
                        } else if (type === 'yes') {
                            btnType = 'success';
                        } else if (type === 'ok') {
                            btnType = 'info';
                        } else if (type === 'cancel') {
                            btnType = 'error';
                        }
                        Lobibox.notify(btnType, {
                            size: 'mini',
                            msg: 'This is ' + btnType + ' message'
                        });
                    }
                });
            });
            $('#popupConfirmNoIcon').click(function () {
                Lobibox.confirm({
                    iconClass: false,
                    msg: 'Are you sure?'
                });
            });
        })();
        (function () {
            $('#popupWindowExample').click(function () {
                Lobibox.window({
                    title: 'Window title',
                    //Available types: string, jquery object, function
                    content: function () {
                        return $('.container');
                    },
                    url: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.css',
                    autoload: false,
                    loadMethod: 'GET',
                    //Load parameters
                    params: {
                        param1: 'Lorem',
                        param2: 'Ipsum'
                    },
                    buttons: {
                        load: {
                            text: 'Load from url'
                        },
                        close: {
                            text: 'Close',
                            closeOnClick: true
                        }
                    },
                    callback: function ($this, type) {
                        if (type === 'load') {
                            $this.load(function () {
                                var $body = $this.$el.find('.lobibox-body');
                                $body.html('<div class="highlight"><pre><code>' + $body.html() + '</code></pre></div>');
                                hljs.highlightBlock($body.find('code')[0]);
                            });
                        }
                    }
                });
            });
        })();
    })();
	
    (function () {
        //            Notification basic example
        (function () {
            $('#basicDefault').click(function () {
                Lobibox.notify('default', {
                    msg: MSG_small
                });
            });
            $('#basicInfo').click(function () {
                Lobibox.notify('info', {
                    msg: MSG_small
                });
            });
            $('#basicWarning').click(function () {
                Lobibox.notify('warning', {
                    msg: MSG_small
                });
            });
            $('#basicError').click(function () {
                Lobibox.notify('error', {
                    msg: MSG_small
                });
            });
            $('#basicSuccess').click(function () {
                Lobibox.notify('success', {
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification with image
        (function () {
            $('#basicDefaultImage').click(function () {
                Lobibox.notify('default', {
                    img: IMG_PREFIX + '1.jpg',
                    msg: MSG_small
                });
            });
            $('#basicInfoImage').click(function () {
                Lobibox.notify('info', {
                    img: IMG_PREFIX + '1.jpg',
                    msg: MSG_small
                });
            });
            $('#basicWarningImage').click(function () {
                Lobibox.notify('warning', {
                    img: IMG_PREFIX + '2.jpg',
                    msg: MSG_small
                });
            });
            $('#basicErrorImage').click(function () {
                Lobibox.notify('error', {
                    img: IMG_PREFIX + '3.jpg',
                    msg: MSG_small
                });
            });
            $('#basicSuccessImage').click(function () {
                Lobibox.notify('success', {
                    img: IMG_PREFIX + '4.jpg',
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification Without sound
        (function () {
            $('#basicInfoNoSound').click(function () {
                Lobibox.notify('info', {
                    sound: false,
                    msg: MSG_small
                });
            });
            $('#basicWarningNoSound').click(function () {
                Lobibox.notify('warning', {
                    sound: false,
                    msg: MSG_small
                });
            });
            $('#basicErrorNoSound').click(function () {
                Lobibox.notify('error', {
                    sound: false,
                    msg: MSG_small
                });
            });
            $('#basicSuccessNoSound').click(function () {
                Lobibox.notify('success', {
                    sound: false,
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification custom title
        (function () {
            $('#basicDefaultCustomTitle').click(function () {
                Lobibox.notify('default', {
                    title: 'Info title',
                    msg: MSG_small
                });
            });
            $('#basicInfoCustomTitle').click(function () {
                Lobibox.notify('info', {
                    title: 'Info title',
                    msg: MSG_small
                });
            });
            $('#basicWarningCustomTitle').click(function () {
                Lobibox.notify('warning', {
                    title: 'Warning title',
                    msg: MSG_small
                });
            });
            $('#basicErrorCustomTitle').click(function () {
                Lobibox.notify('error', {
                    title: 'Error title',
                    msg: MSG_small
                });
            });
            $('#basicSuccessCustomTitle').click(function () {
                Lobibox.notify('success', {
                    title: 'Success title',
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification without icon
        (function () {
            $('#basicDefaultNoIcon').click(function () {
                Lobibox.notify('default', {
                    icon: false,
                    msg: MSG_small
                });
            });
            $('#basicInfoNoIcon').click(function () {
                Lobibox.notify('info', {
                    icon: false,
                    msg: MSG_small
                });
            });
            $('#basicWarningNoIcon').click(function () {
                Lobibox.notify('warning', {
                    icon: false,
                    msg: MSG_small
                });
            });
            $('#basicErrorNoIcon').click(function () {
                Lobibox.notify('error', {
                    icon: false,
                    msg: MSG_small
                });
            });
            $('#basicSuccessNoIcon').click(function () {
                Lobibox.notify('success', {
                    icon: false,
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification custom delay
        (function () {
            $('#basicDefaultCustomDelay').click(function () {
                Lobibox.notify('default', {
                    delay: 25000,
                    msg: MSG_small
                });
            });
            $('#basicInfoCustomDelay').click(function () {
                Lobibox.notify('info', {
                    delay: 25000,
                    msg: MSG_small
                });
            });
            $('#basicWarningCustomDelay').click(function () {
                Lobibox.notify('warning', {
                    delay: 25000,
                    msg: MSG_small
                });
            });
            $('#basicErrorCustomDelay').click(function () {
                Lobibox.notify('error', {
                    delay: 25000,
                    msg: MSG_small
                });
            });
            $('#basicSuccessCustomDelay').click(function () {
                Lobibox.notify('success', {
                    delay: 25000,
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification no delay
        (function () {
            $('#basicDefaultNoDelay').click(function () {
                Lobibox.notify('default', {
                    delay: false,
                    title: 'Info title',
                    msg: MSG_small
                });
            });
            $('#basicInfoNoDelay').click(function () {
                Lobibox.notify('info', {
                    delay: false,
                    title: 'Info title',
                    msg: MSG_small
                });
            });
            $('#basicWarningNoDelay').click(function () {
                Lobibox.notify('warning', {
                    delay: false,
                    title: 'Warning title',
                    msg: MSG_small
                });
            });
            $('#basicErrorNoDelay').click(function () {
                Lobibox.notify('error', {
                    delay: false,
                    title: 'Error title',
                    msg: MSG_small
                });
            });
            $('#basicSuccessNoDelay').click(function () {
                Lobibox.notify('success', {
                    delay: false,
                    title: 'Success title',
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification positioning
        (function () {
            $('#basicInfoPosition').click(function () {
                Lobibox.notify('info', {
                    position: 'top left',
                    msg: MSG_small
                });
            });
            $('#basicWarningPosition').click(function () {
                Lobibox.notify('warning', {
                    position: 'top right',
                    msg: MSG_small
                });
            });
            $('#basicErrorPosition').click(function () {
                Lobibox.notify('error', {
                    position: 'bottom left',
                    msg: MSG_small
                });
            });
            $('#basicSuccessPosition').click(function () {
                Lobibox.notify('success', {
                    position: 'bottom right',
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification custom width
        (function () {
            $('#basicInfoWidth').click(function () {
                Lobibox.notify('info', {
                    width: 300,
                    msg: MSG_small
                });
            });
            $('#basicWarningWidth').click(function () {
                Lobibox.notify('warning', {
                    width: 500,
                    msg: MSG_small
                });
            });
            $('#basicErrorWidth').click(function () {
                Lobibox.notify('error', {
                    width: $(window).width(),
                    msg: MSG_small
                });
            });
            $('#basicSuccessWidth').click(function () {
                Lobibox.notify('success', {
                    width: 600,
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification custom animation
        (function () {
            $('#basicInfoAnimation').click(function () {
                Lobibox.notify('info', {
                    showClass: 'fadeInDown',
                    hideClass: 'fadeUpDown',
                    msg: MSG_small
                });
            });
            $('#basicWarningAnimation').click(function () {
                Lobibox.notify('warning', {
                    showClass: 'bounceIn',
                    hideClass: 'bounceOut',
                    msg: MSG_small
                });
            });
            $('#basicErrorAnimation').click(function () {
                Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    msg: MSG_small
                });
            });
            $('#basicSuccessAnimation').click(function () {
                Lobibox.notify('success', {
                    showClass: 'rollIn',
                    hideClass: 'rollOut',
                    msg: MSG_small
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification Large
        (function () {
            $('#largeDefaultBasic').click(function () {
                Lobibox.notify('default', {
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeInfoBasic').click(function () {
                Lobibox.notify('info', {
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeWarningBasic').click(function () {
                Lobibox.notify('warning', {
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeErrorBasic').click(function () {
                Lobibox.notify('error', {
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeSuccessBasic').click(function () {
                Lobibox.notify('success', {
                    size: 'large',
                    msg: MSG_medium
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification large with image
        (function () {
            $('#largeDefaultImage').click(function () {
                Lobibox.notify('default', {
                    img: IMG_PREFIX + '1.jpg',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeInfoImage').click(function () {
                Lobibox.notify('info', {
                    img: IMG_PREFIX + '1.jpg',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeWarningImage').click(function () {
                Lobibox.notify('warning', {
                    img: IMG_PREFIX + '2.jpg',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeErrorImage').click(function () {
                Lobibox.notify('error', {
                    img: IMG_PREFIX + '3.jpg',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeSuccessImage').click(function () {
                Lobibox.notify('success', {
                    img: IMG_PREFIX + '4.jpg',
                    size: 'large',
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification large positioning
        (function () {
            $('#largeDefaultPosition').click(function () {
                Lobibox.notify('default', {
                    size: 'large',
                    position: 'bottom left',
                    msg: MSG_medium
                });
            });
            $('#largeInfoPosition').click(function () {
                Lobibox.notify('info', {
                    size: 'large',
                    position: 'bottom left',
                    msg: MSG_medium
                });
            });
            $('#largeWarningPosition').click(function () {
                Lobibox.notify('warning', {
                    size: 'large',
                    position: 'bottom right',
                    msg: MSG_medium
                });
            });
            $('#largeErrorPosition').click(function () {
                Lobibox.notify('error', {
                    size: 'large',
                    position: 'top left',
                    msg: MSG_medium
                });
            });
            $('#largeSuccessPosition').click(function () {
                Lobibox.notify('success', {
                    size: 'large',
                    position: 'top right',
                    msg: MSG_medium
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification large animation
        (function () {
            $('#largeDefaultAnimation').click(function () {
                Lobibox.notify('default', {
                    showClass: 'fadeInDown',
                    hideClass: 'fadeUpDown',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeInfoAnimation').click(function () {
                Lobibox.notify('info', {
                    showClass: 'fadeInDown',
                    hideClass: 'fadeUpDown',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeWarningAnimation').click(function () {
                Lobibox.notify('warning', {
                    showClass: 'bounceIn',
                    hideClass: 'bounceOut',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeErrorAnimation').click(function () {
                Lobibox.notify('error', {
                    showClass: 'zoomInUp',
                    hideClass: 'zoomOutDown',
                    size: 'large',
                    msg: MSG_medium
                });
            });
            $('#largeSuccessAnimation').click(function () {
                Lobibox.notify('success', {
                    showClass: 'rollIn',
                    hideClass: 'rollOut',
                    size: 'large',
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification mini basic
        (function () {
            $('#miniDefaultAnimation').click(function () {
                Lobibox.notify('default', {
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniInfoAnimation').click(function () {
                Lobibox.notify('info', {
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniWarningAnimation').click(function () {
                Lobibox.notify('warning', {
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniErrorAnimation').click(function () {
                Lobibox.notify('error', {
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniSuccessAnimation').click(function () {
                Lobibox.notify('success', {
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification mini with image
        (function () {
            $('#miniDefaultImage').click(function () {
                Lobibox.notify('default', {
                    img: IMG_PREFIX + '1.jpg',
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniInfoImage').click(function () {
                Lobibox.notify('info', {
                    img: IMG_PREFIX + '1.jpg',
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniWarningImage').click(function () {
                Lobibox.notify('warning', {
                    img: IMG_PREFIX + '2.jpg',
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniErrorImage').click(function () {
                Lobibox.notify('error', {
                    img: IMG_PREFIX + '3.jpg',
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
            $('#miniSuccessImage').click(function () {
                Lobibox.notify('success', {
                    img: IMG_PREFIX + '4.jpg',
                    size: 'mini',
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification mini without icon
        (function () {
            $('#miniDefaultNoIcon').click(function () {
                Lobibox.notify('default', {
                    size: 'mini',
                    icon: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniInfoNoIcon').click(function () {
                Lobibox.notify('info', {
                    size: 'mini',
                    icon: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniWarningNoIcon').click(function () {
                Lobibox.notify('warning', {
                    size: 'mini',
                    icon: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniErrorNoIcon').click(function () {
                Lobibox.notify('error', {
                    size: 'mini',
                    icon: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniSuccessNoIcon').click(function () {
                Lobibox.notify('success', {
                    size: 'mini',
                    icon: false,
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification mini with title
        (function () {
            $('#miniDefaultTitle').click(function () {
                Lobibox.notify('default', {
                    size: 'mini',
                    title: 'Lorem ipsum',
                    msg: MSG_xsmall
                });
            });
            $('#miniInfoTitle').click(function () {
                Lobibox.notify('info', {
                    size: 'mini',
                    title: 'Lorem ipsum',
                    msg: MSG_xsmall
                });
            });
            $('#miniWarningTitle').click(function () {
                Lobibox.notify('warning', {
                    size: 'mini',
                    title: 'Lorem ipsum',
                    msg: MSG_xsmall
                });
            });
            $('#miniErrorTitle').click(function () {
                Lobibox.notify('error', {
                    size: 'mini',
                    title: 'Lorem ipsum',
                    msg: MSG_xsmall
                });
            });
            $('#miniSuccessTitle').click(function () {
                Lobibox.notify('success', {
                    size: 'mini',
                    title: 'Lorem ipsum',
                    msg: MSG_xsmall
                });
            });
        })();
        //------------------------------------------------------------------------------
        //                Notification mini with title
        (function () {
            $('#miniDefaultRounded').click(function () {
                Lobibox.notify('default', {
                    size: 'mini',
                    rounded: true,
                    delayIndicator: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniInfoRounded').click(function () {
                Lobibox.notify('info', {
                    size: 'mini',
                    rounded: true,
                    delayIndicator: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniWarningRounded').click(function () {
                Lobibox.notify('warning', {
                    size: 'mini',
                    rounded: true,
                    delayIndicator: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniErrorRounded').click(function () {
                Lobibox.notify('error', {
                    size: 'mini',
                    rounded: true,
                    delayIndicator: false,
                    msg: MSG_xsmall
                });
            });
            $('#miniSuccessRounded').click(function () {
                Lobibox.notify('success', {
                    size: 'mini',
                    rounded: true,
                    delayIndicator: false,
                    msg: MSG_xsmall
                });
            });
        })();
    })();
});