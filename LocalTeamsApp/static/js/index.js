$('.icon').click(function () {
	if($('#password1').attr('type') == "text"){
		$('#password1').attr('type', 'password');
		$('#glyph').attr('class', 'glyphicon glyphicon-eye-close o-fld');
		
	}
	else if($('#password1').attr('type') == "password"){
		$('#password1').attr('type', 'text');
		$('#glyph').attr('class', 'glyphicon glyphicon-eye-open o-fld');
	}
});
$('.iconC').click(function () {
	if($('#password2').attr('type') == "text"){
		$('#password2').attr('type', 'password');
		$('#glyphC').attr('class', 'glyphicon glyphicon-eye-close o-fld');
	}
	else if($('#password2').attr('type') == "password"){
		$('#password2').attr('type', 'text');
		$('#glyphC').attr('class', 'glyphicon glyphicon-eye-open o-fld');
	}
});
$('.iconL').click(function () {
	if($('.passwordL').attr('type') == "text"){
		$('.passwordL').attr('type', 'password');
		$('#glyphL').attr('class', 'glyphicon glyphicon-eye-close');
	}
	else if($('.passwordL').attr('type') == "password"){
		$('.passwordL').attr('type', 'text');
		$('#glyphL').attr('class', 'glyphicon glyphicon-eye-open');
	}
});

/*$('.carousel').bind('slide.bs.carousel', function (e) {
   console.log('before');
});
$('.carousel').bind('slid.bs.carousel', function (e) {
   console.log('After');
});*/
$(document).ready(function(){
	$('.carousel').carousel({
		interval: 15000 
	});
	$('.selectpicker').selectpicker();
	$("#email").click(function(){
		$("#o-fld,#o-fld1").slideDown("slow");
	});
	$("#forget_pass").click(function(){
		$("#forget_div").toggle("slow");
	});	
	var d = new Date();
	var n = d.getFullYear();
	document.getElementById("demo").innerHTML = n;
});
$(document).mouseup(function (e){
	var container = $("#email");
	var container2 = $(".o-fld");
	if ( !(container.is(e.target) || container2.is(e.target)) )// if the target of the click isn't the container...
	{
		if($('#email').val() === 0){
			$("#o-fld,#o-fld1").slideUp("slow");
		}
	}
});
