var homeSleeves = false;
var awaySleeves = false;

function saveKit() {

	var home = $('#kc-shirt-1');
	var away = $('#kc-shirt-2');
	
	var homeSave = $('#shirt-1');
	var awaySave = $('#shirt-2');
	
	
	//HOME KIT #####################
	
	//get colours
	var home_shirt_color = home.find('.club-reg-shirt-mid').css('background-color');
	var home_left_sleeve = home.find('.club-reg-shirt-left').css('background-color');
	var home_right_sleeve = home.find('.club-reg-shirt-right').css('background-color');
	
	//save shirt colour
	homeSave.find('.club-reg-shirt-mid').css('background-color', home_shirt_color);
	
	//if sleeves are set, save them
	if(homeSleeves) {
		homeSave.find('.club-reg-shirt-left').css('background-color', home_left_sleeve);
		homeSave.find('.club-reg-shirt-right').css('background-color', home_right_sleeve);
		
		homeSave.find('.club-reg-shirt-hoops').css('z-index','899'); //set stripes/hoops under sleeve colour
		homeSave.find('.club-reg-shirt-stripes').css('z-index','899'); //set stripes/hoops under sleeve colour
		homeSave.find('.club-reg-shirt-split').css('z-index','899'); //set stripes/hoops under 
						
						
	} else {
		homeSave.find('.club-reg-shirt-left').css('background-color', home_shirt_color);
		homeSave.find('.club-reg-shirt-right').css('background-color', home_shirt_color);
		
		homeSave.find('.club-reg-shirt-hoops').css('z-index','905'); //set stripes/hoops under sleeve colour
		homeSave.find('.club-reg-shirt-stripes').css('z-index','905'); //set stripes/hoops under sleeve colour
		homeSave.find('.club-reg-shirt-split').css('z-index','905'); //set stripes/hoops under 
	
	}
	
	//get apttern type
	var home_pattern = $('input[name=ks-home-pattern]:checked').val();
	
	
	homeSave.find('.club-reg-shirt-hoops').css('display', 'none');
	homeSave.find('.club-reg-shirt-stripes').css('display', 'none');
	homeSave.find('.club-reg-shirt-split').css('display', 'none');
	
	//get pattern colour, depending on pattern
	if(home_pattern == "stripe") {
		var home_pattern_color = home.find('.club-reg-shirt-stripes .stripe').css('background-color');
		homeSave.find('.club-reg-shirt-stripes .stripe').css('background-color', home_pattern_color);
		homeSave.find('.club-reg-shirt-stripes').css('display', 'block');
	} else if(home_pattern == "hoop") {
		var home_pattern_color = home.find('.club-reg-shirt-hoops .hoop').css('background-color');
		homeSave.find('.club-reg-shirt-hoops .hoop').css('background-color', home_pattern_color);
		homeSave.find('.club-reg-shirt-hoops').css('display', 'block');
	} else if(home_pattern == "split") {
		var home_pattern_color = home.find('.club-reg-shirt-split').css('background-color');
		homeSave.find('.club-reg-shirt-split').css('background-color', home_pattern_color);
		homeSave.find('.club-reg-shirt-split').css('display', 'block');
	}
	
	
	//AWAY KIT #########
	
	//get colours
	var away_shirt_color = away.find('.club-reg-shirt-mid').css('background-color');
	var away_left_sleeve = away.find('.club-reg-shirt-left').css('background-color');
	var away_right_sleeve = away.find('.club-reg-shirt-right').css('background-color');
	
	//save shirt colour
	awaySave.find('.club-reg-shirt-mid').css('background-color', away_shirt_color);
	
	//if sleeves are set, save them
	if(awaySleeves) {
		awaySave.find('.club-reg-shirt-left').css('background-color', away_left_sleeve);
		awaySave.find('.club-reg-shirt-right').css('background-color', away_right_sleeve);
		
		awaySave.find('.club-reg-shirt-hoops').css('z-index','899'); //set stripes/hoops under sleeve colour
		awaySave.find('.club-reg-shirt-stripes').css('z-index','899'); //set stripes/hoops under sleeve colour
		awaySave.find('.club-reg-shirt-split').css('z-index','899'); //set stripes/hoops under 
						
						
	} else {
		awaySave.find('.club-reg-shirt-left').css('background-color', away_shirt_color);
		awaySave.find('.club-reg-shirt-right').css('background-color', away_shirt_color);
		
		awaySave.find('.club-reg-shirt-hoops').css('z-index','905'); //set stripes/hoops under sleeve colour
		awaySave.find('.club-reg-shirt-stripes').css('z-index','905'); //set stripes/hoops under sleeve colour
		awaySave.find('.club-reg-shirt-split').css('z-index','905'); //set stripes/hoops under 
	
	}
	
	//get apttern type
	var away_pattern = $('input[name=ks-away-pattern]:checked').val();
	
	
	awaySave.find('.club-reg-shirt-hoops').css('display', 'none');
	awaySave.find('.club-reg-shirt-stripes').css('display', 'none');
	awaySave.find('.club-reg-shirt-split').css('display', 'none');
	
	//get pattern colour, depending on pattern
	if(away_pattern == "stripe") {
		var away_pattern_color = away.find('.club-reg-shirt-stripes .stripe').css('background-color');
		awaySave.find('.club-reg-shirt-stripes .stripe').css('background-color', away_pattern_color);
		awaySave.find('.club-reg-shirt-stripes').css('display', 'block');
	} else if(away_pattern == "hoop") {
		var away_pattern_color = away.find('.club-reg-shirt-hoops .hoop').css('background-color');
		awaySave.find('.club-reg-shirt-hoops .hoop').css('background-color', away_pattern_color);
		awaySave.find('.club-reg-shirt-hoops').css('display', 'block');
	} else if(away_pattern == "split") {
		var away_pattern_color = away.find('.club-reg-shirt-split').css('background-color');
		awaySave.find('.club-reg-shirt-split').css('background-color', away_pattern_color);
		awaySave.find('.club-reg-shirt-split').css('display', 'block');
	}
	
	
						
}


$(document).ready(function() {

	$('#save-kits').click(function() {
	
		saveKit();
		$('#kitCreator').modal('hide');
	
	});

	/* $('#shirt-1-cp').click(function() {
		$('#kitCreator').modal('show');
	
	});
	
	$('#shirt-2-cp').click(function() {
	
		$('#kitCreator').modal('show');
	});
 */
	

	var reset = false;
	$('.ks-cp').spectrum({
		
		color: "",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 5,
		preferredFormat: "hex",
		localStorageKey: "spectrum.demo",
		palette: [ ],
		clickoutFiresChange: true,
		allowEmpty: true,
		change: function(color) {			
			
			var kit = $(this).data('kit'); //home or away kit
			var part = $(this).data('part'); //part of shirt
			
			var kitCSSID = (kit=="home") ? "kc-shirt-1" : "kc-shirt-2"; //appropriate CSS id
			
			switch(part) {
			
				case "shirt":
				
					if(!color) {
					
						$("#" + kitCSSID + " .club-reg-shirt-mid").css('background-color', 'white');
						if((kit == "home" && !homeSleeves) || (kit == "away" && !awaySleeves)){			//if sleeves aren't yet set, set them too
							$("#" + kitCSSID + " .club-reg-shirt-left").css('background-color', 'none');
							$("#" + kitCSSID + " .club-reg-shirt-right").css('background-color', 'none');
						}
						
					} else {
					
						$("#" + kitCSSID + " .club-reg-shirt-mid").css('background-color', color.toHexString()); //set mid color
						
						if((kit == "home" && !homeSleeves) || (kit == "away" && !awaySleeves)){			//if sleeves aren't yet set, set them too
							$("#" + kitCSSID + " .club-reg-shirt-left").css('background-color', color.toHexString());
							$("#" + kitCSSID + " .club-reg-shirt-right").css('background-color', color.toHexString());
						}
					
					}

				break;
				case "sleeve":
				
					if(!color) {
						$("#" + kitCSSID + " .club-reg-shirt-left").css('background-color', $("#" + kitCSSID + " .club-reg-shirt-mid").css('background-color'));
						$("#" + kitCSSID + " .club-reg-shirt-right").css('background-color', $("#" + kitCSSID + " .club-reg-shirt-mid").css('background-color'));
						$("#" + kitCSSID + " .club-reg-shirt-stripes").css('z-index','905'); //set stripes/hoops under sleeve colour
						$("#" + kitCSSID + " .club-reg-shirt-hoops").css('z-index','905'); //set stripes/hoops under sleeve colour
						$("#" + kitCSSID + " .club-reg-shirt-split").css('z-index','905'); //set stripes/hoops under sleeve colour
						
						if(kit == "home") { 
							homeSleeves = false;//sleeves are set!
						} else {
							awaySleeves = false;//sleeves are set!
						}
					
					
					} else {
						$("#" + kitCSSID + " .club-reg-shirt-left").css('background-color', color.toHexString());
						$("#" + kitCSSID + " .club-reg-shirt-right").css('background-color', color.toHexString());
						$("#" + kitCSSID + " .club-reg-shirt-stripes").css('z-index','899'); //set stripes/hoops under sleeve colour
						$("#" + kitCSSID + " .club-reg-shirt-hoops").css('z-index','899'); //set stripes/hoops under sleeve colour
						$("#" + kitCSSID + " .club-reg-shirt-split").css('z-index','899'); //set stripes/hoops under sleeve colour
						
						if(kit == "home") { 
							homeSleeves = true;//sleeves are set!
						} else {
							awaySleeves = true;//sleeves are set!
						}
					}
					

				break;
				case "pattern":
				
					if(!color) {
					
						//set pattern colours!
						$("#" + kitCSSID + " .club-reg-shirt-stripes .stripe").css('background-color', 'none');
						$("#" + kitCSSID + " .club-reg-shirt-hoops .hoop").css('background-color', 'none');
						$("#" + kitCSSID + " .club-reg-shirt-split").css('background-color', 'none');
					
					} else {		
						
						//set pattern colours!
						$("#" + kitCSSID + " .club-reg-shirt-stripes .stripe").css('background-color', color.toHexString());
						$("#" + kitCSSID + " .club-reg-shirt-hoops .hoop").css('background-color', color.toHexString());
						$("#" + kitCSSID + " .club-reg-shirt-split").css('background-color', color.toHexString());
					
					}
					
				break;
			}
			
			
		
		}
	});
	
	$('input[name=ks-away-pattern], input[name=ks-home-pattern]').on('change', function() { //when activate/decative pattern

		var dataKit = $(this).data('kit');
		
		console.log(dataKit);
		var kitCSSID = (dataKit == "home") ? "kc-shirt-1" : "kc-shirt-2";
		var pattern = $('input[name=ks-' + dataKit + '-pattern]:checked').val();
		
		$("#" + kitCSSID + " .club-reg-shirt-hoops").css('display', 'none');
		$("#" + kitCSSID + " .club-reg-shirt-stripes").css('display', 'none');
		$("#" + kitCSSID + " .club-reg-shirt-split").css('display', 'none');
		
		if(pattern  == "stripe") {
			$("#" + kitCSSID + " .club-reg-shirt-stripes").css('display', 'block');
		
		} else if(pattern == "hoop") {
			$("#" + kitCSSID + " .club-reg-shirt-hoops").css('display', 'block');
			
		} else if(pattern == "split") {		
			$("#" + kitCSSID + " .club-reg-shirt-split").css('display', 'block');
		}
		
	});	
	

});