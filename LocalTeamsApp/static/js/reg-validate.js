var club_nameE = estdyrE = genderE = num_teamsE = ground_nameE = postcodeE = street_nameE = telephoneE = mobile_noE = mobile_noEx = trainingE = club_historyE = age_group = team_gender = false;//club_logoE = club_coverE = club_teamE =
var endyr = new Date();
endyr = parseInt(endyr.getFullYear())+1;
var telephonePtrn = /^(0)[0-9]{10}$/;

function checkPostcode(PostVal){
	$.ajax({
		type: "POST",
		url: "/is_postcode/",
		data:{
				postcode:$("#postcode").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
			},
		success: function(response, jqXHR) {
			if(response.valid){
				$("#postcodeRE").attr('class','hide');
				$("#postcodeM").attr('class','hide');
				$("#postcodeV").attr('class','hide');
			}
			else{
				$("#postcodeRE").attr('class','img-responsive errL-icon show');
				$("#postcodeM").attr('class','hide');
				$("#postcodeV").attr('class','show');
			}
			PostVal(response.valid)
		},
		error: function(err){
			PostVal(false);
			console.log(err);
		}
	});
}

function adminNoX(MobVal){
	$.ajax({
		type: "POST",
		url: "/eXAdminM/",
		data:{
			mobile:$("#mobile_no").val(),
		},
		success: function(response) {
			if(response.valid){
				$("#mobileRE").addClass('hide');
				$("#mobile_noX").addClass('hide');
				$("#mobile_noM").addClass('hide');
				$("#mobile_noV").addClass('hide');
			}
			else{
				$("#mobileRE").removeClass('hide');
				$("#mobile_noX").removeClass('hide');
				$("#mobile_noM").addClass('hide');
				$("#mobile_noV").addClass('hide');
			}
			MobVal(response.valid);
		},
		error: function(err){
			MobVal(false);
			console.log(err);
		}
	});
}

$("#club_name").focusout(function(){
	if($("#club_name").val() == ""){
		$("#club_nameM").attr('class','show');
		$("#clubRE").attr('class','img-responsive club-icon show');
		window.club_nameE = false;
	}else{
		$("#clubRE").attr('class','hide');
		$("#club_nameM").attr('class','hide');
		window.club_nameE = true;
	}
});

$("#mobile_no").focusout(function(){
		window.mobile_noE = telephonePtrn.test($(this).val());
		if(mobile_noE){
			$("#mobileRE").addClass('hide');
			$("#mobile_noX").addClass('hide');
			$("#mobile_noM").addClass('hide');
			$("#mobile_noV").addClass('hide');
			adminNoX(function(MobVal){
				window.mobile_noEx = MobVal;
			})
			
		}
		else{
			if($("#mobile_no").val().length != 11 && $("#mobile_no").val().length != 0){
				$("#mobileRE").removeClass('hide');
				$("#mobile_noX").addClass('hide');
				$("#mobile_noM").addClass('hide');
				$("#mobile_noV").removeClass('hide');
			}else{
				$("#mobileRE").removeClass('hide');
				$("#mobile_noX").addClass('hide');
				$("#mobile_noM").removeClass('hide');
				$("#mobile_noV").addClass('hide');
			}
		}
	});	

	$("#estdyr").focusout(function(){
		if($("#estdyr").val() == ""){
			window.estdyrE = false;
			$("#estdRE").attr('class','img-responsive errL-icon show');
			$("#estdyrM").attr('class','show');
		}else{
			if( ( (($("#estdyr").val() > 1849)?true:false) && (($("#estdyr").val() < window.endyr)?true:false) )?true:false ){
				window.estdyrE = true;
				// $("#estdyr").blur();
				$("#estdRE").attr('class','hide');
				$("#estdyrM").attr('class','hide');
			}
			else{
				$("#estdRE").attr('class','img-responsive errL-icon show');
				$("#estdyrM").attr('class','show');
			}
		}
	});
	
	$("#gender").change(function(){
			if($("#gender").val() == ""){
			$("#genderRE").attr('class','img-responsive errL-icon show');
			$("#genderM").attr('class','show');
			window.genderE = false;
		}else{
			$("#genderRE").attr('class','hide');
			$("#genderM").attr('class','hide');
			window.genderE = true;
		}
	});	
	
	$("#num-teams").change(function(){
		if($("#num-teams").val() == 0){
		$("#teamsRE").attr('class','img-responsive errL-icon show');
		$("#num-teamsM").attr('class','show');
		$("#limit").attr('class','hide');
		window.num_teamsE = false;
		}else{
			if($("#num-teams").val() < 21){
				$("#teamsRE").attr('class','hide');
				$("#num-teamsM").attr('class','hide');
				$("#limit").attr('class','hide');
				window.num_teamsE = true;
			}
			else{
				window.num_teamsE = false;
				$("#teamsRE").attr('class','img-responsive errL-icon show');
				$("#limit").attr('class','show');
				$("#num-teamsM").attr('class','hide');
			}
		}
	});

	$("#ground-name").focusout(function(){
		if($("#ground-name").val() == ""){
			window.ground_nameE = false;
			$("#groundRE").attr('class','img-responsive errL-icon show');
			$("#ground-nameM").attr('class','show');
		}else{
			window.ground_nameE = true;
			$("#groundRE").attr('class','hide');
			$("#ground-nameM").attr('class','hide');
		}
	});
	
	$("#postcode").change(function(){
		if($("#postcode").val() == ""){
			$("#postcodeRE").attr('class','img-responsive errL-icon show');
			$("#postcodeM").attr('class','show');
			$("#postcodeV").attr('class','hide');
		}
		else{
			checkPostcode(function(PostVal){
				window.postcodeE = PostVal
			});
		}
	});	
		
	$("#street-name").focusout(function(){
		if($("#street-name").val() == ""){
			window.street_nameE = false;
			$("#streetRE").attr('class','img-responsive errL-icon show');
			$("#street-nameM").attr('class','show');
		}else{
			window.street_nameE = true;
			$("#streetRE").attr('class','hide');
			$("#street-nameM").attr('class','hide');
		}
	});
		
	$("#telephone").focusout(function(){
		window.telephoneE = telephonePtrn.test($(this).val());
		if(telephoneE){
		$("#telephoneV").attr('class','hide');
		$("#telephoneM").attr('class','hide');
		$("#telephoneRE").attr('class','hide');
		}else{
			if($("#telephone").val().length != 11 && $("#telephone").val().length != 0){
				$("#telephoneRE").attr('class','img-responsive errL-icon show');
				$("#telephoneV").attr('class','show');
				$("#telephoneM").attr('class','hide');
			}else{
				$("#telephoneRE").attr('class','img-responsive errL-icon show');
				$("#telephoneM").attr('class','show');
				$("#telephoneV").attr('class','hide');
			}
		}
	});

	/************************************************************************/
function checkagain() {
	age_group_len = $('.age_group option:selected').filter("[value='']").length
	if(age_group_len == 0){
		$("#age_groupM").attr('class','hide');
		$("#age_groupRE").attr('class','hide');
		window.age_group = true;
	}
	else{
		$("#age_groupM").attr('class','show');
		$("#age_groupRE").attr('class','img-responsive age_group-icon show');
		window.age_group = false;
		
	}
	team_gender_len = $('.team_gender option:selected').filter("[value='']").length
	if(team_gender_len == 0){
		$("#team_genderM").attr('class','hide');
		$("#team_genderRE").attr('class','hide');
		window.team_gender = true;
	}
	else{
		$("#team_genderM").attr('class','show');
		$("#team_genderRE").attr('class','img-responsive team_gender-icon show');
		window.team_gender = false;
	}
	
	if($("#club_name").val() == ""){
		$("#club_nameM").attr('class','show');
		$("#clubRE").attr('class','img-responsive club-icon show');
		window.club_nameE = false;
		$("#club_name").focus();
	}else{
		window.club_nameE = true;
		$("#clubRE").attr('class','hide');
		$("#club_nameM").attr('class','hide');
		$("#club_name").blur();
	}

	window.mobile_noE = telephonePtrn.test($("#mobile_no").val());
	if(mobile_noE){
		$("#mobile_no").blur();
		$("#mobileRE").addClass('hide');
		$("#mobile_noX").addClass('hide');
		$("#mobile_noM").addClass('hide');
		$("#mobile_noV").addClass('hide');
		adminNoX(function(MobVal){
			window.mobile_noEx = MobVal;
		})
	}
	else{
		if($("#mobile_no").val().length != 11 && $("#mobile_no").val().length != 0){
			$("#mobile_no").focus();
			$("#mobileRE").removeClass('hide');
			$("#mobile_noX").addClass('hide');
			$("#mobile_noM").addClass('hide');
			$("#mobile_noV").removeClass('hide');
			window.mobile_noE = false
		}else{
			$("#mobile_no").focus();
			$("#mobileRE").removeClass('hide');
			$("#mobile_noX").addClass('hide');
			$("#mobile_noM").removeClass('hide');
			$("#mobile_noV").addClass('hide');
			window.mobile_noE = false
		}
	}

	if($("#estdyr").val() == ""){
		$("#estdyr").focus();
		$("#estdRE").attr('class','img-responsive errL-icon show');
		$("#estdyrM").attr('class','show');
		window.estdyrE = false;
	}else{
		if( ( (($("#estdyr").val() > 1849)?true:false) && (($("#estdyr").val() < window.endyr)?true:false) )?true:false ){
			window.estdyrE = true;
			$("#estdyr").blur();
			$("#estdRE").attr('class','hide');
			$("#estdyrM").attr('class','hide');
		}
		else{
			window.estdyrE = false;
			$("#estdyr").focus();
			$("#estdRE").attr('class','img-responsive errL-icon show');
			$("#estdyrM").attr('class','show');
		}
	}
	
	if($("#gender").val() == ""){
		$("#gender").focus();
		$("#genderRE").attr('class','img-responsive errL-icon show');
		$("#genderM").attr('class','show');
		window.genderE = false;
	}else{
		$("#gender").blur();
		$("#genderRE").attr('class','hide');
		$("#genderM").attr('class','hide');
		window.genderE = true;
	}
	
	if($("#num-teams").val() == 0){
		$("#num-teams").focus();
		$("#teamsRE").attr('class','img-responsive errL-icon show');
		$("#num-teamsM").attr('class','show');
		$("#limit").attr('class','hide');
	}else{
		if($("#num-teams").val() > 21){
			$("#num-teams").focus();
			$("#teamsRE").attr('class','img-responsive errL-icon show');
			$("#limit").attr('class','show');
			$("#num-teamsM").attr('class','hide');
			window.num_teamsE = false;
		}
		else{
			$("#num-teams").blur();
			$("#teamsRE").attr('class','hide');
			$("#num-teamsM").attr('class','hide');
			$("#limit").attr('class','hide');
			window.num_teamsE = true;
		}
	}

	if($("#ground-name").val() == ""){
		$("#ground-name").focus();
		$("#groundRE").attr('class','img-responsive errL-icon show');
		$("#ground-nameM").attr('class','show');
		window.ground_nameE = false;
	}
	else{
		window.ground_nameE = true;
		$("#ground-name").blur();
		$("#groundRE").attr('class','hide');
		$("#ground-nameM").attr('class','hide');
	}

	if($("#postcode").val() == ""){
		$("#postcode").focus();
		$("#postcodeRE").attr('class','img-responsive errL-icon show');
		$("#postcodeM").attr('class','show');
		$("#postcodeV").attr('class','hide');
		window.postcodeE = false;
	}else{
		checkPostcode(function(PostVal){
			window.postcodeE = PostVal;
			if(window.postcodeE){
				$("#postcode").blur();
			}
		});
	}

	if($("#street-name").val() == ""){
		$("#street-name").focus();
		$("#streetRE").attr('class','img-responsive errL-icon show');
		$("#street-nameM").attr('class','show');
	}else{
		window.street_nameE = true;
		$("#street-name").blur();
		$("#streetRE").attr('class','hide');
		$("#street-nameM").attr('class','hide');
	}
	
	window.telephoneE = telephonePtrn.test($("#telephone").val());
	if(telephoneE){
		$("#telephone").blur();
		$("#telephoneV").attr('class','hide');
		$("#telephoneM").attr('class','hide');
		$("#telephoneRE").attr('class','hide');
	}else{
		$("#telephone").focus();
		if($("#telephone").val().length != 11 && $("#telephone").val().length != 0){
			$("#telephoneRE").attr('class','img-responsive errL-icon show');
			$("#telephoneV").attr('class','show');
			$("#telephoneM").attr('class','hide');
		}else{
			$("#telephoneRE").attr('class','img-responsive errL-icon show');
			$("#telephoneM").attr('class','show');
			$("#telephoneV").attr('class','hide');
		}
	}


	if($("#training").val() == ""){
		$("#training").focus();
		$("#trainingRE").attr('class','img-responsive training-icon show');
		$("#training_textM").attr('class','show');
		window.trainingE = false;
	}
	else{
		window.trainingE = true;
		$("#training").blur();
		$("#trainingRE").attr('class','hide');
		$("#training_textM").attr('class','hide');
	}

	if($("#club_history").val() == ""){
		$("#club_history").focus();
		$("#historyRE").attr('class','img-responsive history-icon show');
		$("#history_textM").attr('class','show');
		window.club_historyE = false;
	}else{
		window.club_historyE = true;
		$("#club_history").blur();
		$("#historyRE").attr('class','hide');
		$("#history_textM").attr('class','hide');
	}
	
	if(window.club_nameE && window.estdyrE && window.genderE && window.num_teamsE && window.ground_nameE && window.street_nameE && window.telephoneE && window.mobile_noE && window.trainingE && window.age_group && window.team_gender && window.club_historyE && true){/* window.postcodeE && window.mobile_noEx && */
		adminNoX(function(MobVal){
			mobile_noEx = MobVal;
			checkPostcode(function(PostVal){
				postcodeE = PostVal;
				if(MobVal && PostVal && true)
				{
					$("#clubForm").submit();
				}
			});
		});
	}
	else{
		$("#submitClubInfo").prop('disabled', false);
	}
}