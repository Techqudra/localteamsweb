$(document).ready(function(){
	
	$('#club-reg-photo-upload').click(function() {	//PreviewImage
		if($(this).css('background-image') == "none") {		
			$('#club-logo-upload').click(); //inputbox id
		}
			// $('#imageUpload').modal('show');
	});

	$("#club-logo-upload").change(function(){
		PreviewImage("club-logo-upload", "club-reg-photo-upload");	
		$('#club-reg-photo-upload').css('background-color', 'transparent');
		//PreviewImage("club-logo-upload", "club-logo-preview");	
	});

	$('#club-reg-cover-upload').click(function() {
		if($(this).css('background-image') == "none") {		
			$('#club-cover-upload').click();
			$('#club-logo-preview-before').hide();
		}
			// $('#imageUpload').modal('show');
	});

	$("#club-cover-upload").change(function(){
		PreviewImage("club-cover-upload", "club-reg-cover-upload");	
		$('#club-reg-cover-upload').css('background-color', 'transparent');
		if($('#mobileTest').is(':visible')) {
			$('#club-reg-cover-upload').css('height', '140px');
		}		
		if($('#tabletTest').is(':visible')) {		
			$('#club-reg-cover-upload').css('height', '350px');
		}
		//$('#club-logo-preview').css('display', 'block');		
	});

	$('#club-reg-team-upload').click(function() {
		if($(this).css('background-image') == "none") {		
			$('#club-team-upload').click();
		}
		// $('#imageUpload').modal('show');
	});
	$("#club-team-upload").change(function(){
		PreviewImage("club-team-upload", "club-reg-team-upload");
		$('#club-reg-team-upload').css('background-color', 'transparent');
		if($('#mobileTest').is(':visible')) {
			$('#club-reg-team-upload').css('height', '140px');
		}		
		if($('#tabletTest').is(':visible')) {		
			$('#club-reg-team-upload').css('height', '350px');
		}	
		if($('#desktopTest').is(':visible')) {		
			$('#club-reg-team-upload').css('height', '170px');
		}			
	});

	$('#club-reg-photo-upload').dblclick(function() {
		$(this).css('background-image', 'none');		
		$('#club-logo-upload').click();		
		
	});

	$('#club-reg-cover-upload').dblclick(function() {
		$(this).css('background-image', 'none');		
		$('#club-cover-upload').click();
		$('#club-logo-preview-before').hide();
	});
	$('#club-reg-team-upload').dblclick(function() {
		$(this).css('background-image', 'none');	
		$('#club-team-upload').click();
	});



	//preview images for registration
	function PreviewImage(fieldID, previewID) {
			var oFReader = new FileReader();
			oFReader.readAsDataURL(document.getElementById(fieldID).files[0]);

			oFReader.onload = function (oFREvent) {
				// console.log(oFREvent.target.result);
				$('#' + previewID).css('background-image', 'url(' + oFREvent.target.result + ')');
				//$('#' + previewID).css('background-size', 'cover');
				$('#' + previewID).css('border-style', 'none');
				$('#' + previewID + " div:not(#club-logo-preview)").hide();
				
				if(previewID == "club-reg-team-upload") {
					$('#club-reg-team-upload').addClass('team-done');
				}
				if(previewID == "club-reg-photo-upload") {
					$('#club-reg-photo-upload').css('width', '140px');
				}
				if(previewID == "player-reg-photo-upload") {
					$('#player-reg-photo-upload').css('width', '140px');
				}			
			};
		};
});