# coding=utf-8
# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.files.images import get_image_dimensions
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.db import IntegrityError, connection
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response, redirect
from django.utils.encoding import smart_str, smart_unicode
from django.views.decorators import csrf
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.template.loader import render_to_string, get_template
from django.template import Context

from views_web import push_notification_android_web, push_notification_ios_web
from models import *

import datetime
import json
import random
import string
import time
import urllib
import urllib2

from ipware.ip import get_ip
from json import loads, dumps
from operator import itemgetter
from pyfcm import FCMNotification
from subprocess import call

defaultLogo = settings.DEFAULTLOGO
defaultPlayer = settings.DEFAULTPLAYER
ICO = settings.ICO
PATH = settings.PROJECT_ROOT
absAppPath = settings.APP_ROOT

iOS_app = "https://www.apple.com/in/itunes/charts/free-apps/"
google_play_app = "https://play.google.com/store?hl=en"

def now():
	'''Provides current datetime'''
	return str(datetime.datetime.now()).split('.')[0]
	
def e_otp():
	''' Generates 6 digit random OTP for authenticating user after signing up '''
	return ''.join(random.sample(string.digits,6))
	# return '123456'

def reset_string():
	''' Generates a 32 characters string for reseting password '''
	return ''.join(random.sample(string.uppercase+string.lowercase+string.digits,32))

def get_client_ip(request):
	''' Get IP of client '''
	xff = str(request.META['HTTP_X_FORWORDED_FOR'])
	return xff
	
def rgb2hex(rgb_color):
	''' Convert colour code from rgb to hex '''
	list = rgb_color.replace('rgb(','').replace(')','').split(',')
	r = int(list[0])
	g = int(list[1])
	b = int(list[2])
	hex = "#{:02x}{:02x}{:02x}".format(r,g,b)
	return hex

def hex2rgb(hexcode):
	''' convert colour code from hex to rgb '''
	rgb = tuple(map(ord,hexcode[1:].decode('hex')))
	return rgb

@csrf_exempt
def defaultView(request):
	''' Provide common required data like background images, etc. '''
	profile_list = Roles.objects.all()
	gender_list = Gender.objects.all()
	images_list = BackImages.objects.filter(hide=False)
	age_list = AgeGroups.objects.all().order_by('id')
	genderL=[]
	imagesLindex = []
	ageL = []
	GlobalDict = {}
	
	for age in age_list:
		ageL.append(age.age_group)
	
	for image in images_list:
		GlobalDict['id']=image.id
		GlobalDict['image_url']=image.image_url
		if image.image_name:
			GlobalDict['description']= image.image_name
			GlobalDict['rel_url']= str(image.rel_url)
		else:
			GlobalDict['description'] = ''
			GlobalDict['rel_url'] = '#'
		imagesLindex.append(GlobalDict)
		GlobalDict={}

	for gen in gender_list:
		GlobalDict['gen_id']=gen.id
		GlobalDict['gen_abbv']=gen.gen_abbv
		GlobalDict['gender']=gen.gender.title()
		genderL.append(GlobalDict)
		GlobalDict={}
	
	defaultData = {'profile_list':profile_list,'imagesLindex':json.dumps(imagesLindex),'ageL':json.dumps(ageL),'genderL':genderL}
	return defaultData

def sendMail(activation_code, email):
	''' Sending activation code for authentication via email '''
	subject = "Comfirmation of email-id"
	activation_code = str(activation_code)
	activation_code = str(activation_code[0])+" "+str(activation_code[1])+" "+str(activation_code[2])+" "+str(activation_code[3])+" "+str(activation_code[4])+" "+str(activation_code[5])
	ctx = {'email':email,'activation_code':activation_code}
	message = get_template('email_template.html').render(Context(ctx))
	email_msg = EmailMessage(subject, message, to=[email], from_email=settings.EMAIL_HOST_USER)
	email_msg.content_subtype = 'html'
	response = email_msg.send()
	return str(response)
	
	
@csrf_exempt
def reset_pass_mail(request):
	''' Sends reset passwrd link via email '''
	if 'email' in request.POST:
		email = request.POST['email']
		user = Users.objects.filter(email = email)
		if user.exists():
			string32 = reset_string()
			users = user[0]
			users.forgotten_password_code = string32
			users.save()

			link = "https://local-teams.com/reset_pass/"+str(string32)
			subject = "Reset password"
			ctx = {'email':email,'link':link}
			message = get_template('reset_pass_mail.html').render(Context(ctx))
			email_msg = EmailMessage(subject, message, to=[email], from_email="support@local-teams.com")
			email_msg.content_subtype = 'html'
			response = email_msg.send()
			status = True
		else:
			status = False
		defaultData = defaultView(request)
		data = {'status':status,'email':email,'images_list':defaultData['imagesLindex'],'title':"Password Reset"}
		return render(request,'forget_pass.html',data)
	return HttpResponseRedirect('/')
	
@csrf_exempt
def contact_us_mail(request):
	''' Sending queries to admin '''
	name = request.POST['name']
	email = request.POST['email']
	details = request.POST['details']
	subject = "Contact Us: Query by - "+str(name)
	message = str(details)
	# email_msg = EmailMessage(subject, message, to=["support@local-teams.com"], from_email=str(email))
	email_msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER], from_email=str(email))
	response = email_msg.send()
	return HttpResponseRedirect('/')

@csrf_exempt		
def push_notification_web(get_data,token_hex):
	''' Sending push notifications on browser '''
	api_key= settings.FCM_API_KEY
	push_service = FCMNotification(api_key = api_key )
	message_title = "Local Teams"
	message_body = get_data
	result = push_service.notify_single_device(registration_id=token_hex, message_body=message_body)
	return result

@csrf_exempt
def updateUserToken(request):
	''' Updating browser token provided by FCM '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		users_id = request.session['users_id']
		token = request.POST['token']
		# print token
		users = Users.objects.get(id = users_id)
		if users.browser_token != token:
			username = str(users.email).split('@')[0]
			if users.browser_token:
				msg = 'Hello %s, Welcome Back to LocalTeams'%username
				users.browser_token = token
				users.source_device = 'W'
				users.updated_by = users.id
				users.updated_at = now()
				users.save()
			else:
				msg = 'Welcome %s, LocalTeams likes to send you notification.'%username
				users.browser_token = token
				users.source_device = 'W'
				users.created_by = users.id
				users.created_at = now()
				users.save()
			
			push_notification_web(msg,token)
		request.session['users_id'] = users.id
		data = {'token':token,'users':users.email}
		return JsonResponse(data)
	
@csrf_exempt
def firebase_messaging_sw_js(request):
	filename = '/static/firebase-messaging-sw.js'
	jsfile = open(absAppPath+filename, 'r')
	response = HttpResponse(content=jsfile)
	response['Content-Type'] = 'application/javascript'
	return response

@csrf_exempt
def base(request):
	''' To load Header and Footer for each webpage '''
	defaultData = defaultView(request)
	menu_head = 'Login'
	
	nt = []
	for i in range(1,21):
		nt.append(i)
	data = {'images_list':defaultData['imagesLindex'], 'gender_list':defaultData['genderL'], 'age_list':defaultData['ageL'], 'menu_head':menu_head,'nt':nt}#,'title':title}
	return render(request,'base.html', data)

@csrf_exempt
def image_upload(image_folder,image_name,byte_array,club_id):
	''' To Upload images '''
	path = settings.MEDIA_ROOT
	imgN = image_folder+'/'+image_name+'_'+str(datetime.datetime.now().strftime("%Y%m%d_%I%M%S"))+'-'+str(club_id)+'.jpg'
	destination = open(path+'/'+imgN, 'wb')
	destination.write(byte_array.decode('base64'))
	destination.close()
	return imgN

@csrf_exempt
def clubRegistration(request):
	''' To load club registration form '''
	defaultData = defaultView(request)
	menu_head = 'Settings'
	title = 'Registration'
	nt = []
	for i in range(1,21):
		nt.append(i)
	data = {'images_list':defaultData['imagesLindex'], 'gender_list':defaultData['genderL'], 'age_list':defaultData['ageL'], 'menu_head':menu_head,'nt':nt, 'title':title, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'club-registration.html', data)
	#, 'users_id':kwargs['users_id']}

@csrf_exempt
def loginMobile(request):
	''' Mobile login view '''
	if 'HTTP_REFERER' in request.META:
		navigate = str(request.META['HTTP_REFERER'].split('/',3)[-1])
		if navigate == 'loginMobile/' or navigate == 'login/':
			navigate = ''
		print 'navigate - ',navigate
		request.session['navigate'] = navigate
		defaultData = defaultView(request)
		return render(request, 'login.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login"})
	else:
		return HttpResponseRedirect('/')
	
@csrf_exempt
def clubFix(request):
	''' To load fixture page '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		uid = request.session['users_id']
		user = Users.objects.get(id = uid)
		if user.email_verified:
			if 'club_id' not in request.session:
				return HttpResponseRedirect('/clubRegistration/')
			else:
				menu_head = "Settings"
				title = "Club Fixtures"
				tab = "Fixtures"
				club = request.session['club_id']
				club = Clubs.objects.get(id = club)
				ground = Grounds.objects.filter(id = club.ground_id)
				ground = ground[0]
				clubteams = ClubTeams.objects.filter(club = club, is_active = 1).order_by('team_name')
				teamCount = clubteams.count()
				defaultData = defaultView(request)
				if 'msg' in request.session:
					msg = request.session['msg']
					del request.session['msg']
				else:
					msg=''
				return render(request,'club_fixture.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Settings", 'msg':msg,'club':club,'ground':ground,'clubteams':clubteams,'title':title,'tab':tab,'teamCount':teamCount,'iOS_app':iOS_app, 'google_play_app':google_play_app})
		else:
			return HttpResponseRedirect('/emailV/')

@csrf_exempt
def createFix(request):
	''' For creating new fixture '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		users_id = request.session['users_id']
		club = request.session['club_id']
		club = Clubs.objects.get(id = club)
		request.session['club_id'] = club.id
		rgb_color = str(club.strip_colour)
		manager_list = []
		if rgb_color:
			hex_color = rgb2hex(rgb_color)
		else:
			hex_color = str("#939496")
		fixLabel = request.POST['fixLabel']
		fixDate = request.POST['koDate']
		fixDate = datetime.datetime.strptime(fixDate, '%d-%m-%Y').strftime('%Y-%m-%d')
		fixTime = request.POST['koTime']
		fixTime = datetime.datetime.strptime(fixTime, '%I:%M %p').strftime('%H:%M')
		fixDetails = request.POST['matchDetails']
		fixPlayers = request.POST['players']
		fixPlayers = fixPlayers.split(',')
		created_by = users_id
		
		fixE = Fixtures.objects.filter(fixture = fixLabel, fixture_date = fixDate, fixture_time = fixTime, fixture_visibility = True).exists()
		if fixE:
			request.session['msg'] = "This fixture is already created, you can not create fixtures with same data"
			return HttpResponseRedirect('/clubFix/')
		else:		
			create_fix = Fixtures(club = club, fixture = fixLabel[:199], fixture_date = fixDate, fixture_time = fixTime, fixture_venue = fixDetails, created_at = now(), resend_at = now(), created_by = created_by)
			create_fix.save()
			
			fixture_name = create_fix.fixture
			fixture_date = str(datetime.datetime.strftime( datetime.datetime.strptime(str(create_fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
			fixture_time = str(datetime.datetime.strftime(datetime.datetime.strptime(str(create_fix.fixture_time), "%H:%M"), "%I:%M %p"))
			kick_off = fixture_date+" @ "+fixture_time
			
			''' For sending push notification player to seleted players for Fixture '''
			for player in fixPlayers:
				player_data = Players.objects.get(id = player)
				cb_tm = ClubTeams.objects.get(id = player_data.club_team_id)
				manager_list.append(cb_tm.club_manager_id)
				usertokenE = UsersToken.objects.filter(player_id = player_data.id).exists()
				squad = Squad(club_team = player_data.club_team, fixture = create_fix, player = player_data, created_by = created_by, created_at = now())
				squad.save()
				get_data = {}
				get_data["fixture_name"] = fixture_name
				get_data["body"] = "Please confirm your availability for "+create_fix.fixture
				get_data["click_action"] = "av"
				get_data["tag"] = "availability"
				get_data["strip_color"] = hex_color
				get_data["club_name"] = club.club_name
				get_data["tab"] = "av"
				get_data["id"] = str(create_fix.id)
				get_data["kick_off"] = kick_off
				get_data["fixture_date"] = fixture_date
				get_data["match_details"] = create_fix.fixture_venue
				if usertokenE:
					usertoken = UsersToken.objects.get(player_id = player_data.id)
					token_hex = usertoken.device_token
					get_data["userType"] = usertoken.usertype
					source = usertoken.source_device
					is_notification_block = usertoken.is_notification_block
					get_data["usertoken"] = usertoken.id
					if token_hex:
						# sound = usertoken.notification_tone
						# if sound:
							# get_data['sound'] = sound
						# else:
							# get_data['sound'] = "Default"
						if source == "i":
							if is_notification_block == 0:
								dataI = push_notification_ios_web(get_data,token_hex)
						if source == "a" :
							if is_notification_block == 0:
								dataA = push_notification_android_web(get_data,token_hex)
						squad.notification = "1"
						squad.save()
				
			''' For sending push notification to manager/s of players who are invited for the fixture '''
			managers = list(sorted(set(manager_list)))
			for manager in managers:
				usertoken = UsersToken.objects.filter(manager_id = manager)
				if usertoken.exists():
					get_data = {}
					get_data["fixture_name"] = fixture_name
					get_data["body"] = "New fixture "+fixture_name+" on "+kick_off
					get_data["click_action"] = "rv"
					get_data["tag"] = "review"
					get_data["strip_color"] = hex_color
					get_data["club_name"] = club.club_name
					get_data["tab"] = "rv"
					get_data["id"] = str(create_fix.id)
					get_data["kick_off"] = kick_off
					get_data["fixture_date"] = fixture_date
					get_data["match_details"] = create_fix.fixture_venue
					usertoken = usertoken[0]
					token_hex = usertoken.device_token
					get_data["userType"] = usertoken.usertype
					source = usertoken.source_device
					is_notification_block = usertoken.is_notification_block
					get_data["usertoken"] = usertoken.id
					if token_hex:
						if source == "i":
							if is_notification_block == 0:
								dataI = push_notification_ios_web(get_data,token_hex)
						if source == "a" :
							if is_notification_block == 0:
								dataA = push_notification_android_web(get_data,token_hex)
					
			return HttpResponseRedirect('/clubNotif/')

@csrf_exempt
def clubNotif(request, pk=None):
	''' To load fixtures on review tab '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		uid = request.session['users_id']
		user = Users.objects.get(id = uid)
		if user.email_verified:			
			if 'club_id' not in request.session:
				return HttpResponseRedirect('/clubRegistration/')
			else:
				club_id = request.session['club_id']
				defaultData = defaultView(request)
				tab = "Notification"
				menu_head = "Settings"
				title = "Club Notification"
				defaultData = defaultView(request)
				teams = ClubTeams.objects.filter(club_id = club_id, is_active = 1).values_list('team_name','id')
				positions = FootballPlayerPositions.objects.all()
				club_data = Clubs.objects.get(id=club_id)
				fixture_data = Fixtures.objects.filter(club_id = club_id, fixture_visibility = True).order_by('-fixture_date','fixture_time','created_at')
				if pk is not None:
					FixEx = Fixtures.objects.filter(id = pk).exists()
					if FixEx:
						notification = Notifications.objects.filter(fixture_id = pk, is_visible = bool(1))
						for noti in notification:
							noti.read = bool(1)
							noti.updated_at = now()
							noti.save()
					else:
						pk = None
					
				return render(request,'club_notification.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'teams' : teams,'response':club_data,'positions': positions,'title':title,'tab':tab,'fixture_data':fixture_data,'fix_id':pk,'iOS_app':iOS_app, 'google_play_app':google_play_app})
		else:
			return HttpResponseRedirect('/emailV/')

@csrf_exempt		
def squadForFixture(request):
	''' To load data of fixture like list of players, each player's status on review tab '''
	GlobalDict ={}
	squadList = []
	fix_id = request.POST['fix_id']
	fixture = Fixtures.objects.get(id = fix_id)
	squad_fix = Squad.objects.filter(fixture_id = fix_id)
	for fix in squad_fix:
		player = Players.objects.get(id = fix.player_id)
		userstokenE = UsersToken.objects.filter(player = player).exists()
		position = FootballPlayerPositions.objects.get(id = player.player_position_id)
		
		if player.player_img == '':
			player_img = defaultPlayer
		else:
			player_img = str(player.player_img.url)
		
		GlobalDict['player_id']= str(fix.player_id)
		GlobalDict['player_img'] = player_img
		GlobalDict['player_name']= player.player_name
		GlobalDict['player_position']= str(position.position)
		GlobalDict['player_position_id']= str(position.id)
		# GlobalDict['player_status']= str(fix.status_id) if userstokenE else "4"
		if fix.status_id not in [1,2]:
			if userstokenE:
				GlobalDict['player_status']	= "3"
			else:
				GlobalDict['player_status']	= "4"
		else:
			GlobalDict['player_status'] = fix.status_id
		squadList.append(GlobalDict)
		GlobalDict ={}
	fixture_name = fixture.fixture
	fix_confirm = str(fixture.fixture_confirm)
	resend = str(fixture.resend)
	squadList = sorted(squadList, key=lambda k: k['player_position_id']) #to sort list by position
	len_squadlist = len(squadList)
	data = {"squadList" : squadList,"len_squadlist":len_squadlist,"fixture_name":fixture_name,"resend":resend,"is_confirm":fix_confirm}
	return JsonResponse(data)

@csrf_exempt
def clubSquad(request):
	''' To load sqaud page '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		uid = request.session['users_id']
		user = Users.objects.get(id = uid)
		if user.email_verified:
			if 'club_id' not in request.session:
				return HttpResponseRedirect('/clubRegistration/')
			else:
				if 'msg' in request.session:
					menu_head = "Settings"
					title = "Club Squad"
					tab = "Squad"
					defaultData = defaultView(request)
					club_id = request.session['club_id']
					teams = ClubTeams.objects.filter(club_id = club_id, is_active = 1).values_list('team_name','id').order_by('team_name')
					teamCount = teams.count()
					positions = FootballPlayerPositions.objects.all().order_by('id')
					club_data = Clubs.objects.filter(id=club_id)[0]
					msg = request.session['msg']
					del request.session['msg']
					return render(request,'club_squad.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'teams' : teams,'response':club_data,'positions' : positions,'title':title,'tab':tab,'msg': msg,'teamCount':teamCount})
				else:
					menu_head = "Settings"
					title = "Club Squad"
					tab = "Squad"
					defaultData = defaultView(request)
					club_id = request.session['club_id']
					teams = ClubTeams.objects.filter(club_id = club_id, is_active = 1).values_list('team_name','id').order_by('team_name')
					teamCount = teams.count()
					positions = FootballPlayerPositions.objects.all().order_by('id')
					club_data = Clubs.objects.filter(id=club_id)[0]
					return render(request,'club_squad.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'teams' : teams,'response':club_data,'positions' : positions,'title':title,'tab':tab,'teamCount':teamCount,'iOS_app':iOS_app, 'google_play_app':google_play_app})
		else:
			return HttpResponseRedirect('/emailV/')
			
@csrf_exempt
def login(request):
	''' To get logged in '''
	if 'users_id' in request.session:
		return HttpResponseRedirect('/')
	else:
		if 'HTTP_REFERER' in request.META:
			navigate = str(request.META['HTTP_REFERER'].split('/',3)[-1])
			if navigate == 'loginMobile/':
				navigate = request.session['navigate']
			defaultData = defaultView(request)
			if 'emailL' in request.POST:
				email = request.POST['emailL']
				password = request.POST['passL']
				data = Users.objects.filter(username = email, password = password).exists()
				if data:
					data = Users.objects.get(username = email, password = password)
					request.session['users_id'] = data.id
					is_club = Clubs.objects.filter(users = data).exists()
					if data.email_verified:			
						if is_club:
							club = Clubs.objects.filter(users_id = data.id).values_list('id',flat=True)[0]
							request.session['club_id'] = club
							if (navigate != ''):
								return HttpResponseRedirect('/'+str(navigate))
							else:
								return HttpResponseRedirect('/clubProfile/')
						else:
							if (navigate != ''):
								return HttpResponseRedirect('/'+str(navigate))
							else:
								return HttpResponseRedirect('/clubRegistration/')
					else:
						if is_club:
							club = Clubs.objects.filter(users_id = data.id).values_list('id',flat=True)[0]
							request.session['club_id'] = club
						activation_code = str(e_otp())
						response = sendMail(activation_code, email)
						data.activation_code = activation_code
						data.save()
						return HttpResponseRedirect('/emailV/')
					# return render(request, 'Test.html', {'email_verified':data.email_verified,'session':request.session['users_id']})
				else:
					msg = "Try 'Login' with valid credentials or 'Forgotten password?' "
					return render(request,'login.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login", 'msg':msg, 'email':email})
			else:
				msg = "Please Login OR Register First"
				return render(request,'login.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login",'msg':msg})
		else:
			return HttpResponseRedirect('/')

@csrf_exempt
def index(request):
	''' Initial webpage '''
	# # ip = get_ip(request)
	# ip = str(request)
	# print 'ip ',ip
	defaultData = defaultView(request)
	if 'users_id' not in request.session:
		menu_head = 'Login'
		data = {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'tab':''}
		return render(request, 'index.html', data)
	else:
		users_id = request.session['users_id']
		user = Users.objects.filter(id = users_id)
		if user.exists():
			user = user[0]
			# return HttpResponse(str(users_id))
			if user.email_verified:
				menu_head = 'Settings'
				return HttpResponseRedirect('/clubProfile/')
			else:
				return HttpResponseRedirect('/emailV/')
		else:
			return HttpResponseRedirect('/logout/')

@csrf_exempt
def signUpUser(request):
	''' Store new user '''
	if 'country' in request.POST:
		defaultData = defaultView(request)
		if 'email' in request.POST:
			email = request.POST['email']
			username = request.POST['email']
			password = request.POST['pass']
			country = request.POST['country']
			usertype = request.POST['usertype']
			updates = request.POST.getlist('updates[]')
			if updates:
				notify = True
			else:
				notify = False
			activation_code = str(e_otp())
			exE = Users.objects.filter(email = email).exists()
			if exE:
				msg = "Email Already Exists...Try Login or SignUp with another email"
				return render(request, 'index.html',  {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login", 'exists':exE, 'msg':msg})
			else:
				countryE = Country.objects.filter(country_name = country).exists()
				countryT = Country.objects.filter(country_name = country.title()).exists()
				if countryE:
					country = Country.objects.get(country_name = country)
				elif countryT:
					country = Country.objects.get(country_name = country.title())
				else:
					return render(request, 'index.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login", 'msg':"Please Select country shown in auto-complete"})
					
				response = sendMail(activation_code, email)
				
				dataI = Users(email = email, username = username, password = password, usertype_id = usertype, country =country, activation_code = activation_code, update_notifications = notify)
				
				dataI.save()
				users_id = dataI.id
				request.session['users_id'] = users_id
				# # return HttpResponseRedirect('/clubRegistration/')
				return HttpResponseRedirect('/emailV/')
		else:
			msg = "Please Login OR Register First"
			return render(request, 'login.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login", 'msg':msg,'tab':''})
	else:
		return HttpResponseRedirect('/')

@csrf_exempt
def validCountry(request):
	""" AJAX """
	''' Check if entered country is valid or not '''
	q = request.POST['country']
	valid = Country.objects.filter(country_name=q.title()).exists()
	data = {'valid':valid}
	return JsonResponse(data)

@csrf_exempt
def validPostcode(request):
	""" AJAX """
	''' Check if entered postal code is valid or not '''
	q = request.POST['postcode']
	valid = PostalCodes.objects.filter(postcode=q.upper()).exists()
	data = {'valid':valid}
	return JsonResponse(data)		

@csrf_exempt
def emailAvail(request):
	""" AJAX """
	''' Check if entered email is already exists or not '''
	q = request.POST['email']
	valid = Users.objects.filter(email=q).exists()
	data = {'valid':valid}
	return JsonResponse(data)

@csrf_exempt
def userAvail(request):
	""" AJAX """
	''' Check if provided user credentials are valid or not '''
	email = request.POST['usernameLD']
	password = request.POST['passwordLD']
	valid = Users.objects.filter(username = email, password = password).exists()
	data = {'valid':valid}
	return JsonResponse(data)

@csrf_exempt
def autocompleteCity(request):
	""" AJAX """
	''' Provide city name matching with entered characters '''
	city = request.GET.get('term', '')
	results = []
	city_list = City.objects.filter(city_name__startswith=city.title()).order_by('city_name')
	for cityn in city_list:
		results.append(cityn.city_name+', '+str(cityn.county_name))
	data = json.dumps(results)
	mimetype = 'application/json'
	return HttpResponse(data,mimetype)


@csrf_exempt
def autocompleteCountry(request):
	""" AJAX """
	''' Provide country name matching with entered characters '''
	q = request.GET.get('term', '')
	results = []
	country_list = Country.objects.filter(country_name__startswith=q.title()).order_by('country_name')
	for coutry in country_list:
		results.append(coutry.country_name)
	data = json.dumps(results)
	mimetype = 'application/json'
	return HttpResponse(data,mimetype)

@csrf_exempt
def autocompletePostCodes(request):
	""" AJAX """
	''' Provide postal codes matching with entered characters '''
	q = request.GET.get('term', '')
	if q:
		results = []
		postal_list = PostalCodes.objects.filter(postcode__startswith=q.upper())
		for postC in postal_list:
			results.append(str(postC.postcode))
		results.sort()	
		data = json.dumps(results)
		mimetype = 'application/json'
		return HttpResponse(data,mimetype)
	else:
		"Please Provide data..."
	
@csrf_exempt
def autocompleteStreet(request):
	""" AJAX """
	''' Provides street name for entered postal code '''
	'''	either from local database (if available) '''
	'''	or from CraftyClicks (if not available in local database) '''
	key = settings.CRAFTYCLICKS_KEY #by Jass
	postcode = request.POST["postcode"]
	is_avail = StreetData.objects.filter(postcode = postcode).exists()
	street_namesL = []
	if not is_avail:
		url = settings.CRAFTYCLICKS_URL
		params = {"postcode": postcode,"key": key,"response": "data_formatted"}
		req = urllib2.Request(url + '?' + urllib.urlencode(params))
		try:
			res = urllib2.urlopen(req)
		except:
			pass
		response = json.loads(res.read())
		if 'error_msg' in response:
			data = "Sorry no data found"
		else:
			del_pts = response["delivery_points"]
			for lines in del_pts:
				street = str(lines["line_1"])+" "+str(lines["line_2"])
				street_data = StreetData(postcode = postcode, street_name = street.title(), line_1 = str(lines["line_1"]).title(), line_2 = str(lines["line_2"]).title(), organisation_name = str(lines["organisation_name"]).title(), department_name = str(lines["department_name"]).title(), postal_county = str(response["postal_county"]), traditional_county = str(response["traditional_county"]), town = str(response["town"]) )
				street_data.save()
	
	street_nameObj = StreetData.objects.filter(postcode = postcode).distinct('street_name')
	for street in street_nameObj:
		street_namesL.append(str(street.street_name))
	
	street_namesL.sort()
	data = json.dumps(street_namesL)
	mimetype = 'application/json'
	return HttpResponse(data,mimetype)
	
@csrf_exempt
def autocompleteAge(request):
	""" AJAX """
	''' Loads age group dropdown according to entered number of teams '''
	q = int(request.GET.get('term', ''))
	GlobalDict = {}
	GlobalList = []
	if 0 < q < 7:
		id = 14+(q-1)
		age_list = AgeGroups.objects.filter(id__lte = id).order_by('description')
	else:
		age_list = AgeGroups.objects.all().order_by('description')
	
	if q == 1 or q == 0:
		for age in age_list:
			GlobalDict["age_group"] = age.age_group
			GlobalDict["age_group_id"] = age.id
			GlobalList.append(GlobalDict)
			GlobalDict = {}
	else:
		for age in age_list:
			if age.id == 2:
				GlobalDict["age_group"] = 'Adults A'
				GlobalDict["age_group_id"] = age.id
				GlobalList.append(GlobalDict)
				GlobalDict = {}
			else:
				GlobalDict["age_group"] = age.age_group
				GlobalDict["age_group_id"] = age.id
				GlobalList.append(GlobalDict)
				GlobalDict = {}
	
	return JsonResponse(GlobalList, safe = False)

@csrf_exempt
def postcodeAutofilled(request):
	""" AJAX """
	''' Loads City, County, Country for the provided postalcode '''
	postcode = request.POST['pc']
	if postcode is None:
		data = {}
	else:
		postE = PostalCodes.objects.filter(postcode = postcode).exists()
		if postE:
			postcode = PostalCodes.objects.get(postcode = postcode)
			city = int(postcode.city_id_id)
			county = postcode.county
			country = postcode.country_id_id
			postal_town = postcode.postal_town
			countryE = Country.objects.get(id = country)
			cityE = City.objects.filter(id = city).exists()
			if cityE:
				city = City.objects.get(id = city)
				city_name = city.city_name
			else:
				city_name = ''
			data = {'city' : str(postcode.city_id),'city_name':city_name,'county':county,'country':countryE.country_name,'postal_town':postal_town}
		else:
			data = {}
	return JsonResponse(data)

@csrf_protect
def club_registration(request):
	''' Stores club data from registration form '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		users = Users.objects.get(id = request.session['users_id'])
		is_club = Clubs.objects.filter(users = users).exists()
		if is_club:
			return HttpResponseRedirect('/clubProfile/')
		else:
			age_groups = ''
			club_name = request.POST['club_name'].upper()
			club_reg_strip = request.POST['club_reg_strip']
			estdyr = request.POST['estdyr']
			admin_mobile_no = request.POST['admin_mobile_no']
			gender = request.POST['gender']
			num_teams = request.POST['num_teams']
			league_table = request.POST['league_table']
			fixture_link = request.POST['fixture_link']
			website_link = request.POST['website_link']
			ground_name = request.POST['ground_name']#
			postcode = request.POST['postcode']#
			street_name = request.POST['street_name']#
			telephone = request.POST['telephone']#
			town_name = request.POST['town_name']#
			county_name = request.POST['county_name']#
			country_name = request.POST['country_name']#
			training = request.POST['training']
			club_history = request.POST['club_history']
			home_kit = json.loads(request.POST['home_kit'])
			away_kit = json.loads(request.POST['away_kit'])
			club_cover = request.POST['club_cover']
			club_logo = request.POST['club_logo']
			club_team = request.POST['club_team']
			
			users.mobile_no = admin_mobile_no
			users.save()
			
			i=0
			age_groups += '['
			clb_tm_genId = []
			clb_tm_genNm = []
			age_id_List = []
			age_obj_List = []
			if gender == "3":
				for i in range(int(num_teams)):
					if(request.POST['age_groups_'+str(i+1)] != ""):
						age_id_List.append(int(request.POST['age_groups_'+str(i+1)]))
						clb_tm_genId.append( int( request.POST['CT_gender_'+str(i+1)] ) )
						ag = AgeGroups.objects.get(id = int(request.POST['age_groups_'+str(i+1)]))
						gen = Gender.objects.get(id = int(request.POST['CT_gender_'+str(i+1)]))
						clb_tm_genNm.append(str(gen.gender.title()))
						if request.POST['age_groups_'+str(i+1)] == '2':
							if str(num_teams) >= "2":
								age_obj_List.append(ag)
								agegrp = str(ag.age_group)+" A"
							else:
								age_obj_List.append(ag)
								agegrp = str(ag.age_group)
						else:
							age_obj_List.append(ag)
							agegrp = str(ag.age_group)
						if i == 0:
							age_groups += '"' + agegrp + ' ('+ gen.gender.title() + ') "'
						else:
							age_groups += ',"' + agegrp + ' ('+ gen.gender.title() + ') "'
			else:
				for i in range(int(num_teams)):
					age_id_List.append(int(request.POST['age_groups_'+str(i+1)]))
					clb_tm_genId.append( int( gender ) )
					ag = AgeGroups.objects.get(id = int(request.POST['age_groups_'+str(i+1)]))
					gen = Gender.objects.get(id = int(gender))
					clb_tm_genNm.append(str(gen.gender.title()))
					
					if request.POST['age_groups_'+str(i+1)] == '2':
						if str(num_teams) >= "2":
							age_obj_List.append(ag)
							agegrp = str(ag.age_group)+" A"
						else:
							age_obj_List.append(ag)
							agegrp = str(ag.age_group)
					else:
						age_obj_List.append(ag)
						agegrp = str(ag.age_group)
					if i == 0:
						age_groups += '"' + agegrp + ' ('+ gen.gender.title() + ') "'
					else:
						age_groups += ',"' + agegrp + ' ('+ gen.gender.title() + ') "'
			age_groups += ']'
			
			county_name = County.objects.get(county_name = county_name)
			town_name = City.objects.get(city_name = town_name,county_name=county_name)
			country_name = Country.objects.get(country_name = country_name)
			postcode = PostalCodes.objects.get(postcode = postcode)

			home_kit = Kits(shirt_colour = home_kit['home_mid'],sleeve_colour = home_kit['home_sleeve_L'],pattern_colour = home_kit['home_pattern_color'],pattern_mode = home_kit['home_pattern'])
			home_kit.save()
			
			away_kit = Kits(shirt_colour = away_kit['away_mid'],sleeve_colour = away_kit['away_sleeve_L'],pattern_colour = away_kit['away_pattern_color'],pattern_mode = away_kit['away_pattern'])
			away_kit.save()
			
			grounds = Grounds(ground_name = ground_name, street = street_name, town = town_name, county = county_name, postcode = postcode, phone = telephone)
			grounds.save()
			
			club = Clubs(club_name = club_name,
						strip_colour = club_reg_strip,
						established = estdyr,
						gender_id = gender,
						# num_teams = num_teams,
						league_table_url = league_table,
						fixture_table_url = fixture_link,
						website_url = website_link,
						ground = grounds,
						training_text = training,
						history_text = club_history,
						country_name = country_name,
						city_name = town_name,
						county_name = county_name,
						home_kit = home_kit,
						away_kit = away_kit,
						latitude = postcode.latitude,
						longitude = postcode.longitude,
						age_groups = age_groups,
						users = users);
			club.save()
			if club_cover:
				club_cover = request.POST['club_cover'].split('base64,', 1 )[1]
				club_cover = image_upload('club_covers',"cover",club_cover,club.id)
			if club_logo:
				club_logo = request.POST['club_logo'].split('base64,', 1 )[1]
				club_logo = image_upload('club_logos',"logo",club_logo,club.id)
			if club_team:
				club_team = request.POST['club_team'].split('base64,', 1 )[1]
				club_team = image_upload('club_team',"team",club_team,club.id)
			
			club.cover_photo_url = club_cover
			club.logo_photo = club_logo
			club.team_photo = club_team
			club.save()
			request.session['club_id'] = club.id

			club.num_teams = len(age_id_List)
			club.save()
	
			age_groups = json.loads(club.age_groups)
			i=0
			for i in range(int(club.num_teams)):
				clubteams = ClubTeams(team_name = age_groups[i],club = club, agegroup = age_obj_List[i], gender = clb_tm_genNm[i])
				clubteams.save()
			
		return HttpResponseRedirect('/clubProfile/')

@csrf_protect
def clubSave(request):
	''' Updates club data '''
	if 'users_id' not in request.session:
		return HttpResponseRedirect('/')
	else:
		users = Users.objects.get(id = request.session['users_id'])
		club_obj = Clubs.objects.get(users = users)
		request.session['club_id'] = club_obj.id
		
		ground_obj = Grounds.objects.get(id = club_obj.ground_id)
		home_obj = Kits.objects.get(id = club_obj.home_kit_id)
		away_obj = Kits.objects.get(id = club_obj.away_kit_id)
		
		age_groups = []
		
		club_name = request.POST['club_name'].upper()
		club_reg_strip = request.POST['club_reg_strip']
		estdyr = request.POST['estdyr']
		gender = request.POST['gender']
		num_teams = request.POST['num_teams_val']
		new_added = int(request.POST['new_added'])
		removed = json.loads(request.POST['removed'])
		league_table = request.POST['league_table']
		fixture_link = request.POST['fixture_link']
		website_link = request.POST['website_link']
		ground_name = request.POST['ground_name']#
		postcode = request.POST['postcode']#
		street_name = request.POST['street_name']#
		telephone = request.POST['telephone']#
		town_name = request.POST['town_name']#
		county_name = request.POST['county_name']#
		country_name = request.POST['country_name']#
		training = request.POST['training']
		club_history = request.POST['club_history']
		home_kit = json.loads(request.POST['home_kit'])
		away_kit = json.loads(request.POST['away_kit'])
		club_cover = request.POST['club_cover']
		club_logo = request.POST['club_logo']
		club_team = request.POST['club_team']
		admin_mobile_no = request.POST['admin_mobile_no']
		
		users.mobile_no = admin_mobile_no
		users.save()

		age_groups = json.loads(club_obj.age_groups)
		age_groups_new = []
		clb_tm_genId = []
		clb_tm_genNm = []
		age_id_List = []
		age_obj_List = []

		i=0
		add = 0
		if gender == "3":
			for i in range(int(new_added)):
				if(request.POST['age_groups_'+str(i+1)] != ""):
					age_id_List.append(int(request.POST['age_groups_'+str(i+1)]))
					clb_tm_genId.append( int( request.POST['CT_gender_'+str(i+1)] ) )
					ag = AgeGroups.objects.get(id = int(request.POST['age_groups_'+str(i+1)]))
					gen = Gender.objects.get(id = int(request.POST['CT_gender_'+str(i+1)]))
					clb_tm_genNm.append(str(gen.gender.title()))
					
					if request.POST['age_groups_'+str(i+1)] == '2':
						age_obj_List.append(ag)
						agegrp = str(ag.age_group)+" A"
					else:
						age_obj_List.append(ag)
						agegrp = str(ag.age_group)
					
					new_grp = str(agegrp)+' ('+str(gen.gender.title())+')'
					age_groups.append(str(new_grp))
					age_groups_new.append(str(new_grp))
					add +=1
			age_groups=json.dumps(age_groups)
		else:
			for i in range(int(new_added)):
				if(request.POST['age_groups_'+str(i+1)] != ""):
					age_id_List.append(int(request.POST['age_groups_'+str(i+1)]))
					clb_tm_genId.append( int( gender ) )
					ag = AgeGroups.objects.get(id = int(request.POST['age_groups_'+str(i+1)]))
					gen = Gender.objects.get(id = int(gender))
					clb_tm_genNm.append(str(gen.gender.title()))
				
					if request.POST['age_groups_'+str(i+1)] == '2':
						age_obj_List.append(ag)
						agegrp = str(ag.age_group)+" A"
					else:
						age_obj_List.append(ag)
						agegrp = str(ag.age_group)
				
					new_grp = str(agegrp)+' ('+str(gen.gender.title())+')'
					age_groups.append(str(new_grp))
					age_groups_new.append(str(new_grp))
					add +=1
			age_groups=json.dumps(age_groups)

		new_added = add
		
		country_name = Country.objects.get(country_name = country_name)
		county_name = County.objects.get(county_name = county_name)
		town_name = City.objects.get(city_name = town_name, county_name = county_name)
		postcode = PostalCodes.objects.get(postcode = postcode)

		home_obj.shirt_colour = home_kit['home_mid']
		home_obj.sleeve_colour = home_kit['home_sleeve_L']
		home_obj.pattern_colour = home_kit['home_pattern_color']
		home_obj.pattern_mode = home_kit['home_pattern']
		home_obj.save()
		
		away_obj.shirt_colour = away_kit['away_mid']
		away_obj.sleeve_colour = away_kit['away_sleeve_L']
		away_obj.pattern_colour = away_kit['away_pattern_color']
		away_obj.pattern_mode = away_kit['away_pattern']
		away_obj.save()
		
		ground_obj.ground_name = ground_name
		ground_obj.street = street_name
		ground_obj.town = town_name
		ground_obj.county = county_name
		ground_obj.postcode = postcode
		ground_obj.phone = telephone
		ground_obj.save()
		
		club_obj.club_name = club_name
		club_obj.strip_colour = club_reg_strip
		club_obj.established = estdyr
		club_obj.gender_id = gender
		club_obj.num_teams = club_obj.num_teams+(new_added-len(removed))
		club_obj.league_table_url = league_table
		club_obj.fixture_table_url = fixture_link
		club_obj.website_url = website_link
		club_obj.ground = ground_obj
		club_obj.training_text = training
		club_obj.history_text = club_history
		club_obj.country_name = country_name
		club_obj.city_name = town_name
		club_obj.county_name = county_name
		club_obj.home_kit = home_obj
		club_obj.away_kit = away_obj
		club_obj.latitude = postcode.latitude
		club_obj.longitude = postcode.longitude
		club_obj.age_groups = age_groups
		club_obj.save()
		
		if club_logo != '':
			if club_obj.logo_photo == '':
				club_logo = club_logo.split('base64,', 1 )[1]
				club_logo = image_upload('club_logos',"logo",club_logo,club_obj.id)
				club_obj.logo_photo = club_logo
				club_obj.save()
			else:
				if club_obj.logo_photo.url != club_logo:
					img_name = PATH+str(club_obj.logo_photo.url)
					call(["rm", img_name])
					club_logo = club_logo.split('base64,', 1 )[1]
					club_logo = image_upload('club_logos',"logo",club_logo,club_obj.id)
					club_obj.logo_photo = club_logo
					club_obj.save()
			
		if club_cover != '':
			if club_obj.cover_photo_url == '':
				club_cover = club_cover.split('base64,', 1 )[1]
				club_cover = image_upload('club_covers',"cover",club_cover,club_obj.id)
				club_obj.cover_photo_url = club_cover
				club_obj.save()
			else:
				if club_obj.cover_photo_url.url != club_cover:
					img_name = PATH+str(club_obj.cover_photo_url.url)
					call(["rm", img_name])
					club_cover = club_cover.split('base64,', 1 )[1]
					club_cover = image_upload('club_covers',"cover",club_cover,club_obj.id)
					club_obj.cover_photo_url = club_cover
					club_obj.save()
			
		if club_team != '' :
			if club_obj.team_photo == '':
				club_team = club_team.split('base64,', 1 )[1]
				club_team = image_upload('club_team',"team",club_team,club_obj.id)
				club_obj.team_photo = club_team
				club_obj.save()
			else:
				if club_obj.team_photo.url != club_team:
					img_name = PATH+str(club_obj.team_photo.url)
					call(["rm", img_name])
					club_team = club_team.split('base64,', 1 )[1]
					club_team = image_upload('club_team',"team",club_team,club_obj.id)
					club_obj.team_photo = club_team
					club_obj.save()
		
		
		age_groups = json.loads(club_obj.age_groups)
		
		i=0
		for i in range(int(new_added)):
			clubteams = ClubTeams(team_name = age_groups_new[i],club = club_obj, agegroup = age_obj_List[i], gender = clb_tm_genNm[i])
			clubteams.save()
			
		if (len(removed)) > 0:
			n = 0
			for i in range(n, len(removed)):
				try:
					age_groups.remove(removed[i])
				except:
					pass
			
			club_obj.age_groups = json.dumps(age_groups)
			club_obj.save()
		
			n = 0
			for i in range(n, len(removed)):
				clubteams = ClubTeams.objects.filter(club=club_obj, team_name=removed[i])
				for team in clubteams:
					team.is_active = 0
					team.save()
					player = Players.objects.filter(club_team = team)
					for pl in player:
						pl.is_active = 0
						pl.save()
				
		clubteams = ClubTeams.objects.filter(club=club_obj, is_active=1)
		num_teams = clubteams.count()
		allteams = "["
		i = 1
		if num_teams > 0:
			for team in clubteams:
				if i == num_teams:
					allteams += '"'+team.team_name+'"'
				else:
					allteams += '"'+team.team_name+'" ,'
					i += 1
		allteams += "]"
		club_obj.num_teams = num_teams
		club_obj.age_groups = allteams
		club_obj.save()
			
	return HttpResponseRedirect('/clubProfile/')

@csrf_exempt
def clubEdit(request):
	''' Loads club-registration form for updations with stored data '''
	defaultData = defaultView(request)
	if 'users_id' not in request.session:
		msg = "Please Login OR Register First"
		return render(request, 'index.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':"Login", 'msg':msg, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
	else:
		title = "Club Profile"
		print request.session['users_id']
		uid = request.session['users_id']
		users = Users.objects.get(id = uid)
		gender_list = Gender.objects.all()
		genderL=[]
		GlobalDict={}
	
		for gen in gender_list:
			GlobalDict['gen_id']=gen.id
			GlobalDict['gen_abbv']=gen.gen_abbv
			GlobalDict['gender']=gen.gender.title()
			genderL.append(GlobalDict)
			GlobalDict={}
			
		data = Clubs.objects.filter(users_id = uid)
		data = data[0]
		age_group = json.loads(data.age_groups)
		agegroups = ''
		for i in range(data.num_teams-1):
			if i == 0:
				agegroups += str(age_group[i])
			else:
				agegroups += ', '+ str(age_group[i])
		num_teams = data.num_teams
		ground = Grounds.objects.filter(id = data.ground_id)
		ground = ground[0]
		homekit = Kits.objects.filter(id = data.home_kit_id)
		homekit = homekit[0]
		awaykit = Kits.objects.filter(id = data.away_kit_id)
		awaykit = awaykit[0]
		
		nt = []
		for i in range(1, 21):
			nt.append(i)
		return render(request,'club-registration.html',{'images_list':defaultData['imagesLindex'], 'response' : data,'ground' : ground,'homekit' : homekit, 'awaykit' : awaykit,'editmode':1,'num_teams':num_teams,'agegroups':agegroups,'gender_list':genderL,'menu_head':"Settings",'mobile_no':users.mobile_no,'nt':nt,'age_group':age_group,'title':title, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
		
@csrf_exempt
def XmobileP(request):
	""" AJAX """
	''' Check if entered mobile_no is already exists in player table or not '''
	mobile = request.POST['mobile']
	valid = Players.objects.filter(player_mob_no = mobile, is_active = 1).exists()
	if 'id' in request.POST:
		if valid:
			players = Players.objects.filter(player_mob_no = mobile, is_active = 1)
			for player in players:
				if str(player.id) == str(request.POST['id']):
					data = {'valid' : True}
				else:
					data = {'valid' : False}
		else:
			data = {'valid':not valid}
	else:
		data = {'valid':not valid}
	return JsonResponse(data)

@csrf_exempt
def XmobileM(request):
	""" AJAX """
	''' Check if entered mobile_no is already exists in manager table or not '''
	mobile = request.POST['mobile']
	valid = ClubManager.objects.filter(manager_mob_no = mobile).exists()
	if valid:
		managers = ClubManager.objects.get(manager_mob_no = mobile)
		if managers.manager_img:
			manager_img = managers.manager_img.url
		else:
			manager_img = defaultPlayer
		
		data = {'valid':not valid,'manager_name':str(managers.manager_name),'manager_id':managers.id,'manager_img':manager_img}
	else:
		data = {'valid':not valid}
	return JsonResponse(data)
	
@csrf_exempt
def eXAdminM(request):
	""" AJAX """
	''' Check if entered mobile_no is already exists in users table or not '''
	mobile = request.POST['mobile']
	valid = Users.objects.filter(mobile_no = mobile).exists()
	if valid:
		user = Users.objects.get(mobile_no = mobile)
		if user.id == request.session['users_id']:
			data = {'valid':valid}
		else:
			data = {'valid':not valid}
	else:
		data = {'valid':not valid}
	return JsonResponse(data)

@csrf_exempt
def eXManSave(request):
	""" AJAX """
	''' Add existing manager of a team to other team '''
	manager_id = request.POST['manager_id']
	teamid = request.POST['teamid']

	team = ClubTeams.objects.get(id = teamid)
	team.club_manager_id = manager_id
	team.save()
	msg = 'Manager successfully added for '+str(team.team_name)+'!'
	status = True
	data = {'status':status,'msg' : msg}
	return JsonResponse(data)
	
@csrf_exempt
def notificationHide(request):
	""" AJAX """
	''' Hide notification from review tab '''
	data = request.POST.getlist('fixtures[]')
	for id in data:
		Fix = Fixtures.objects.get(id = id)
		Fix.fixture_visibility = 0
		Fix.save()
		notifications = Notifications.objects.filter(fixture = Fix)
		for noti in notifications:
			noti.is_visible = bool(0)
			noti.updated_at = now()
			noti.save()
	return JsonResponse({"response":"done"}, safe= False)

@csrf_exempt 
def resend_msg(request):
	""" AJAX """
	''' Resend fixture notification to all players who haven't responded yet '''
	fixture_id = request.POST['fixture_id']
	Fix = Fixtures.objects.get(id = fixture_id)
	club = Clubs.objects.get(id = Fix.club_id)
	rgb_color = str(club.strip_colour)
	if rgb_color:
		hex_color = rgb2hex(rgb_color)
	else:
		hex_color = str("#939496")
	players = request.POST.getlist('players[]')
	GlobalDict = {}
	responseList = []
	# data = {}
	for player in players:
		sqdE = Squad.objects.filter(player_id = player, fixture_id = fixture_id, hide_fix = "f").exists()
		fix_data = Fixtures.objects.get(id = fixture_id)
		if sqdE:
			sqd = Squad.objects.get(player_id = player, fixture_id = fixture_id)
			usertokenE= UsersToken.objects.filter(player_id = player).exists()
			if  usertokenE:
				fix_data.resend = True
				fix_data.resend_at = now()
				fix_data.save()
				sqd.resend = True
				sqd.save()
				fix = Fixtures.objects.get(id = fixture_id)
				usertoken = UsersToken.objects.get(player_id = player)
				# usertoken = usertoken[0]
				token_hex = usertoken.device_token
				kick_off = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))+" @ "+str(datetime.datetime.strftime(datetime.datetime.strptime(str(fix.fixture_time), "%H:%M:%S"), "%I:%M %p"))
				get_data = {}
				get_data["fixture_name"] = fix.fixture
				get_data["body"] = "Please confirm your availability for "+fix.fixture
				get_data["strip_color"] = hex_color
				get_data["club_name"] = club.club_name
				get_data["tab"] = "av"
				get_data["tag"] = "availability"
				get_data["id"] = str(fix.id)
				get_data["kick_off"] = kick_off
				get_data["fixture_date"] = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
				get_data["match_details"] = fix.fixture_venue
				get_data["click_action"] = "av"
				get_data["userType"] = usertoken.usertype
				get_data["usertoken"] = usertoken.id
				source = usertoken.source_device
				
				# sound = usertoken.notification_tone
				# if sound:
					# get_data['sound'] = sound
				# else:
					# get_data['sound'] = "Default"
				GlobalDict1 = {}
				if source == "i":
					data = push_notification_ios_web(get_data,token_hex)
					GlobalDict1["player_id"] = player
					GlobalDict1["status"] = bool(data)
				else :
					data = push_notification_android_web(get_data,token_hex)
					GlobalDict1["player_id"] = player
					GlobalDict1["status"] = bool(data['status'])
				responseList.append(GlobalDict1)
				
				GlobalDict['response'] = responseList
				GlobalDict['msg'] =  "msg has been sent"
				
			else:
				GlobalDict['status'] = "2"
				GlobalDict['msg'] =  "Token Not Found"
		else:
			GlobalDict['status'] = "1"
			GlobalDict['msg'] =  "You can not use Resend, because you have already resend the notification..."
	return JsonResponse(GlobalDict, safe = False)
	
#'''Dhananjay start'''
@csrf_exempt
def clubProfile(request):
	''' Loads club's profile '''
	defaultData = defaultView(request)
	if 'users_id' not in request.session:
		msg = "Please Login OR Register First"
		menu_head = "Login"
		return render(request, 'index.html', {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head, 'msg':msg, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
	else:
		uid = request.session['users_id']
		user = Users.objects.filter(id = uid)
		if user.exists():
			user = user[0]
			if user.email_verified:
				if 'club_id' not in request.session:
					return HttpResponseRedirect('/clubRegistration/')
				else:
					title = "Club Profile"
					tab = "Profile"
					menu_head = "Settings"
					uid = request.session['users_id']
					users = Users.objects.filter(id = uid).exists()
					if users:
						users = Users.objects.get(id = uid)
						data = Clubs.objects.get(users_id = uid)
						clubteams = ClubTeams.objects.filter(club=data, is_active=1)
						num_teams = clubteams.count()
						allteams = "["
						i = 1
						if num_teams > 0:
							for team in clubteams:
								if i == num_teams:
									allteams += '"'+team.team_name+'"'
								else:
									allteams += '"'+team.team_name+'" ,'
									i += 1
						allteams += "]"
						data.num_teams = num_teams
						data.age_groups = allteams
						data.save()
						ground = Grounds.objects.filter(id = data.ground_id)
						ground = ground[0]
						homekit = Kits.objects.filter(id = data.home_kit_id)
						homekit = homekit[0]
						awaykit = Kits.objects.filter(id = data.away_kit_id)
						awaykit = awaykit[0]
						
						age_group = json.loads(data.age_groups)
						age_group.sort()
						html = ""
						for i in range(data.num_teams):
							html += "<div>"+str(age_group[i])+"</div>"
						
						agegroups = html
						
						return render(request,'club_profile.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'response' : data,'ground' : ground,'homekit' : homekit, 'awaykit' : awaykit,'agegroups':agegroups,'mobile_no':users.mobile_no,'title':title,'tab':tab, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
					else:
						return HttpResponseRedirect('/logout/')
			else:
				return HttpResponseRedirect('/emailV/')
		else:
			return HttpResponseRedirect('/logout/')

@csrf_exempt
def searchAdvance(request):
	''' Loads advance search page from quick search bar '''
	if 'users_id' not in request.session:
		menu_head = "Login"
		email_verified = True
	else:
		menu_head = "Settings"
		user = Users.objects.get(id = request.session['users_id'])
		email_verified = user.email_verified

	title = 'Advance Search'
	tab=""
	defaultData = defaultView(request)
	search_cities = []
	if request.GET.get('search'):
		city = request.GET.get('search')
		if city.find(',') != -1:
			s_city = city.split(',')
			city = s_city[0]
			city2 = s_city[1].strip()
			exist_city = City.objects.filter(city_name = city).exists()
			if exist_city:
				countyid = County.objects.filter(county_name = city2).values_list('id', flat=True)[0]
				cid = City.objects.filter(city_name = city,county_name_id = countyid).values_list('id',flat=True)[0]
				hola = Clubs.objects.filter(city_name = cid)
				stats = hola.exists()
				if stats == False:
					search_cities.append(["none","","",""])
				else:

					for ci in Clubs.objects.filter(city_name = cid):
						postcode_id = Grounds.objects.filter(id = ci.ground_id).values_list('postcode',flat=True)[0]
						county = County.objects.filter(id = ci.county_name_id).values_list('county_name',flat=True)[0]
						postcode = PostalCodes.objects.filter(id = postcode_id).values_list('postcode', flat=True)[0]
						if ci.logo_photo:
							logo_photo = ci.logo_photo.url
						else:
							logo_photo = defaultLogo
						search_cities.append([ci.club_name,city,postcode,logo_photo,ci.id,county])
			else:
				search_cities.append(["none","","",""])
		else:
				search_cities.append(["none","","",""])


	GlobalDict = {}
	club = Clubs.objects.raw('SELECT id,club_name FROM "LocalTeamsApp_clubs"')
	gender = Gender.objects.raw('SELECT id,gender FROM "LocalTeamsApp_gender"')
	age_group = AgeGroups.objects.raw('SELECT id,age_group FROM "LocalTeamsApp_agegroups" ORDER BY id ASC')
	country = Country.objects.raw('SELECT id,country_name FROM "LocalTeamsApp_country"')
	county = County.objects.raw('SELECT id,county_name FROM "LocalTeamsApp_county" ORDER BY county_name ASC')
	profile_list = Roles.objects.all()
	images_list = BackImages.objects.all()

	imagesL=[]
	i=1
	for image in images_list:
		GlobalDict['id']=i
		GlobalDict['image_url']=image.image_url
		i += 1
		imagesL.append(GlobalDict)
		GlobalDict={}
	
	allclubs = []
	for cc in club:
		allclubs.append(cc.club_name)
	
	allcounty = []
	for coty in county:
		allcounty.append(coty.county_name.title())

	countries = []
	globDict = {}
	for cou in country:
		countries.append(cou.country_name.title())
		
	genders = []
	for gen in gender:
		genders.append([gen.gender.title(),gen.id])	
	age_groups = []	
	for ag in age_group:
		age_groups.append([ag.age_group.title(),ag.id])
	
	q = City.objects.values('city_name').distinct().values_list('city_name',flat=True)
	registerclubs = []
	for regclub in q:
				
		registerclubs.append(regclub)
	if len(search_cities) != 0:
		return render(request, 'advance_search.html',{'profile_list':profile_list,'images_list':defaultData['imagesLindex'], 'images_lists':imagesL, 'allclubs' : json.dumps(allclubs),'gender' : genders, 'age_group' : age_groups,'countries' : json.dumps(countries),'counties' : json.dumps(allcounty),'allpost' : json.dumps(0),'search' : search_cities,'title':title,'menu_head':menu_head, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
	else:
		return render(request, 'advance_search.html',{'profile_list':profile_list,'images_list':defaultData['imagesLindex'], 'images_lists':imagesL, 'allclubs' : json.dumps(allclubs),'gender' : genders, 'age_group' : age_groups,'countries' : json.dumps(countries),'counties' : json.dumps(allcounty),'allpost' : json.dumps(0),'title':title,'menu_head':menu_head,'tab':tab, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
		
@csrf_exempt
def AdvancedSearch(request):
	''' Filter search result according to provided data '''
	allclubs1 = Clubs.objects.all()
	if request.POST['clubname']:
		cname = request.POST['clubname']
		allclubs1 = allclubs1.filter(club_name = cname)
	if request.POST['gender']:
		gender = request.POST['gender']
		allclubs1 = allclubs1.filter(gender_id = gender)
	if request.POST['age']:
		age_id = request.POST['age']
		age = AgeGroups.objects.filter(id = age_id).values_list('age_group', flat=True)[0]
		allclubs1 = allclubs1.filter(age_groups__contains = age)
	if request.POST['city']:
		city = request.POST['city']
		s_city = city.split(',')
		city = s_city[0]
		city2 = s_city[1].strip()
		countyid = County.objects.filter(county_name = city2).values_list('id', flat=True)[0]
		citid = City.objects.filter(city_name = city,county_name_id = countyid).values_list('id', flat=True)[0]
		allclubs1 = allclubs1.filter(city_name = citid)
	if request.POST['county']:
		county = request.POST['county']
		countyid = County.objects.filter(county_name = county).values_list('id', flat=True)[0]
		allclubs1 = allclubs1.filter(county_name = countyid)	
	if request.POST['country']:
		country = request.POST['country']
		contid = Country.objects.filter(country_name = country).values_list('id', flat=True)[0]
		allclubs1 = allclubs1.filter(country_name = contid)		

	allclubs = []
	for cc in allclubs1:
		cit = cc.city_name
		postcode_id = Grounds.objects.filter(id = cc.ground_id).values_list('postcode',flat=True)[0]
		county = County.objects.filter(id = cc.county_name_id).values_list('county_name',flat=True)[0]
		postcode = PostalCodes.objects.filter(id = postcode_id).values_list('postcode', flat=True)[0]
		if cc.logo_photo:
			logo_photo = cc.logo_photo.url
		else:
			logo_photo = defaultLogo
		allclubs.append([cc.club_name,cit.city_name,postcode,logo_photo,cc.id,county])
	return HttpResponse(json.dumps(allclubs))
	
@csrf_exempt
def popcountry(request):
	""" AJAX """
	''' Loads list of cities for selected country '''
	selcou = request.POST['selectedcountry']
	present = Country.objects.filter(country_name = selcou)
	allcities = []
	pres_length = len(present)
	if pres_length != 0:
		for to in present:
			id = to.id
		for cities in City.objects.filter(country_name = id):	
			allcities.append(cities.city_name)
		
	return HttpResponse(json.dumps(allcities))

@csrf_exempt
def showmap(request):
	""" AJAX """
	''' Loads Map on club's profile page '''
	city = request.POST['city']
	citid = City.objects.filter(city_name = city)
	cityid = citid[0].id
	present = Clubs.objects.all()
	allmarkers = []
	for mapval in present.filter(city_name = cityid):
		postcode_id = Grounds.objects.filter(id = mapval.ground_id).values_list('postcode',flat=True)[0]
		postcode = PostalCodes.objects.filter(id = postcode_id).values_list('postcode', flat=True)[0]
		county = County.objects.filter(id = mapval.county_name_id).values_list('county_name',flat=True)[0]
		if mapval.logo_photo:
			logo_photo = mapval.logo_photo.url
		else:
			logo_photo = defaultLogo
	 	allmarkers.append([mapval.club_name,city,postcode,logo_photo,mapval.id,county])

	return HttpResponse(json.dumps(allmarkers))	

@csrf_exempt
def index_search(request):
	""" AJAX """
	''' Quick search bar '''
	city = request.GET.get('search')
	citid = City.objects.filter(city_name = city)
	cityid = citid[0].id
	present = Clubs.objects.all()
	allmarkers = []
	for mapval in present.filter(city_name = cityid):
	 	allmarkers.append([mapval.club_name,city])
	return render_to_response('advance_search.html',{'data' : json.dumps(allmarkers)})
	
@csrf_exempt
def distance_search(request):
	''' Advance search '''
	allclubs1 = Clubs.objects.all()
	distance = request.POST['distance']
	postalc = request.POST['postal_code']
	latitude_q = PostalCodes.objects.filter(postcode = postalc).values_list('latitude',flat=True)[0]
	longitude_q = PostalCodes.objects.filter(postcode = postalc).values_list('longitude',flat=True)[0]
	latitude = float(str(unicode(latitude_q.lstrip())))
	longitude = float(str(unicode(longitude_q.lstrip())))
	distance_units = 3959
	query = 'SELECT id,latitude,longitude,city_name_id, ( 3959 * acos( cos( radians(%f) ) * cos( radians( latitude ) ) * cos( radians(longitude) - radians(%f) ) + sin( radians(%f) ) * sin( radians( latitude ) ) ) ) AS Distance ,club_name FROM "LocalTeamsApp_clubs" GROUP BY id,club_name,latitude,longitude HAVING ( 3959 * acos( cos( radians(%f) ) * cos( radians( latitude ) ) * cos( radians(longitude) - radians(%f) ) + sin( radians(%f) ) * sin( radians( latitude ) ) ) ) < %d ORDER BY Distance;' % (latitude,longitude,latitude,latitude,longitude,latitude,int(distance))
	result = Clubs.objects.raw(query)
	# defaultLogo = '/static/images/icon/LT_logo_default_1100x1100.png'
	data = []
	rex = []
	distances = {}
	distances_array = []
	GlobalDict = {}
	for i in result:
		dam = Clubs.objects.filter(id = i.id)
		for doma in dam:
			GlobalDict['cid'] = doma.id
			GlobalDict['cname'] = doma.club_name
			GlobalDict['cityname'] = City.objects.filter(id = doma.city_name_id).values_list('city_name',flat=True)[0]
			GlobalDict['agegroups'] = doma.age_groups
			GlobalDict['distance'] = format(i.distance,'.2f')
			GlobalDict['postalc'] = postalc
			if doma.logo_photo:
				GlobalDict['logophoto'] = doma.logo_photo.url
			else:
				GlobalDict['logophoto'] = defaultLogo
			GlobalDict['gender'] = Gender.objects.filter(id = doma.gender_id).values_list('gender',flat=True)[0]
			GlobalDict['county_name'] = County.objects.filter(id = doma.county_name_id).values_list('county_name',flat=True)[0]
			GlobalDict['country_name'] = Country.objects.filter(id = doma.country_name_id).values_list('country_name',flat=True)[0]
			rex.append(GlobalDict)
			GlobalDict ={}
	if request.POST['gender']:
		gender = request.POST['gender']
		gender = Gender.objects.filter(id = gender).values_list('gender',flat=True)[0]
		valuelist = [gender]
		rex = copyf(rex, 'gender', valuelist)
	if request.POST['county']:
		county = request.POST['county']
		valuelist = [county]
		rex = copyf(rex, 'county_name', valuelist)
	if request.POST['city']:
		city = request.POST['city']
		s_city = city.split(',')
		city = s_city[0]
		city2 = s_city[1].strip()
		valuelist = [city]
		rex = copyf(rex, 'cityname', valuelist)	
		valuelist1 = [city2]
		rex = copyf(rex, 'county_name', valuelist1)	
	if request.POST['country']:
		country = request.POST['country']
		valuelist = [country]
		rex = copyf(rex,'country_name', valuelist)
	if request.POST['clubclubname']:
		cname = request.POST['clubclubname'].upper()
		valuelist = [cname]
		rex = copyf(rex,'cname', valuelist)
	if request.POST['age']:
		age = request.POST['age']
		age_name = AgeGroups.objects.filter(id = age).values_list('age_group',flat=True)[0]
		dre = []
		for gym in rex:

			if age_name in gym['agegroups']:
				dre.append(gym)
		return HttpResponse(json.dumps(dre))

	return HttpResponse(json.dumps(rex))
#''' Dhananjay end '''

def logout(request):
	''' Logout '''
	if 'users_id' in request.session:
		del request.session['users_id']
	if 'club_id' in request.session:
		del request.session['club_id']

	return HttpResponseRedirect('/')

def viewProfile(request, club_id):
	''' Loads profile of given club_id '''
	if 'users_id' not in request.session:
		menu_head = "Login"
	else:
		menu_head = "Settings"
		
	title = 'Club Profile'
	data = Clubs.objects.filter(id = club_id).exists()
	if data:
		defaultData = defaultView(request)
		data = Clubs.objects.get(id = club_id)
		ground = Grounds.objects.get(id = data.ground_id)
		homekit = Kits.objects.get(id = data.home_kit_id)
		awaykit = Kits.objects.get(id = data.away_kit_id)
		
		age_group = json.loads(data.age_groups)
		age_group.sort()
		html = ""
		for i in range(data.num_teams):
			html += "<div>"+str(age_group[i])+"</div>"
		
		agegroups = html		
		return render(request,'view_profile.html',{'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'response' : data,'ground' : ground,'homekit' : homekit, 'awaykit' : awaykit,'agegroups':agegroups,'menu_head':menu_head,'title':title, 'iOS_app':iOS_app, 'google_play_app':google_play_app})
	else:
		return HttpResponseRedirect('/')
		
# copyf(rex,'cname', valuelist)
def copyf(data, key, allowed):
    return filter(lambda x: key in x and x[key] in allowed, data)

@csrf_exempt
def getplayers(request):
	""" AJAX """
	''' Loads players of provided team '''
	team = request.POST['teamid']
	if team == '':
		return HttpResponse('Blank')
	else:
		status = ClubTeams.objects.filter(id = team, is_active = 1).exists()
		if status:
			status = ClubTeams.objects.get(id = team)
			managername = ClubManager.objects.filter(id = status.club_manager_id).exists()
			if managername:
				managername = ClubManager.objects.filter(id = status.club_manager_id)
				team_players = []
				players = Players.objects.filter(club_team_id = team).filter(is_active = 1).order_by('player_position_id','player_name')
				i = 0
				for pl in players:
					if i == 4:
						i = 0
					pos = FootballPlayerPositions.objects.filter(id = pl.player_position_id)[0]
					if pl.player_img:
						player_img = pl.player_img.url
					else:
						player_img = defaultPlayer
					team_players.append([pl.player_name,pos.position,pl.id,player_img,i,pl.player_mob_no,pos.id])
					i = i+1
				html = render_to_string('loadplayers.html', {'players': team_players,'manager' : managername,'defaultPlayer':defaultPlayer})
			else:
				html = 0
			return HttpResponse(html)
	
@csrf_exempt
def addmanager(request):
	''' Store manager details '''
	teamid = request.POST['teamid']
	teams = ClubTeams.objects.get(id = teamid)
	name = request.POST['manager_name']
	mobile = request.POST['manager_mobile']
	created_by = request.session['club_id']
	manager_obj = ClubManager(manager_name = name,manager_mob_no = mobile, created_at = now(), created_by = created_by)
	manager_obj.save()
	
	if request.POST['m_logo'] != '':
		ids = str(manager_obj.created_by)
		logo = request.POST['m_logo'].split('base64,', 1 )[1]
		logo_link = image_upload('manager_profile_image',"manager",logo,ids)
		manager_obj.manager_img = logo_link
		manager_obj.save()
	
	mid = manager_obj.id
	# usersData = UsersToken.objects.filter(manager_id = mid)
	# for ud in usersData:
		# ud.delete()
	team = ClubTeams.objects.filter(id = teamid, is_active = 1)[0]
	team.club_manager_id = mid
	team.save()
	msg = 'Manager successfully added for '+str(teams.team_name)+'!'
	request.session['msg'] = msg
	return HttpResponseRedirect('/clubSquad/')
		
@csrf_exempt
def addplayer(request):
	""" AJAX """
	''' Stores new player for provided team '''
	name = request.POST['pname']
	position = request.POST['position']
	mobile = request.POST['mobile']
	teamid = request.POST['teamid']
	pic = request.POST['pic']
	player = Players.objects.filter(player_mob_no = mobile, is_active = 0)
	if player.exists():
		players_save = player[0]
		players_save.player_name = name
		players_save.player_position_id = position
		players_save.player_mob_no = mobile
		players_save.is_active = 1
		players_save.club_team_id = teamid
		players_save.save()
		if pic == '':
			players_save.player_img = None
			players_save.save()
		
	else:
		players_save = Players(player_name = name,player_mob_no = mobile,player_position_id = position,club_team_id = teamid)
		players_save.save()
	
	if pic != '':
		pic = request.POST['pic'].split('base64,', 1 )[1]
		pic_link = image_upload('player_profile_image',"player",pic,mobile)
		players_save.player_img = pic_link
		players_save.save()
		
	status = ClubTeams.objects.filter(id = teamid).values_list('club_manager_id',flat=True)[0]
	managername = ClubManager.objects.filter(id = status)
	
	team_players = []
	players = Players.objects.filter(club_team_id = teamid).filter(is_active = 1).order_by('id')
	i = 0
	for pl in players:
		if i == 4:
			i = 0
		pos = FootballPlayerPositions.objects.filter(id = pl.player_position_id)[0]
		if pl.player_img:
			player_img = pl.player_img.url
		else:
			player_img = defaultPlayer
		team_players.append([pl.player_name,pos.position,pl.id,player_img,i,pl.player_mob_no,pos.id])
		i = i+1
	html = render_to_string('loadplayers.html', {'players': team_players,'manager' : managername})
	return HttpResponse(html)

@csrf_exempt
def deleteplayer(request):
	""" AJAX """
	''' Delete player from team '''
	data = request.POST.getlist('players[]')
	for d in data:
	 	play = Players.objects.filter(id = d)[0]
	 	play.is_active = 0
	 	play.save()
		notifications = Notifications.objects.filter(player = play)
		for noti in notifications:
			noti.is_visible = bool(0)
			noti.updated_at = now()
			noti.save()
		user_token = UsersToken.objects.filter(player_id = d)
		if user_token.exists():
			for token in user_token:
				token.delete()
	return HttpResponse("done")

@csrf_exempt
def deletemanager(request):
	""" AJAX """
	''' Delete manager for a team '''
	manager_id = request.POST['manager']
	teamid = request.POST['teamid']
	team = ClubTeams.objects.get(id = teamid)
	team.club_manager_id = None
	team.save()
	teams = ClubTeams.objects.filter(club_manager_id = manager_id)
	if not teams.exists():
		manager = ClubManager.objects.get(id = manager_id)
		manager.is_active = 0
		manager.save()
		user_token = UsersToken.objects.filter(manager_id = manager_id)
		if user_token.exists():
			for token in user_token:
				token.delete()
	data = {"response":True}
	return JsonResponse(data)

@csrf_exempt
def updateplayer(request):
	""" AJAX """
	''' Update player's details '''
	name = request.POST['name']
	position = request.POST['position']
	id_player = request.POST['id']
	teamid = request.POST['teamid']
	mobile = request.POST['mobile']
	
	play = Players.objects.get(id = id_player)
	play.player_name = name
	play.player_position_id = position
	play.player_mob_no = mobile
	play.save()
	if request.POST['pic'] != '':
		if play.player_img:
			img_name = PATH+str(play.player_img.url)
			call(["rm", img_name])
		pic = request.POST['pic'].split('base64,', 1 )[1]
		pic_url = image_upload('player_profile_image',"player",pic,id_player)
		play.player_img = pic_url
		play.save()
		
	team = ClubTeams.objects.get(id = teamid)
	manager = ClubManager.objects.get(id = team.club_manager_id)	
	team_players = []	
	players = Players.objects.filter(club_team_id = teamid).filter(is_active = 1).order_by('id')
	i = 0
	for pl in players:
		if i == 4:
			i = 0
		pos = FootballPlayerPositions.objects.filter(id = pl.player_position_id)[0]
		if pl.player_img:
			player_img = pl.player_img.url
		else:
			player_img = defaultPlayer
		team_players.append([pl.player_name,pos.position,pl.id,player_img,i,pl.player_mob_no,pos.id])
		i = i+1
	# html = render_to_string('loadplayers.html', {'players': team_players,'manager' : managername})
	html = render_to_string('loadplayers.html', {'players': team_players,'manager' : manager.manager_name})
	return HttpResponse(html)

@csrf_exempt
def updatemanager(request):
	""" AJAX """
	''' Updates manager's details '''
	id = request.POST['id']
	name = request.POST['name']
	mobile = request.POST['mobile']
	teamid = request.POST['teamid']
	
	manager_obj = ClubManager.objects.get(id = id)
	manager_obj.manager_name = name
	manager_obj.manager_mob_no = mobile
	manager_obj.save()
	
	if request.POST['pic'] != '':
		if manager_obj.created_by:
			ids = str(manager_obj.created_by)
		else:
			ids = str(0)
		if manager_obj.manager_img:
			img_name = PATH+str(manager_obj.manager_img.url)
			call(["rm", img_name])
		logo = request.POST['pic'].split('base64,', 1 )[1]
		logo_link = image_upload('manager_profile_image',"manager",logo,ids)
		manager_obj.manager_img = logo_link
		manager_obj.updated_by = request.session['club_id']
		manager_obj.updated_at = now()
		manager_obj.save()
	
	teams = ClubTeams.objects.get(id = teamid)
	status = ClubTeams.objects.filter(id = teams.id, is_active = 1).values_list('club_manager_id',flat=True)[0]
	managername = ClubManager.objects.filter(id = status)	
	team_players = []	
	players = Players.objects.filter(club_team_id = teams.id).filter(is_active = 1).order_by('id')
	i = 0
	for pl in players:
		if i == 4:
			i = 0
		pos = FootballPlayerPositions.objects.filter(id = pl.player_position_id)[0]
		if pl.player_img:
			player_img = pl.player_img.url
		else:
			player_img = defaultPlayer
		team_players.append([pl.player_name,pos.position,pl.id,player_img,i,pl.player_mob_no,pos.id])
		i = i+1
	html = render_to_string('loadplayers.html', {'players': team_players,'manager' : managername})
	return HttpResponse(html)	

@csrf_exempt
def getsquad(request):
	""" AJAX """
	''' Load players for while creating fixture '''
	data = request.POST.getlist('items[]')
	result = []
	players = Players.objects.filter(club_team_id__in = data).filter(is_active = 1).order_by('player_position_id','player_name')
	i = 0
	plcount = players.count()
	team_players = []
	for pl in players:
		if i == 4:
			i = 0
		pos = FootballPlayerPositions.objects.filter(id = pl.player_position_id)[0]
		if pl.player_img:
			player_img = pl.player_img.url
		else:
			player_img = defaultPlayer
		team_players.append([pl.player_name,pos.position,pl.id,player_img,i])
		i = i+1
	html = render_to_string('squadplayers.html', {'players': team_players,"count":plcount})
	
	return HttpResponse(html)
	
#''' Dhananjay end '''

@csrf_exempt
def get_user_data(request):
	""" Web API """
	''' Just for checking if user is present or not for provided credentials '''
	if request.method == "POST":
		data = json.loads(request.body)
		mobile_no = data['mobile_no']
		GlobalDict = {}
		usersData = Users.objects.filter(mobile_no = mobile_no).exists()
		if usersData:
			usersData = Users.objects.filter(mobile_no = mobile_no).count()
			if usersData == 1:
				usersData = Users.objects.get(mobile_no = mobile_no)
				GlobalDict["status"] = "Exists"
				GlobalDict["username"] = str(usersData.username)
				GlobalDict["password"] = str(usersData.password)
			else:
				GlobalDict["status"] = "Exists"
				GlobalDict["msg"] = "Contact with DB Admin"
		else:
			GlobalDict["status"] = "DNE"
			GlobalDict["msg"] = "Does not exists"
		return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def finalSquad(request):
	""" AJAX """
	''' Loads squad finalized for the fixture '''
	fixture_id = request.POST['fixture_id']
	players = request.POST.getlist('players[]')
	GlobalDict = {}
	fix_data = Fixtures.objects.get(id = fixture_id, fixture_visibility = "t")
	fix_data.fixture_confirm = True
	fix_data.save()
	GlobalDict['status']=fix_data.fixture_confirm
	for pl in players:
		sqd = Squad.objects.get(player_id = pl, fixture_id = fix_data)
		sqd.squad_confirm = True
		sqd.save()
			
	squad_data = Squad.objects.filter(fixture_id = fixture_id, status_id = 1)
	for data in squad_data:
		usertokenE = UsersToken.objects.filter(player = data.player_id).exists()
		if usertokenE:
			usertoken = UsersToken.objects.get(player = data.player_id)
			source = usertoken.source_device
			token_hex = usertoken.device_token
			club = Clubs.objects.get(id = fix_data.club_id)
			rgb_color = str(club.strip_colour)
			if rgb_color:
				hex_color = rgb2hex(rgb_color)
			else:
				hex_color = str("#939496")
			if token_hex:
				fixture_date = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix_data.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
				fixture_time = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix_data.fixture_time), "%H:%M:%S"), "%I:%M %p") )
				kick_off = fixture_date +" @ "+ fixture_time
				get_data = {}
				get_data["id"] = str(fix_data.id)
				get_data["userType"] = usertoken.usertype
				get_data["fixture_name"] = fix_data.fixture
				get_data["body"] = "Confirmed players for "+fix_data.fixture
				get_data["kick_off"] = kick_off
				get_data["fixture_date"] = fixture_date
				# get_data["fixture_time"] = fixture_time
				get_data["match_details"] = fix_data.fixture_venue
				get_data["click_action"] = "av"
				get_data["tag"] = "availablity"
				get_data["tab"] = "av"
				get_data["strip_color"] = hex_color
				get_data["club_name"] = club.club_name
				get_data["usertoken"] = usertoken.id
				if str(data.player_id) in players:
					get_data["is_confirm"] = "1"
				else:
					get_data["is_confirm"] = "0"
				if source == "i":
					data1 = push_notification_ios_web(get_data,token_hex)
				else :
					data1 = push_notification_android_web(get_data,token_hex)
				print "data1 - ",data1
	return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def matchDaySqd(request):
	""" AJAX """
	''' Loads players who are added in the fixture '''
	fixture_id = request.POST['fixture_id']
	squad_data = Squad.objects.filter(fixture_id = fixture_id, squad_confirm = 't')
	GlobalDict = {}
	PlayersList = []
	PlayerDict = {}
	for player in squad_data:
		player_info = Players.objects.get(id = player.player_id)
		PlayerDict["player_id"] = str(player_info.id)
		PlayerDict["player_name"] = player_info.player_name
		if player_info.player_img:
			player_img = player_info.player_img.url
		else:
			player_img = defaultPlayer
		PlayerDict["player_img"] = str(player_img)
		PlayerDict["player_position_id"] = str(player_info.player_position_id)
		PlayerDict["player_position"] = str(player_info.player_position).capitalize()
		PlayersList.append(PlayerDict)
		PlayerDict = {}
	GlobalDict["player_list"] = PlayersList
	GlobalDict["count"] = squad_data.count()
	return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def notifications_list(request):
	""" AJAX """
	''' Loads all notifications for notification icon '''
	if request.method == "POST":
		if 'users_id' in request.session:
			users_id = request.session["users_id"]
			notification = Notifications.objects.filter(users_id = users_id, is_visible = True).order_by('-updated_at')
			badge_count = notification.filter(read = bool(0)).count()
			GlobalDict = {}
			GlobalList = []
			for notif in notification:
				player = Players.objects.get(id = notif.player_id)
				GlobalDict["click_action"] = "https://local-teams.com/clubNotif/"+str(notif.fixture_id)
				GlobalDict["player_icon"] = str(player.player_img.url) if player.player_img else defaultPlayer
				GlobalDict["player_name"] = player.player_name
				GlobalDict["player_status"] = notif.player_status.lower()
				GlobalDict["fixture_name"] = notif.fixture.fixture
				GlobalDict["notif_id"] = notif.id
				GlobalDict["updated_at"] = notif.updated_at
				GlobalDict["msg_status"] = "read" if notif.read else "unread"
				GlobalList.append(GlobalDict)
				GlobalDict = {}
			GlobalDict["notifications"] = GlobalList
			GlobalDict["badge_count"] = badge_count
			return JsonResponse(GlobalDict, safe=False)
		else:
			return HttpResponse("No data")
			
@csrf_exempt
def notifications_read(request):
	""" AJAX """
	''' Change background of visited notiffications '''
	if request.method == "POST":
		id = request.POST['notif_id']
		notification = Notifications.objects.get(id = id)
		notification.read = bool(1)
		notification.updated_at = now()
		notification.save()
		fix_noti = Notifications.objects.filter(fixture = notification.fixture)
		for noti in fix_noti:
			noti.read = bool(1)
			noti.updated_at = now()
			noti.save()
		
		GlobalDict = {'read':bool(1)}
		return JsonResponse(GlobalDict, safe=False)

@csrf_exempt
def emailV(request):
	''' Loads Email verification page '''
	if 'users_id' in request.session:
		users_id = request.session["users_id"]
		# ip = get_ip(request)
		count = 0
		defaultData = defaultView(request)
		menu_head = 'Settings'
		data = {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'tab':'','title':'Email Verification','count':count,'users_id':users_id,'activation_code':'right', 'iOS_app':iOS_app, 'google_play_app':google_play_app}
		return render(request,'Emailtest.html',data)
	else:
		return HttpResponseRedirect('/logout/')

@csrf_exempt
def verifyEmail(request):
	''' Verifies if provided activation code is valid or not '''
	if 'users_id' in request.session:
		if request.method == "POST":
			users_id = request.session["users_id"]
			code = request.POST['v_code']
			# uid = request.session['users_id']
			userE = Users.objects.filter(id=users_id).exists()
			if userE:
				user = Users.objects.get(id=users_id)
				if str(user.activation_code) == str(code):
					user.email_verified = True
					user.activation_code = None
					user.save()
					return HttpResponseRedirect('/clubProfile/')
				else:
					defaultData = defaultView(request)
					menu_head = 'Settings'
					tab = ""
					data = {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'tab':tab,'title':'Email Verification','count':0, 'activation_code':'wrong'}
					return render(request,'Emailtest.html',data)
		else:
			return HttpResponseRedirect('/emailV/')
	else:
		return HttpResponseRedirect('/logout/')

@csrf_exempt
def resendEmail(request):
	''' Resends Email activation code again '''
	if 'users_id' in request.session:
		users_id = request.session["users_id"]
		userE = Users.objects.filter(id=users_id)
		if userE.exists():
			user = userE[0]
			activation_code = user.activation_code
			email = user.email
			try:
				response = sendMail(activation_code, email)
			except Exception, e:
				response = str(e)
		
		result = {'status':1,'response':str(response)}
	else:
		result = {'status':2}
	return JsonResponse(result)
	
@csrf_exempt
def defaultsite(request):
	return HttpResponseRedirect('/')
	
@csrf_exempt
def send_emails(request):
	''' Sends email for activation code '''
	# subject = "Comfirmation of email-id"
	# msg_body = "Your email varification code is 1324"
	# response = send_mail(subject,msg_body,'support@local-teams.com',['psjay4595@gmail.com'],fail_silently=False,)
	email = "psjay4595@gmail.com"
	# activation_code = "123456"
	activation_code = str(e_otp())
	response = sendMail(activation_code, email)
	return HttpResponse(str(response))

@csrf_exempt
def Test(request):
	""" Just Test Page """
	if 'users_id' in request.session:
		menu_head = "Settings"
	else:
		menu_head = "Login"
	# ip = get_ip(request)
	count = 2
	# print 'ip ',ip
	defaultData = defaultView(request)
	# menu_head = 'Settings'
	data = {'profile_list':defaultData['profile_list'],'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'tab':'','title':'Test','count':count, 'request':request}
	return render(request, 'Test.html', data)
	
@csrf_exempt
def template(request):
	''' Template for loading policies, terms, conditions, etc. on webpage '''
	defaultData = defaultView(request)
	data = {'images_list':defaultData['imagesLindex'],'title':'Test','html_page':''}
	return render(request,'template.html', data)

@csrf_exempt
def about_us(request):
	''' To load about us webpage '''
	if 'users_id' in request.session:
		menu_head = "Settings"
	else:
		menu_head = "Login"
	defaultData = defaultView(request)
	html_page = "about_us.html"
	title = "About Us"
	data = {'menu_head':menu_head,'images_list':defaultData['imagesLindex'],'title':title,'html_page':html_page, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'template2.html', data)
	
@csrf_exempt
def contact_us(request):
	''' To load contact us webpage '''
	if 'users_id' in request.session:
		menu_head = "Settings"
		user = Users.objects.get(id = request.session['users_id'])
		email = str(user.email)
	else:
		menu_head = "Login"
		email = ""
	defaultData = defaultView(request)
	html_page = "contact_us.html"
	title = "Contact Us"
	data = {'menu_head':menu_head, 'images_list':defaultData['imagesLindex'], 'title':title, 'email':email, 'html_page':html_page, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'template2.html', data)
	
@csrf_exempt
def privacy_policy(request):
	''' To load privacy policy webpage '''
	if 'users_id' in request.session:
		menu_head = "Settings"
	else:
		menu_head = "Login"
	defaultData = defaultView(request)
	html_page = "privacy_policy.html"
	title = "Privacy Policy"
	data = {'menu_head':menu_head,'images_list':defaultData['imagesLindex'],'title':title,'html_page':html_page, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'template2.html', data)
	
@csrf_exempt
def cookie_policy(request):
	''' To load cookie policy webpage '''
	if 'users_id' in request.session:
		menu_head = "Settings"
	else:
		menu_head = "Login"
	defaultData = defaultView(request)
	html_page = "cookie_policy.html"
	title = "Cookie Policy"
	data = {'menu_head':menu_head,'images_list':defaultData['imagesLindex'],'title':title,'html_page':html_page, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'template2.html', data)

@csrf_exempt
def terms_and_conditions(request):
	''' To load terms and conditions webpage '''
	if 'users_id' in request.session:
		menu_head = "Settings"
	else:
		menu_head = "Login"
	defaultData = defaultView(request)
	html_page = "terms_and_conditions.html"
	title = "Terms & Condition"
	data = {'menu_head':menu_head,'images_list':defaultData['imagesLindex'],'title':title,'html_page':html_page, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
	return render(request,'template2.html', data)

@csrf_exempt
def Privacy_policy(request):
	''' To load privacy policy webpage '''
	return render(request,'privacy_policy.html')
@csrf_exempt
def Cookie_policy(request):
	''' To load cookie policy webpage '''
	return render(request,'cookie_policy.html')
@csrf_exempt
def Terms_and_conditions(request):
	''' To load terms and conditions webpage '''
	data = {'open':'target="_blank"'}
	return render(request,'terms_and_conditions.html',data)
	
@csrf_exempt
def reset_pass(request,q):
	''' Loads page for resetting password '''
	user = Users.objects.filter(forgotten_password_code = q)
	if user.exists():
		email = user[0].email
		defaultData = defaultView(request)
		menu_head = 'Login'
		data = {'images_list':defaultData['imagesLindex'],'menu_head':menu_head,'tab':'','title':'Reset Password','count':0, 'email':email, 'iOS_app':iOS_app, 'google_play_app':google_play_app}
		return render(request,'reset_password.html', data)
	else:
		return HttpResponseRedirect('/')

@csrf_exempt
def setPass(request):
	''' Resets the password '''
	if 'pass' in request.POST:
		email = request.POST['email']
		password = request.POST['pass']
		user = Users.objects.filter(email = email)
		if user.exists():
			users = user[0]
			users.password = password
			users.forgotten_password_code = None
			users.save()
			return HttpResponseRedirect("/")
		return HttpResponseRedirect("/")