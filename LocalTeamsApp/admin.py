from django.contrib import admin
from models import *
# Register your models here.

class RolesAdmin(admin.ModelAdmin):
	list_display = ('id','role')
	list_filter = ['role']
	search_fields = ['role']

class BackImagesAdmin(admin.ModelAdmin):
	list_display = ('id','image_name','image_url','rel_url','hide','image_tag')
	list_filter = ['image_name','image_url','rel_url']
	search_fields = ['image_name','image_url','rel_url']

class AgeGroupsAdmin(admin.ModelAdmin):
	list_display = ('id','age_group','description')
	list_filter = ['age_group']
	search_fields = ['age_group']

class StatusAdmin(admin.ModelAdmin):
	list_display = ('id','status')
	list_filter = ['status']
	search_fields = ['status']

class CountryAdmin(admin.ModelAdmin):
	list_display = ('id','iso','country_name')
	list_filter = ['country_name']
	search_fields = ['country_name']

class CountyAdmin(admin.ModelAdmin):
	list_display = ('id','county_name','country_name')
	list_filter = ['county_name','country_name']
	search_fields = ['county_name','country_name']

class CityAdmin(admin.ModelAdmin):
	list_display = ('id','city_name','county_name','country_name')
	list_filter = ['city_name','county_name','country_name']
	search_fields = ['city_name','county_name','country_name']

class FootballPlayerPositionsAdmin(admin.ModelAdmin):
	list_display = ('id','position','pos_abbv')
	list_filter = ['position','pos_abbv']
	search_fields = ['position','pos_abbv']

class GenderAdmin(admin.ModelAdmin):
	list_display = ('id','gender','gen_abbv')
	list_filter = ['gender','gen_abbv']
	search_fields = ['gender','gen_abbv']

class PostalCodesAdmin(admin.ModelAdmin):
	list_display = ('id','postcode','postal_town','county','country','latitude','longitude','local_government_area','region')
	list_filter = ['postcode','postal_town','county','country','latitude','longitude','local_government_area','region']
	search_fields = ['postcode','postal_town','county','country','latitude','longitude','local_government_area','region']

#------------------------------------------------------------------------------------------------#

class KitsAdmin(admin.ModelAdmin):
	list_display = ('id','shirt_colour','sleeve_colour','pattern_colour','pattern_mode')
	list_filter = ['shirt_colour','sleeve_colour','pattern_colour','pattern_mode']
	search_fields = ['shirt_colour','sleeve_colour','pattern_colour','pattern_mode']

class GroundsAdmin(admin.ModelAdmin):
	list_display = ('id','ground_name','street','town','county','postcode','phone','map_url')
	list_filter = ['ground_name','street','town','county','postcode']
	search_fields = ['ground_name','street','town','county','postcode']

class UsersAdmin(admin.ModelAdmin):
	list_display = ('id','ip_address','username','mobile_no','country','usertype','source_device','email_verified','update_notifications')
	list_filter = ['username','mobile_no','country','usertype','source_device']
	search_fields = ['username','mobile_no','country','usertype','source_device']

class ClubsAdmin(admin.ModelAdmin):
	list_display = ('id','club_name','strip_colour','logo_photo','established','gender','num_teams','league_table_url','fixture_table_url','website_url','cover_photo_url','ground','training_text','home_kit','away_kit','team_photo','age_groups','city_name','county_name','country_name','set_background','latitude','longitude','users')
	list_filter = ['club_name','established','gender','num_teams','city_name','county_name','country_name','users']
	search_fields = ['club_name','established','gender','num_teams','city_name','county_name','country_name','users']

class FixturesAdmin(admin.ModelAdmin):
	list_display = ('id','fixture','fixture_date','fixture_time','fixture_venue','fixture_confirm','fixture_visibility','resend','club')
	list_filter = ['fixture','fixture_date','fixture_time','fixture_venue','fixture_confirm','fixture_visibility','resend','club']
	search_fields = ['fixture','fixture_date','fixture_time','fixture_venue','fixture_confirm','fixture_visibility','resend','club']

class ClubManagerAdmin(admin.ModelAdmin):
	list_display = ('id','manager_name','manager_mob_no','manager_img','is_active')
	list_filter = ['manager_name','manager_mob_no','manager_img','is_active']
	search_fields = ['manager_name','manager_mob_no','manager_img']

class ClubTeamsAdmin(admin.ModelAdmin):
	list_display = ('id','team_name','club','agegroup','gender','club_manager','is_active')
	list_filter = ['team_name','club','agegroup','gender','club_manager','is_active']
	search_fields = ['team_name','club','agegroup','gender','club_manager']

class PlayersAdmin(admin.ModelAdmin):
	list_display = ('id','player_name','player_position','player_img','club_team','is_active')
	list_filter = ['player_name','player_position','player_img','club_team','is_active']
	search_fields = ['player_name','player_position','player_img','club_team']

class UsersTokenAdmin(admin.ModelAdmin):
	list_display = ('id','device_token','usertype','users','player','manager','source_device','app_device_token','is_notification_block','notification_tone','badge_count','av_count','rv_count')
	list_filter = ['usertype','users','player','manager','source_device']
	search_fields = ['usertype','users','player','manager','source_device']

class SquadAdmin(admin.ModelAdmin):
	list_display = ('id','club_team','fixture','player','status','resend','notification','squad_confirm','hide_fix')
	list_filter = ['club_team','fixture','player','status','squad_confirm']
	search_fields = ['club_team','fixture','player','status']

class StreetDataAdmin(admin.ModelAdmin):
	list_display = ('id','postcode','organisation_name','postal_county','traditional_county','town','street_name')
	list_filter = ['postcode','organisation_name','postal_county','traditional_county','town','street_name']
	search_fields = ['postcode','organisation_name','postal_county','traditional_county','town','street_name']

class NotificationsAdmin(admin.ModelAdmin):
	list_display = ('id','users','fixture','player','read','player_status','is_visible')
	list_filter = ['users','fixture','player','read','player_status']
	search_fields = ['users','fixture','player','read']

# class Admin(admin.ModelAdmin):
	# list_display = ('id',)
	# list_filter = []
	# search_fields = []

admin.site.register(Roles, RolesAdmin)
admin.site.register(BackImages, BackImagesAdmin)
admin.site.register(AgeGroups, AgeGroupsAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(County, CountyAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(FootballPlayerPositions, FootballPlayerPositionsAdmin)
admin.site.register(Gender, GenderAdmin)
admin.site.register(PostalCodes, PostalCodesAdmin)
admin.site.register(Kits, KitsAdmin)
admin.site.register(Grounds, GroundsAdmin)
admin.site.register(Users, UsersAdmin)
admin.site.register(Clubs, ClubsAdmin)
admin.site.register(Fixtures, FixturesAdmin)
admin.site.register(ClubManager, ClubManagerAdmin)
admin.site.register(ClubTeams, ClubTeamsAdmin)
admin.site.register(Players, PlayersAdmin)
admin.site.register(UsersToken, UsersTokenAdmin)
admin.site.register(Squad, SquadAdmin)
admin.site.register(StreetData, StreetDataAdmin)
admin.site.register(Notifications, NotificationsAdmin)
# admin.site.register(, Admin)