# coding=utf-8
# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.files.images import get_image_dimensions
from django.core.mail import send_mail
from django.db import IntegrityError, connection
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response,redirect
from django.template.context import RequestContext
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.cache import cache_control

from models import *

# from django.utils import simplejson

import os
import json
import time
import urllib
import datetime
import string
import random
import requests

from pyfcm import FCMNotification
from twilio.rest import Client

import json 
from json import loads, dump
# from PIL import Image
from apns import APNs, Frame, Payload

defaultLogo = settings.DEFAULTLOGO
defaultPlayer = settings.DEFAULTPLAYER
ICO = settings.ICO

def now():
	return str(datetime.datetime.now()).split('.')[0]
	
def rgb2hex(rgb_color):
	is_in_rgb = rgb_color.find('rgb')
	if is_in_rgb == 0:
		list = rgb_color.replace('rgb(','').replace(')','').split(',')
		r = int(list[0])
		g = int(list[1])
		b = int(list[2])
		hex = "#{:02x}{:02x}{:02x}".format(r,g,b)
	else:
		is_in_hex = rgb_color.find("#")
		if is_in_hex == 0:
			hex = rgb_color
	return hex
	
@csrf_exempt
def country_web(request):
	country_list = Country.objects.all()
	GlobalDict = {}
	countryL = []
	for cnt in country_list:
		GlobalDict['cnt_id'] = str(cnt.id)
		GlobalDict['cnt_name'] = str(cnt.country_name)
		countryL.append(GlobalDict)
		GlobalDict={}
	return JsonResponse(countryL,safe=False)

def app_device_token():
	return ''.join(random.sample(string.lowercase+string.digits,10))

def otp():
	# return ''.join(random.sample(string.digits,4))
	return "1234"

def send_otp_sms(OTP, mobile):
	# client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH)
	client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH)
	msg_body = "OTP is "+str(OTP)
	from_ = str(settings.TWILIO_FROM)
	data = client.messages.create(to=str(mobile),from_=from_ ,body=msg_body)
	return data

# def send_otp_sms(OTP, mobile):
	# base_url = "http://smsport.biz/API/WebSMS/Http/v1.0a/index.php"
	# username = "hatwar"
	# password = "321@321"
	# sender = "TECQUD"
	# to = mobile
	# message = "OTP is "+str(OTP)
	# URL = base_url+"?username="+username+"&password="+password+"&sender="+sender+"&to="+str(to)+"&message="+message	
	# req = requests.get(URL)
	# return req

####-----LOGIN-----####
@csrf_exempt
def mobileValidation_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		mob_no = str(data['contact'])
		token = str(data['token'])
		source = str(data['source'])
		
		mobile_token = app_device_token()
		OTP = otp()
		GlobalDict = {}
		
		usersEx = Users.objects.filter(mobile_no = mob_no).exists()
		playerEx = Players.objects.filter(player_mob_no = mob_no, is_active = 1).exists()
		manager = ClubManager.objects.filter(manager_mob_no = mob_no, is_active = 1)
		if manager.exists():
			manager = manager[0]
			teams = ClubTeams.objects.filter(club_manager = manager)
			if not teams.exists():
				manager = ClubManager.objects.get(id = manager.id)
				manager.is_active = 0
				manager.save()
				managerEx = False
			else:
				managerEx = True
		else:
			managerEx = False
		
		# user = Users.objects.get(mobile_no = mob_no) if usersEx else None
		# player = Players.objects.get(player_mob_no = mob_no, is_active = 1) if playerEx else None
		# manager = ClubManager.objects.get(manager_mob_no = mob_no, is_active = 1) if managerEx else None
		
		flag_user = flag_player = flag_club_manager = 0
		
		if usersEx:
			flag_user = 1
		if playerEx:
			flag_player = 1
		if managerEx:
			flag_club_manager = 1
			
		if flag_user == 0 and flag_club_manager == 0 and flag_player == 0:
			GlobalDict['user_type'] = ""
			GlobalDict['is_player'] = ""
			GlobalDict['club_admin_id'] = ""
			GlobalDict['manager_id'] = ""
			GlobalDict['player_id'] = ""
			GlobalDict['otp'] = ""
			GlobalDict['app_device_token'] = ""
			GlobalDict['status'] = "0"
			GlobalDict['msg'] ="The mobile number is not registered"
			
		elif flag_user == 1 and flag_club_manager == 0 and flag_player == 0:
			user = Users.objects.get(mobile_no = mob_no)
			users_id = user.id
			uTE = UsersToken.objects.filter(users_id = users_id).exists()
			if uTE:
				usertoken = UsersToken.objects.get(users_id = users_id)
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "1"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = users_id
				usertoken.manager_id = ""
				usertoken.player_id = ""
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token, users_id = users_id, manager_id = "", player_id = "", source_device = source, usertype = "1", app_device_token = mobile_token)
				usertoken.save()
			
			GlobalDict['user_type'] = "1" # club
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp'] = OTP
			GlobalDict['app_device_token'] = str(mobile_token) 
			GlobalDict['status'] = "1"
			GlobalDict['msg'] ="The mobile number is registered as club admin"
		
		elif flag_user == 0 and flag_club_manager == 0 and flag_player == 1:
			player_data = Players.objects.filter(player_mob_no = mob_no, is_active = 1)
			player_data = player_data[len(player_data)-1]
			player_id = player_data.id
			uTE = UsersToken.objects.filter(player_id = player_id).exists()
			if uTE:
				usertoken = UsersToken.objects.get(player_id = player_id)
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "2"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = ""
				usertoken.manager_id = ""
				usertoken.player_id = player_id
				
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token, player_id = player_id, manager_id = "", users_id = "", source_device = source,usertype = "2",app_device_token = mobile_token)
				usertoken.save()
			
			GlobalDict['user_type'] = "2" # player
			GlobalDict['is_player'] = "1"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] ="The mobile number is registered as player"
			
		elif flag_user == 0 and flag_club_manager == 1 and flag_player == 0:
			manager_data = ClubManager.objects.get(manager_mob_no = mob_no)
			manager_id = manager_data.id
			uTE = UsersToken.objects.filter(manager_id = manager_id).exists()
			if uTE:
				usertoken = UsersToken.objects.get(manager_id = manager_id)
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "3"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = ""
				usertoken.manager_id = manager_id
				usertoken.player_id = ""
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token,manager_id = manager_id, player_id = "", users_id = "", source_device = source,usertype = "3",app_device_token = mobile_token)
				usertoken.save()
			
			GlobalDict['user_type'] = "3" # manager
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] ="The mobile number is registered as manager"
			
		elif flag_user == 1 and flag_club_manager == 1 and flag_player == 0:
			manager_data = ClubManager.objects.get(manager_mob_no = mob_no)
			user = Users.objects.get(mobile_no = mob_no)
			manager_id = manager_data.id
			users_id = user.id
			uTE = UsersToken.objects.filter(Q(manager_id = manager_id) | Q(users_id = users_id)).exists()
			
			if uTE:
				usertoken = UsersToken.objects.filter(Q(manager_id = manager_id) | Q(users_id = users_id))
				for tokenuser in usertoken:
					if tokenuser.users_id == users_id:
						usertoken = tokenuser
					elif tokenuser.manager_id == manager_id:
						usertoken = tokenuser
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "4"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = users_id
				usertoken.manager_id = manager_id
				usertoken.player_id = ""
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token,manager_id = manager_id,users_id = users_id, player_id = "", source_device = source,usertype = "4",app_device_token = mobile_token)
				usertoken.save()
				
			GlobalDict['user_type'] = "4" # manager and club
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] = "The mobile number is registered club admin & manager"
			
		elif flag_user == 0 and flag_club_manager == 1 and flag_player == 1:
			manager_data = ClubManager.objects.get(manager_mob_no = mob_no)
			player_data = Players.objects.get(player_mob_no = mob_no, is_active = 1)
			manager_id = manager_data.id
			player_id = player_data.id
			uTE = UsersToken.objects.filter( Q(manager_id = manager_id) | Q(player_id = player_id) ).exists()
			if uTE:
				userstoken = UsersToken.objects.filter(Q(manager_id = manager_id) | Q(player_id = player_id))
				for tokenuser in userstoken:
					if tokenuser.manager_id == manager_id:
						usertoken = tokenuser
					elif tokenuser.player_id == player_id:
						usertoken = tokenuser
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "5"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = ""
				usertoken.manager_id = manager_id
				usertoken.player_id = player_id
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token, manager_id = manager_id, player_id = player_id, users_id = "", source_device = source, usertype = "5", app_device_token = mobile_token)
				usertoken.save()

			GlobalDict['user_type'] = "5" # manager and player
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] = "The mobile number is registerd as manager & player"
			
		elif flag_user == 1 and flag_club_manager == 0 and flag_player == 1:
			GlobalDict = {}
			player_data = Players.objects.get(player_mob_no = mob_no, is_active = 1)
			user = Users.objects.get(mobile_no = mob_no)
			users_id = user.id
			player_id = player_data.id
			uTE = UsersToken.objects.filter(Q(users_id = users_id) | Q(player_id = player_id)).exists()
			if uTE:
				userstoken = UsersToken.objects.filter(Q(users_id = users_id)|Q(player_id = player_id))
				for tokenuser in userstoken:
					if tokenuser.users_id == users_id:
						usertoken = tokenuser
					elif tokenuser.player_id == player_id:
						usertoken = tokenuser				
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "6"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = users_id
				usertoken.manager_id = ""
				usertoken.player_id = player_id
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token, users_id = users_id, player_id = player_id,  manager_id = "", source_device = source, usertype = "6", app_device_token = mobile_token)
				usertoken.save()
				
			GlobalDict['user_type'] = "6" # player and club
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id)
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] = "The mobile number is registerd as club admin & player"
			
		elif flag_user == 1 and flag_club_manager == 1 and flag_player == 1:
			GlobalDict = {}
			managerL = []
			player_data =Players.objects.get(player_mob_no = mob_no, is_active = 1)
			manager_data = ClubManager.objects.get(manager_mob_no = mob_no)
			user = Users.objects.get(mobile_no = mob_no)
			manager_id = manager_data.id
			users_id = user.id
			player_id = player_data.id
			uTE = UsersToken.objects.filter(Q(manager_id = manager_id)|Q(users_id = users_id)|Q(player_id = player_id)).exists()
			
			if uTE:
				usertoken = UsersToken.objects.filter(Q(manager_id = manager_id)|Q(users_id = users_id)|Q(player_id = player_id))
				for tokenuser in usertoken:
					if tokenuser.manager_id == manager_id:
						if tokenuser.users_id == users_id or tokenuser.player_id:
							usertoken = tokenuser.id
					else:
						if tokenuser.users_id == users_id or tokenuser.player_id:
							usertoken = tokenuser.id
				usertoken = UsersToken.objects.get(id = usertoken)
				usertoken.device_token = token
				usertoken.source_device = source
				usertoken.usertype = "7"
				usertoken.app_device_token = mobile_token
				usertoken.users_id = users_id
				usertoken.manager_id = manager_id
				usertoken.player_id = player_id
				usertoken.save()
			else:
				usertoken = UsersToken(device_token = token,manager_id = manager_id, users_id = users_id,player_id = player_id,source_device = source,usertype = "7",app_device_token = mobile_token)
				usertoken.save()

			GlobalDict['user_type'] = "7" # player and club and manager
			GlobalDict['is_player'] = "0"
			GlobalDict['club_admin_id'] = str(usertoken.users_id)
			GlobalDict['manager_id'] = str(usertoken.manager_id)
			GlobalDict['player_id'] = str(usertoken.player_id) if usertoken.player_id is not None else ""
			GlobalDict['otp']= OTP
			GlobalDict['app_device_token'] = str(mobile_token)
			GlobalDict['status'] ="1"
			GlobalDict['msg'] ="The mobile number is registered as club admin, player & manager"

		to = "+44"+mob_no
		try:
			SMS = send_otp_sms(OTP, to)
		except Exception, e:
			class Empty:
				pass
			SMS = Empty()
			SMS.status = "Failed"
			SMS.body = str(e)
		GlobalDict['sms_status'] = SMS.status
		GlobalDict['sms_text'] = str(SMS.body)
		
		return JsonResponse(GlobalDict,safe=False)
		
def club_info(club_admin_id):
	clubList  = []
	ClubDict = {}
	teamCount = 0
	if club_admin_id == "":
		clubE = False
	else:
		clubE = Clubs.objects.filter(users_id = club_admin_id).exists()
	
	if clubE:
		club = Clubs.objects.get(users_id = club_admin_id)
		club_teams = ClubTeams.objects.filter(club_id = club.id,is_active=1)
		teamCount = club_teams.count()
		for team in club_teams:
			if team.club_manager_id is not "":
				player_count = Players.objects.filter(club_team_id = team.id,is_active=1).count()
				rgb_color = str(club.strip_colour)
				if rgb_color:
					hex_color = rgb2hex(rgb_color)
				else:
					hex_color = str("#939496")
					
				if club.logo_photo:
					Logo = 'pic_folder/'+str(club.logo_photo)
				else:
					Logo = defaultLogo
				
				ClubDict['team_count'] = str(teamCount)
				ClubDict['team_id'] = str(team.id)
				ClubDict['team_name'] = str(team.team_name)
				ClubDict['player_count'] = str(player_count)
				ClubDict['age_group_id'] = str(team.agegroup_id)
				ClubDict['age_group'] = str(team.agegroup)
				ClubDict['club_id'] = str(team.club_id)
				ClubDict['club_name'] = str(team.club)
				ClubDict['club_logo'] = Logo
				ClubDict['strip_color'] = str(hex_color)
				ClubDict['is_owner'] = "1"
				ClubDict['manager_id'] = str(team.club_manager_id)
				ClubDict['manager_name'] = str(team.club_manager)
				ClubDict['is_updatable'] = "1"
				clubList.append(ClubDict)
				ClubDict = {}
		return clubList
	else:
		return False
		
def manager_info(manager_id):	
	sortedList = []
	managerList = []
	managerDict = {}
	clubList = []
	if manager_id == "":
		managerE = False
	else:
		managerE = ClubManager.objects.filter(id = manager_id).exists()
	
	if managerE:
		manager = ClubManager.objects.filter(id = manager_id)
		manager_teams = ClubTeams.objects.filter(club_manager_id = manager_id).order_by('club_id')
		
		for man_team in manager_teams:
			clubList.append(man_team.club_id)
		clubList.sort()
		clubList = list(set(clubList))
		
		for club in clubList:
			club_teams = ClubTeams.objects.filter(club_id = club,is_active =1)
			for team in club_teams:
				if team.club_manager_id is not "":
					club_data = Clubs.objects.get(id = team.club_id)
					
					player_count = Players.objects.filter(club_team_id = team.id, is_active=1).count()
					rgb_color = str(club_data.strip_colour)
					if rgb_color:
						hex_color = rgb2hex(rgb_color)
					else:
						hex_color = str("#939496")

					if club_data.logo_photo:
						Logo = 'pic_folder/'+str(club_data.logo_photo)
					else:
						Logo = defaultLogo
					
					if str(team.club_manager_id) == str(manager_id) :
						is_updtbl = "1"
					else:
						is_updtbl = "0"
					
					managerDict['team_id'] = str(team.id)
					managerDict['team_name'] = str(team.team_name)
					managerDict['player_count'] = str(player_count)
					managerDict['age_group_id'] = str(team.agegroup_id)
					managerDict['age_group'] = str(team.agegroup)
					managerDict['club_id'] = str(team.club_id)
					managerDict['club_name'] = str(team.club)
					managerDict['club_logo'] = Logo
					managerDict['strip_color'] = str(hex_color)
					managerDict['is_owner'] = "0"
					managerDict['manager_id'] = str(team.club_manager_id)
					managerDict['manager_name'] = str(team.club_manager)
					managerDict['is_updatable'] = is_updtbl
					
					managerList.append(managerDict)
					managerDict = {}
					
		seen = set()
		for data in managerList:
			t = tuple(data.items())
			if t not in seen:
				seen.add(t)
				sortedList.append(data)
		return sortedList
	else:
		return False
		
@csrf_exempt	
def clubTeamsManager_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		is_updatable = is_owner = "0"
		userType = data['user_type']
		club_admin_id = data['club_admin_id']
		manager_id = data['manager_id']
		app_device_token = data['app_device_token']
		
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		
		if useExists:
			if userType == "1" or userType == "6":
				list = club_info(club_admin_id)
				if bool(list):
					GlobalDict['status']= "1"
					GlobalDict['msg']= "Teams of club"
					GlobalDict['total_teams']= list
				else:
					GlobalDict['status']= "0"
					GlobalDict['msg']= "No teams"
					GlobalDict['total_teams']= []
				
			elif userType == "3" or userType ==  "5" :
				list = manager_info(manager_id)
				if bool(list):
					GlobalDict['status']= "1"
					GlobalDict['msg']= "Teams of manager"
					GlobalDict['total_teams']= list
				else:
					GlobalDict['status']= "0"
					GlobalDict['msg']= "No teams"
					GlobalDict['total_teams']= []
				
			elif userType == "4" or userType == "7":
				team_list = []
				newList = []
				team_ids = set()
				club_list = club_info(club_admin_id)
				manager_list = manager_info(manager_id)
				
				if bool(club_list):
					for team in club_list:
						team_ids.add(str(team["team_id"]))
					team_list += club_list
				if bool(manager_list):
					if bool(club_list):
						for man_team in manager_list:
							for club_team in club_list:
								if str(man_team["team_id"]) == str(club_team["team_id"]):
									if str(man_team["team_id"]) not in team_ids:
										team_ids.add(str(man_team["team_id"]))
										newList.append(man_team)
								else:
									if str(man_team["team_id"]) not in team_ids:
										team_ids.add(str(man_team["team_id"]))
										team_list.append(man_team)
					else:
						for man_team in manager_list:
							if str(man_team["team_id"]) not in team_ids:
								team_ids.add(str(man_team["team_id"]))
								team_list.append(man_team)
					
				if bool(team_list):
					GlobalDict['status'] = "1"
					GlobalDict['msg'] = "All teams"
					GlobalDict['total_teams'] = team_list
				else:
					GlobalDict['status'] = "0"
					GlobalDict['msg'] = "No teams"#+str(bool(club_list) ^ bool(manager_list))
					GlobalDict['total_teams'] = []
			else:
				GlobalDict['status']= "0"
				GlobalDict['msg']= "No teams"#+str("else")+str(userType)
				GlobalDict['total_teams'] = []
				
		else:
			GlobalDict['status']= "2"
			GlobalDict['msg']= "App device token doesn't match...."
		
		return JsonResponse(GlobalDict,safe=False)
			
@csrf_exempt
def deleteClubTeams_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		teams = data["team_id"]
		userType = data['user_type']
		app_device_token = data['app_device_token']
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		if useExists:
			for team in teams:
				clubteam = ClubTeams.objects.get(id = team)
				clubteam.is_active = 0
				clubteam.save()
				notifications = Notifications.objects.filter(users = clubteam.club.users)
				for noti in notifications:
					noti.is_visible = bool(0)
					noti.updated_at = now()
					noti.save()
				player = Players.objects.filter(club_team = clubteam)
				for pl in player:
					pl.is_active = 0
					pl.save()
				club_obj = Clubs.objects.get(id = clubteam.club_id)
				clubteam = ClubTeams.objects.filter(club=club_obj, is_active=1)
				num_teams = clubteam.count()
				allteams = "["
				i = 1
				if num_teams > 0:
					for team in clubteam:
						if i == num_teams:
							allteams += '"'+team.team_name+'"'
						else:
							allteams += '"'+team.team_name+'" ,'
							i += 1
				allteams += "]"
				club_obj.num_teams = num_teams
				club_obj.age_groups = allteams
				club_obj.save()
			GlobalDict['status'] = '1'
			GlobalDict['msg'] = 'Team/s are deleted succesfully...'
		else:
			GlobalDict['status'] = '2'
			GlobalDict['msg'] = "App device token doesn't match...."
	return JsonResponse(GlobalDict, safe = False)
			
####-----load team players-----####
@csrf_exempt
def teamPlayers_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		team_id = data['team_id']
		playerE = Players.objects.filter(club_team_id = team_id).exists()
		GlobalDict = {}
		GlobalList = []
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			if playerE:
				player = Players.objects.filter(club_team_id = team_id,is_active=1).order_by('-created_at')
				for ply in player :
					GlobalDict['player_id'] = str(ply.id)
					# GlobalDict['player_name'] = str(ply.player_name)
					GlobalDict['player_name'] = ply.player_name
					GlobalDict['player_position'] =  str(ply.player_position).capitalize()
					GlobalDict['player_position_id'] = str(ply.player_position_id)
					GlobalDict['player_mobile_no'] = str(ply.player_mob_no)
					if ply.player_img:
						player_img = ply.player_img.url
					else:
						player_img = defaultPlayer
					GlobalDict['player_img'] =  player_img
					GlobalList.append(GlobalDict)
					GlobalDict = {}
				GlobalDict['status'] = "1"
				GlobalDict['msg'] = "Players of the team"
				GlobalDict['team_player'] = GlobalList
				return JsonResponse(GlobalDict, safe = False)
			else:
				GlobalDict1 = {}
				GlobalDict['player_name'] = ""
				GlobalDict['player_position_id'] =  ""
				GlobalDict['player_position'] =  ""
				GlobalDict['player_img'] =  ""
				GlobalList.append(GlobalDict)
				GlobalDict1['status'] = "0"
				GlobalDict1['msg'] = "Please add players"
				GlobalDict1['team_player'] = GlobalList
				return JsonResponse(GlobalDict1, safe = False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
			return JsonResponse(GlobalDict, safe = False)

			
@csrf_exempt
def playerPossition_web(request):
	if request.method =="POST": 
		data = json.loads(request.body)
		# token = data['token_id']
		GlobalDict = {}
		GlobalList = []
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			player_possition = FootballPlayerPositions.objects.all().order_by('id')
			for plyp in player_possition:
				GlobalDict['position_id'] = str(plyp.id)
				GlobalDict['position_name'] = str(plyp.position).capitalize()
				GlobalList.append(GlobalDict)
				GlobalDict = {}
			GlobalDict['status'] = "1"
			GlobalDict['token_id'] = app_device_token
			GlobalDict['msg'] = "Football players position"
			GlobalDict['positions'] = GlobalList
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe = False)

		
####-----insertion of players-----####
@csrf_exempt 
def addPlayer_web(request):
	if request.method == "POST" :
		data = json.loads(request.body)
		GlobalDict =  {}
		p_name = data['name']
		p_position = data['position']
		p_mob_no = data['mobile_no'] 
		p_image = data['image']
		p_team_id = data['team_id']
		plyE = Players.objects.filter(player_mob_no = p_mob_no).exists() 
		managerE = ClubManager.objects.filter(manager_mob_no = p_mob_no).exists() 
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			current_date = datetime.datetime.now().strftime ("%Y-%m-%d")
			# path = '/home/vyclean06/Documents/django10/local-teams/LocalTeams/pic_folder/' #path for loacal
			# path = '/home/vyclean06/Documents/django10/LocalTeams/LocalTeams/pic_folder/' #path for loacal
			path = '/local-teams/LocalTeams/LocalTeams/pic_folder/'
			
			if  plyE:
				GlobalDict['status'] = "0"
				GlobalDict['msg'] = "The mobile number is already registered for another player..."
				GlobalDict['team_id'] = ""
				GlobalDict['player_id'] = ""

			else:
				player = Players(player_name = p_name,player_position_id= p_position,player_mob_no = p_mob_no,club_team_id = p_team_id,created_at = str(datetime.datetime.now()))
				player.save()
				
				if p_image != '':
					imgN = 'player_profile_image/player_img'+'_'+current_date+'-'+p_mob_no+'.jpg'
					destination = open(path+imgN, 'wb')
					destination.write(p_image.decode('base64'))
					destination.close()				
					player.player_img = imgN
					player.save()
				
				GlobalDict['status'] = "1"
				GlobalDict['msg'] = "Player added successfully"
				GlobalDict['team_id'] = str(player.club_team_id)
				GlobalDict['player_id'] = str(player.id)
			return JsonResponse(GlobalDict,safe = False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
			return JsonResponse(GlobalDict,safe = False)


####-----update player-----####
@csrf_exempt
def updatePlayer_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		player_id = data['player_id']
		p_name = data['name']
		p_position = data['position']
		p_mob_no = data['mobile_no'] 
		p_image = data['image']
		
		mobE = Players.objects.filter(player_mob_no = p_mob_no).exists()
		count = 0
		if mobE:
			mobE = Players.objects.filter(player_mob_no = p_mob_no)
			for mob in mobE:
				if mob.id == player_id:
					count += 1
		
		ply = Players.objects.get(id = player_id)
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			
			if count < 2:
				if p_image != "": 
					current_date = datetime.datetime.now().strftime ("%Y-%m-%d")
					# path = '/home/vyclean06/Documents/django10/local-teams/LocalTeams/pic_folder/'
					path = '/local-teams/LocalTeams/LocalTeams/pic_folder/'
					imgN = 'player_profile_image/player_img'+'_'+current_date+'-'+player_id+'.jpg'
					destination = open(path+imgN, 'wb')
					destination.write(p_image.decode('base64'))
					destination.close()
					ply.player_img = imgN
					ply.save()		
					
					
				ply.player_name = p_name
				ply.player_position_id = p_position
				ply.player_mob_no = p_mob_no
				ply.save()

				GlobalDict['status'] = "1"
				GlobalDict['msg'] = "Player updated successfully"
				GlobalDict['player_id'] = str(ply.id)
				return JsonResponse(GlobalDict, safe = False)
			else:
				GlobalDict['status'] = "0"
				GlobalDict['msg'] = "The mobile number is already registered for another player..."
				GlobalDict['player_id'] = ""
				return JsonResponse(GlobalDict, safe = False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['player_id'] = ""
			GlobalDict['msg'] = "App device token doesn't match...."
			return JsonResponse(GlobalDict, safe = False)
		
		
####-----delete player-----####			 
@csrf_exempt
def deleteTeamPlayer_web(request):
	if request.method == "POST":
		data =json.loads(request.body)
		GlobalDict = {}
		player_id = data['player_id']
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		if useExists:
			for pl in player_id:
				# plyE = Players.objects.filter(id = pl).exists()
				# if plyE:
				ply = Players.objects.get(id = pl)
				ply.is_active = 0
				ply.save()
				notifications = Notifications.objects.filter(player = ply)
				for noti in notifications:
					noti.is_visible = bool(0)
					noti.updated_at = now()
					noti.save()
				user_token = UsersToken.objects.filter(player_id = pl)
				if user_token.exists():
					for token in user_token:
						token.delete()
			GlobalDict['status'] = "1"
			GlobalDict['msg'] = "Player deleted successfully"
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe = False)
			# else:
				# GlobalDict['status'] = "0"
				# GlobalDict['msg'] = "this player is not present...."
	
	
@csrf_exempt
def notificationBlock_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		player_id = data['player_id']
		is_notification = int(data['is_notification'])
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			usertoken1 = UsersToken.objects.get(player_id = player_id,app_device_token =app_device_token,usertype = userType)
			usertoken1.is_notification_block = is_notification
			usertoken1.save()
			GlobalDict['status'] = "1"
			if is_notification == 0:
				GlobalDict['msg'] = "Notification on"
			elif is_notification == 1:
				GlobalDict['msg'] = "Notification off"
				
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe = False)
		
@csrf_exempt		
def fixtureCreate_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		manager_list = []
		club_admin_id = None
		
		if 'club_admin_id' in data:
			if data['club_admin_id'] != "":
				club_admin_id = data['club_admin_id']
		
		club_id = data['club_id']
		if data['manager_id'] == "":
			manager_id = None
		else:
			manager_id = data['manager_id']
		# club = Clubs.objects.get(users_id = club_admin_id)
		club = Clubs.objects.get(id = club_id)
		club_id = club.id
		users_id = club.users_id
		rgb_color = str(club.strip_colour)
		if rgb_color:
			hex_color = rgb2hex(rgb_color)
		else:
			hex_color = str("#939496")
		fixture = data['fixture']
		fixture_date = str(data['date']) #dd-mm-yyyy
		fixture_date = datetime.datetime.strptime(fixture_date, '%d-%m-%Y').strftime('%Y-%m-%d') #yyyy-mm-dd
		time = data['time']
		time = datetime.datetime.strptime(time, '%I:%M %p').strftime('%H:%M:%S')
		match_details = data['match_details']
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			fixE = Fixtures.objects.filter(club_id = club_id,fixture = fixture,fixture_date=fixture_date,fixture_time = time, fixture_visibility = 't').exists()
			if fixE:
				GlobalDict['fixture_id'] = ""
				GlobalDict['msg'] = "This fixture is already created.." 
				GlobalDict['status'] = "0"
			else:
				fix = Fixtures(club_id = club_id, fixture = fixture, fixture_date = fixture_date,fixture_time = time, fixture_venue =  match_details,created_at = str(datetime.datetime.now()), resend_at = str(datetime.datetime.now()),created_by = club_id,created_by_m = manager_id)
				fix.save()
				fixture_id = fix.id
				player = data['players_id']
				for pl in player:
					player_id = pl
					ply = Players.objects.get(id = pl)
					usertokenE= UsersToken.objects.filter(player_id = ply.id).exists()
					club_team_id = ply.club_team_id
					squad = Squad(fixture_id = fixture_id, player_id = player_id, club_team_id = club_team_id)
					squad.save()
					squad = Squad.objects.get(id = squad.id)

					cb_tm = ClubTeams.objects.get(id = club_team_id)
					manager_list.append(cb_tm.club_manager_id)
					fixture_name = fix.fixture
					fixture_date = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
					fixture_time = str(datetime.datetime.strftime(datetime.datetime.strptime(fix.fixture_time, "%H:%M:%S"), "%I:%M %p"))
					kick_off = fixture_date+" @ "+fixture_time
					
					if usertokenE:
						usertoken = UsersToken.objects.get(player_id = ply.id)
						# usertoken = usertoken[0]
						get_data = {}
						get_data["body"] = "Please confirm your availability for "+fix.fixture
						get_data["fixture_name"] = fixture_name
						get_data["tab"] = "av"
						get_data["strip_color"] = hex_color
						get_data["club_name"] = club.club_name
						get_data["tag"] = "availablity"
						get_data["id"] = str(fix.id)
						get_data["kick_off"] = kick_off
						get_data["fixture_date"] = fixture_date
						get_data["match_details"] = fix.fixture_venue
						get_data["click_action"] = "av"
						get_data["userType"] = usertoken.usertype
						get_data["usertoken"] = usertoken.id

						token_hex = usertoken.device_token
						source = usertoken.source_device
						sound = usertoken.notification_tone
						if sound:
							get_data['sound'] = sound
						else:
							get_data['sound'] = "Default"
						if source == "i":
							data1 = push_notification_ios_web(get_data,token_hex)
						else :
							data1 = push_notification_android_web(get_data,token_hex)
						squad.notification = "1"
						squad.save()
				
				if manager_list:
					managers = list(sorted(set(manager_list)))
					for manager in managers:
						if manager == manager_id:
							pass
						else:
							usertoken = UsersToken.objects.filter(manager_id = manager)
							if usertoken.exists():
								get_data = {}
								get_data["fixture_name"] = fixture_name
								get_data["body"] = "New fixture "+fixture_name+" on "+kick_off
								get_data["click_action"] = "rv"
								get_data["tag"] = "review"
								get_data["strip_color"] = hex_color
								get_data["club_name"] = club.club_name
								get_data["tab"] = "rv"
								get_data["id"] = str(fixture_id)
								get_data["kick_off"] = kick_off
								get_data["fixture_date"] = fixture_date
								get_data["match_details"] = fix.fixture_venue
								usertoken = usertoken[0]
								token_hex = usertoken.device_token
								get_data["userType"] = usertoken.usertype
								source = usertoken.source_device
								is_notification_block = usertoken.is_notification_block
								get_data["usertoken"] = usertoken.id
								if token_hex:
									if source == "i":
										if is_notification_block == 0:
											dataI = push_notification_ios_web(get_data,token_hex)
									if source == "a" :
										if is_notification_block == 0:
											dataA = push_notification_android_web(get_data,token_hex)
				
				if club_admin_id == None:
					usertoken = UsersToken.objects.filter(users_id = users_id)
					if usertoken.exists():
						get_data = {}
						get_data["fixture_name"] = fixture_name
						get_data["body"] = "New fixture "+fixture_name+" on "+kick_off
						get_data["click_action"] = "rv"
						get_data["tag"] = "review"
						get_data["strip_color"] = hex_color
						get_data["club_name"] = club.club_name
						get_data["tab"] = "rv"
						get_data["id"] = str(fixture_id)
						get_data["kick_off"] = kick_off
						get_data["fixture_date"] = fixture_date
						get_data["match_details"] = fix.fixture_venue
						usertoken = usertoken[0]
						token_hex = usertoken.device_token
						get_data["userType"] = usertoken.usertype
						source = usertoken.source_device
						is_notification_block = usertoken.is_notification_block
						get_data["usertoken"] = usertoken.id
						if token_hex:
							if source == "i":
								if is_notification_block == 0:
									dataI = push_notification_ios_web(get_data,token_hex)
							if source == "a" :
								if is_notification_block == 0:
									dataA = push_notification_android_web(get_data,token_hex)
					
				
				GlobalDict['fixture_id'] = str(fixture_id)
				GlobalDict['msg'] = "Fixture notification sent to selected players" 
				GlobalDict['status'] = "1"
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe = False)
		
@csrf_exempt
def fixtureDelete_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		userType = data['user_type']
		app_device_token = data['app_device_token']
		fixture_id = data['fixture_id']
		GlobalDict ={}
		GlobalList =[]
		for id in fixture_id:
			Fix = Fixtures.objects.get(id = id)
			Fix.fixture_visibility = False
			Fix.save()
			notifications = Notifications.objects.filter(fixture = Fix)
			for noti in notifications:
				noti.is_visible = bool(0)
				noti.updated_at = now()
				noti.save()
		GlobalDict['msg']= "Fixtures are deleted.."
		GlobalDict['status']= "1"
	return JsonResponse(GlobalDict, safe= False)
	
@csrf_exempt
def availablityHide_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		userType = data['user_type']
		app_device_token = data['app_device_token']
		fixture_id = data['fixture_id']
		player_id = data['player_id']
		GlobalDict ={}
		GlobalList =[]
		for id in fixture_id:
			player_fix = Squad.objects.filter(fixture_id = id, player_id = player_id)
			for fix in player_fix:
				fix.hide_fix = True
				fix.save()
		GlobalDict['msg']= "Notifications are deleted.."
		GlobalDict['status']= "1"
	return JsonResponse(GlobalDict, safe= False)
		
def users_club_info(club_admin_id):
	clubList = []
	clubDict = {}
	if club_admin_id == "":
		clubList = False
	else:
		club = Clubs.objects.get(users_id = club_admin_id)
		rgb_color = str(club.strip_colour)
		if rgb_color:
			hex_color = rgb2hex(rgb_color)
		else:
			hex_color = str("#939496")
			
		clubDict['club_id'] = str(club.id)
		clubDict['club_name'] = str(club.club_name)
		clubDict['strip_color'] = str(hex_color)
		clubList.append(clubDict)
		clubDict = {}
	return clubList
		
def manager_club_info(manager_id):
	managerList = []
	managerDict = {}
	usersList = []
	if manager_id == "":
		managerList = False
	else:
		man_teams = ClubTeams.objects.filter(club_manager_id = manager_id,is_active =1)
		for teams in man_teams:
			usersList.append(teams.club_id)
		usersList.sort()
		usersList = list(set(usersList))
		for user in usersList:
			club = Clubs.objects.get(id = user)
			rgb_color = str(club.strip_colour)
			if rgb_color:
				hex_color = rgb2hex(rgb_color)
			else:
				hex_color = str("#939496")
			
			managerDict['club_id'] = str(club.id)
			managerDict['club_name'] = str(club.club_name)
			managerDict['strip_color'] = str(hex_color)
			managerList.append(managerDict)
			managerDict = {}
	return managerList
	
@csrf_exempt
def userClubs_web(request):	
	if request.method == "POST":
		data = json.loads(request.body)
		userType = data['user_type']
		club_admin_id = data['club_admin_id']
		manager_id = data['manager_id']
		app_device_token = data['app_device_token']
		GlobalDict = {}
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		if useExists:
			if userType == "1" or userType == "6":			
				list = users_club_info(club_admin_id)
				if bool(list):
					GlobalDict['status']= "1"
					GlobalDict['msg']= "Clubs"
					GlobalDict['clubs']= list
				else:
					GlobalDict['status']= "0"
					GlobalDict['msg']= "No clubs"
					GlobalDict['total_teams']= []
					
			elif userType == "3" or userType == "5":
				list = manager_club_info(manager_id)
				if bool(list):
					GlobalDict['status']= "1"
					GlobalDict['msg']= "Clubs"
					GlobalDict['clubs']= list
				else:
					GlobalDict['status']= "0"
					GlobalDict['msg']= "No clubs"
					GlobalDict['total_teams']= []
				
			elif userType == "4" or userType == "7":
				allLists = []
				userClub = users_club_info(club_admin_id)
				managerClub = manager_club_info(manager_id)
				
				if bool(userClub):
					allLists += userClub
				
				if bool(managerClub):
					for user in managerClub:
						if user["club_id"] != userClub[0]["club_id"]:
							allLists.append(user)
					
				if bool(allLists):
					GlobalDict['status']= "1"
					GlobalDict['msg']= "Clubs"
					GlobalDict['clubs']= allLists
				else:
					GlobalDict['status']= "0"
					GlobalDict['msg']= "No clubs"
					GlobalDict['total_teams']= []
			else:
				GlobalDict['status']= "0"
				GlobalDict['msg']= "No clubs"
				GlobalDict['total_teams']= []
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
			GlobalDict['clubs'] = []
			
		return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def clubTeams_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		club_id = data['club_id']
		GlobalDict = {}
		GlobalList = []
		teamCount = 0
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			club = Clubs.objects.filter(id = club_id)
			if club.exists():
				club = club[0]
				ground = Grounds.objects.get(clubs = club)
				match_details = ""+str(ground.ground_name)+", "+str(ground.street)+", "+str(ground.town)+", "+str(ground.county)+", "+str(ground.postcode)+", Meet up time: 14:00, Please confirm if you are available?"
			
			clubteamsE = ClubTeams.objects.filter(club_id = club_id,is_active = 1).exists()
			if clubteamsE:
				clubteams = ClubTeams.objects.filter(club_id = club_id,is_active = 1)
				teamCount = clubteams.count()
				for ct in clubteams:
					if ct.club_manager_id is not "":
						GlobalDict['team_id'] = str(ct.id)
						GlobalDict['team_name'] = str(ct.team_name)
						GlobalList.append(GlobalDict)
						GlobalDict ={}
				GlobalDict['status']= "1"
				GlobalDict['msg']= "Teams of club"
				GlobalDict['team']= GlobalList
				GlobalDict['team_count']= str(teamCount)
				GlobalDict['match_details']= match_details
			else:
				GlobalDict['team_id'] = ""
				GlobalDict['team_name'] = ""
				GlobalList.append(GlobalDict)
				GlobalDict ={}
				GlobalDict['status']= "0"
				GlobalDict['msg']= "Teams does not available"
				GlobalDict['team']= GlobalList
				GlobalDict['team_count']= str(teamCount)
				GlobalDict['match_details']= ""
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe = False)

@csrf_exempt
def teamPlayerSquad_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		team= data['team_id']
		GlobalDict = {}
		GlobalList = []
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			if team == []:
				GlobalDict1 = {} 
				GlobalDict['player_name'] = ""
				GlobalDict['player_position_id'] =  ""
				GlobalDict['player_position'] =  ""
				GlobalDict['player_img'] =  ""
				GlobalList.append(GlobalDict)
				GlobalDict1['status'] = "0"
				GlobalDict1['msg'] = "Players not avaiable"
				GlobalDict1['team_player'] = GlobalList
				return JsonResponse(GlobalDict1, safe = False)
			else:
				for tm in team:
					player = Players.objects.filter(club_team_id = tm,is_active=1)
					for ply in player :
						GlobalDict['player_id'] = str(ply.id)
						GlobalDict['player_name'] = ply.player_name
						GlobalDict['player_position_id'] =  str(ply.player_position_id)
						GlobalDict['player_position'] =  str(ply.player_position).capitalize()
						if ply.player_img:
							player_img = ply.player_img.url
						else:
							player_img = defaultPlayer
						GlobalDict['player_img'] =  player_img
						GlobalList.append(GlobalDict)
						GlobalDict = {}
				GlobalDict['status'] = "1"
				GlobalDict['msg'] = ""
				GlobalDict['team_player'] = GlobalList
				return JsonResponse(GlobalDict, safe = False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
			return JsonResponse(GlobalDict, safe = False)
	
'''@csrf_exempt 
def notification_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		fixList = []
		users_id = data['club_admin_id']
		
		if data['manager_id'] == "":
			manager_id = ""
		else:
			manager_id = data['manager_id']
		
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			UserE = Clubs.objects.filter(users_id = users_id).exists()
			if UserE:
				club = Clubs.objects.get(users_id = users_id)
				if manager_id:
					fixtE = Fixtures.objects.filter( Q(club_id = club.id) | Q(created_by_m = manager_id), fixture_visibility = 't').exists()
				else:
					fixtE = Fixtures.objects.filter(club_id = club.id, fixture_visibility = 't').exists()
				
				if fixtE:
					if manager_id:
						fixt = Fixtures.objects.filter( Q(club_id = club.id) | Q(created_by_m = manager_id), fixture_visibility = 't').order_by('-fixture_date','-fixture_time')
					else:
						fixt = Fixtures.objects.filter(club_id = club.id, fixture_visibility = 't').order_by('-fixture_date','-fixture_time')
						
					for fix in fixt:
						GlobalDict['fixture_id']= str(fix.id)
						GlobalDict['club_id']= str(fix.club_id)
						GlobalDict['fixture']= str(fix.fixture)
						fix_date = str( datetime.datetime.strftime(datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d"), "%d.%m.%Y") )
						GlobalDict['fixture_date']= str(fix_date)
						fix_time = datetime.datetime.strptime(str(fix.fixture_time), '%H:%M:%S').strftime('%I:%M %p')
						GlobalDict['fixture_time']= str(fix_time)
						club1E = Clubs.objects.filter(id = fix.club_id).exists()
						if club1E:
							club1 = Clubs.objects.get(id = fix.club_id)
							GlobalDict['club_name'] = club1.club_name
							rgb_color = str(club1.strip_colour)
							# hex_color = rgb2hex(rgb_color)
							if rgb_color:
								hex_color = rgb2hex(rgb_color)
							else:
								hex_color = str("#939496")
							GlobalDict['strip_color'] = hex_color
							if club1.logo_photo:
								GlobalDict['club_logo'] = club1.logo_photo.url
							else:
								GlobalDict['club_logo'] = ""
						else:
							GlobalDict['strip_color'] = ""
							GlobalDict['club_name'] = ""
							GlobalDict['club_logo'] = ""
						
						
						fixture_created_at = str(fix.created_at).split(" ")
						fix_created_date = str( datetime.datetime.strftime(datetime.datetime.strptime(str(fixture_created_at[0]), "%Y-%m-%d"), "%d.%m.%Y") )
						# fix_created_date = fix_created[0].replace("-",".")
						fix_created_time = str(fixture_created_at[1]).split('.')[0]
						
						GlobalDict['fixture_created_date']=fix_created_date 
						GlobalDict['fixture_created_time']=str( datetime.datetime.strptime(fix_created_time, '%H:%M:%S').strftime('%I:%M %p') )
						
						GlobalDict['fixture_venue']= str(fix.fixture_venue)
						fixList.append(GlobalDict)
						GlobalDict = {}
					GlobalDict['status']="1"
					GlobalDict['msg']="Reviews"
					GlobalDict['fixtures']= fixList
					return JsonResponse(GlobalDict,safe = False)

				else:
					GlobalDict1 = {}
					GlobalDict['fixture_id']= ""
					GlobalDict['club_id']= ""
					GlobalDict['fixture']= ""
					GlobalDict['fixture_date']= ""
					GlobalDict['fixture_time']= ""
					GlobalDict['fixture_venue']= ""
					GlobalDict['strip_color'] = ""
					GlobalDict['club_name'] = ""
					GlobalDict['club_logo'] = ""

					fixList.append(GlobalDict)
					GlobalDict1['status'] = "0"
					GlobalDict1['msg'] = "This club has no fixtures"
					GlobalDict1['fixtures'] = fixList
					return JsonResponse(GlobalDict1,safe = False)
			else:
				# GlobalDict2 = {}
				GlobalDict['fixture_id']= ""
				GlobalDict['club_id']= ""
				GlobalDict['fixture']= ""
				GlobalDict['fixture_date']= ""
				GlobalDict['fixture_time']= ""
				GlobalDict['fixture_venue']= ""
				GlobalDict['strip_color'] = ""
				GlobalDict['club_name'] = ""
				GlobalDict['club_logo'] = ""

				fixList.append(GlobalDict)
				GlobalDict = {}
				GlobalDict['status'] = "0"
				GlobalDict['msg'] = "Invalid club"
				GlobalDict['fixtures'] = fixList
			return JsonResponse(GlobalDict,safe = False)
		else:
			GlobalDict['fixture_id']= ""
			GlobalDict['club_id']= ""
			GlobalDict['fixture']= ""
			GlobalDict['fixture_date']= ""
			GlobalDict['fixture_time']= ""
			GlobalDict['fixture_venue']= ""
			GlobalDict['strip_color'] = ""
			GlobalDict['club_name'] = ""
			GlobalDict['club_logo'] = ""

			fixList.append(GlobalDict)
			GlobalDict['status']="2"
			GlobalDict['msg']="App device token doesn't match...."
			return JsonResponse(GlobalDict , safe = False)
'''

	# fixList = []
	# GlobalDict = {}
	
		# return GlobalDict
	
def fixLists(fixt):
	GlobalDict = {}
	fixList = []
	for fix in fixt:
		GlobalDict['fixture_id'] = str(fix.id)
		GlobalDict['club_id'] = str(fix.club_id)
		GlobalDict['fixture'] = fix.fixture
		fix_date = str( datetime.datetime.strftime(datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d"), "%d.%m.%Y") )
		GlobalDict['fixture_date']= str(fix_date)
		fix_time = datetime.datetime.strptime(str(fix.fixture_time), '%H:%M:%S').strftime('%I:%M %p')
		GlobalDict['fixture_time']= str(fix_time)
		club1E = Clubs.objects.filter(id = fix.club_id).exists()
		if club1E:
			club1 = Clubs.objects.get(id = fix.club_id)
			GlobalDict['club_name'] = club1.club_name
			rgb_color = str(club1.strip_colour)
			if rgb_color:
				hex_color = rgb2hex(rgb_color)
			else:
				hex_color = str("#939496")
			GlobalDict['strip_color'] = hex_color
			if club1.logo_photo:
				GlobalDict['club_logo'] = club1.logo_photo.url
			else:
				GlobalDict['club_logo'] = defaultLogo
		else:
			GlobalDict['strip_color'] = ""
			GlobalDict['club_name'] = ""
			GlobalDict['club_logo'] = ""
		fixture_created_at = str(fix.created_at).split(" ")
		fix_created_date = str( datetime.datetime.strftime(datetime.datetime.strptime(str(fixture_created_at[0]), "%Y-%m-%d"), "%d.%m.%Y") )
		fix_created_time = str(fixture_created_at[1]).split('.')[0]
		
		GlobalDict['fixture_created_date']=fix_created_date 
		GlobalDict['fixture_created_time']=str( datetime.datetime.strptime(fix_created_time, '%H:%M:%S').strftime('%I:%M %p') )
		
		GlobalDict['fixture_venue']= str(fix.fixture_venue)
		fixList.append(GlobalDict)
		GlobalDict = {}
		
	return fixList

@csrf_exempt 
def notification_web(request):	#Reviews
	if request.method == "POST":
		data = json.loads(request.body)
		GlobalDict = {}
		fixList = []
		users_id = data['club_admin_id']
		if users_id == "":
			users_id = 0
		
		if data['manager_id'] == "":
			manager_id = ""
		else:
			manager_id = data['manager_id']
		
		ElseDict = {}
		ElseDict['fixture_id'] = ""
		ElseDict['club_id'] = ""
		ElseDict['fixture'] = ""
		ElseDict['fixture_date'] = ""
		ElseDict['fixture_time'] = ""
		ElseDict['fixture_venue'] = ""
		ElseDict['strip_color'] = ""
		ElseDict['club_name'] = ""
		ElseDict['club_logo'] = ""
		
		app_device_token = data['app_device_token']
		userType = str(data['user_type'])
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		
		if useExists:
			if userType in ["3","5"]:
				clubteam = ClubTeams.objects.filter(club_manager_id = manager_id)
				if clubteam.exists():
					club_teams = []
					club_ids = []
					user_ids = []
					for club in clubteam:
						club_teams.append(club.id)
						club_ids.append(club.club_id)
						clubData = Clubs.objects.filter(id = club.club_id)
						user_ids.append(clubData[0].users_id)
						
					players = []
					sqd_fix_ids = []
					if club_teams:
						for team in clubteam:
							plys = Players.objects.filter(club_team_id = team)
							if plys.exists():
								for player in plys:
									players.append(player.id)
					
					
					if players:
						sqd_fix = Squad.objects.filter(player_id__in = players).order_by('fixture_id')
						for fix in sqd_fix:
							sqd_fix_ids.append(fix.fixture_id)
						
						sqd_fix_ids = list(sorted(set(sqd_fix_ids)))
					
					club_ids = list(sorted(set(club_ids)))
					user_ids = list(sorted(set(user_ids)))
					
					fixt = Fixtures.objects.filter( Q(club_id__in = club_ids) | Q(created_by_m = manager_id) | Q(created_by__in = user_ids) | Q(id__in = sqd_fix_ids), fixture_visibility = True).order_by('-fixture_date','-fixture_time')
					
					fixList = fixLists(fixt)
					GlobalDict['status']="1"
					GlobalDict['msg']="Reviews"
					GlobalDict['fixtures']= fixList
				else:
					fixList.append(ElseDict)
					GlobalDict['status'] = "0"
					GlobalDict['msg'] = "This club has no fixtures IF 35"
					GlobalDict['fixtures'] = fixList
					# return JsonResponse(GlobalDict,safe = False)
			
			elif userType in ["1","6"]:
				UserE = Clubs.objects.filter(users_id = users_id).exists()
				if UserE:
					club = Clubs.objects.get(users_id = users_id)
					# # if manager_id:
						# # fixtE = Fixtures.objects.filter( Q(club = club) | Q(created_by_m = manager_id), fixture_visibility = True)
					# # else:
						# # fixtE = Fixtures.objects.filter(club = club, fixture_visibility = True)
					fixtE = Fixtures.objects.filter(club = club, fixture_visibility = True)
					if fixtE.exists():
						# # # if manager_id:
							# # # fixt = Fixtures.objects.filter( Q(club_id = club.id) | Q(created_by_m = manager_id), fixture_visibility = 't').order_by('-fixture_date','-fixture_time')
						# # # else:
							# # # fixt = Fixtures.objects.filter(club_id = club.id, fixture_visibility = 't').order_by('-fixture_date','-fixture_time')
						fixt = Fixtures.objects.filter(club_id = club.id, fixture_visibility = 't').order_by('-fixture_date','-fixture_time')
							
						fixList = fixLists(fixt)					
						
						GlobalDict['status']="1"
						GlobalDict['msg']="Reviews"
						GlobalDict['fixtures']= fixList
						# return JsonResponse(GlobalDict,safe = False)
					else:
						fixList.append(ElseDict)
						GlobalDict['status'] = "0"
						GlobalDict['msg'] = "This club has no fixtures"
						GlobalDict['fixtures'] = fixList
						# return JsonResponse(GlobalDict,safe = False)
				else:
					fixList.append(ElseDict)
					GlobalDict['status'] = "0"
					GlobalDict['msg'] = "Invalid club"
					GlobalDict['fixtures'] = fixList
				# return JsonResponse(GlobalDict,safe = False)
			elif userType in ["4","7"]:
				clubteam = ClubTeams.objects.filter(club_manager_id = manager_id)
				if clubteam.exists():
					club_teams = []
					club_ids = []
					user_ids = []
					for club in clubteam:
						club_teams.append(club.id)
						club_ids.append(club.club_id)
						clubData = Clubs.objects.filter(id = club.club_id)
						user_ids.append(clubData[0].users_id)
					
					players = []
					sqd_fix_ids = []
					if club_teams:
						for team in clubteam:
							plys = Players.objects.filter(club_team_id = team)
							if plys.exists():
								for player in plys:
									players.append(player.id)
					
					if players:
						sqd_fix = Squad.objects.filter(player_id__in = players).order_by('fixture_id')
						for fix in sqd_fix:
							sqd_fix_ids.append(fix.fixture_id)
						
						sqd_fix_ids = list(sorted(set(sqd_fix_ids)))
					
					club_ids = list(sorted(set(club_ids)))
					user_ids = list(sorted(set(user_ids)))
					
				UserE = Clubs.objects.filter(users_id = users_id).exists()
				club_id = False
				if UserE:
					club = Clubs.objects.get(users_id = users_id)
					club_id = club.id
				
				if users_id in user_ids:
					pass
				else:
					user_ids.append(users_id)
				
				if club_id:
					if club in club_ids:
						pass
					else:
						club_ids.append(club_id)
				
				club_ids = list(sorted(set(club_ids)))
				user_ids = list(sorted(set(user_ids)))
					
				fixt = Fixtures.objects.filter( Q(club_id__in = club_ids) | Q(created_by_m = manager_id) | Q(created_by__in = user_ids) | Q(id__in = sqd_fix_ids), fixture_visibility = True).order_by('-fixture_date','-fixture_time')
				
				if fixt:
					fixList = fixLists(fixt)
					GlobalDict['status']="1"
					GlobalDict['msg']="Reviews"
					GlobalDict['fixtures']= fixList
				else:
					GlobalDict['status'] = "0"
					GlobalDict['msg'] = "This club has no fixtures ELIF 47"
					GlobalDict['fixtures'] = fixList
		else:
			fixList.append(ElseDict)
			GlobalDict['status']="2"
			GlobalDict['msg']="App device token doesn't match...."
			# return JsonResponse(GlobalDict , safe = False)
		return JsonResponse(GlobalDict , safe = False)
		
@csrf_exempt		
def playerNotification_web(request): #Availability
	if request.method == "POST":
		data = json.loads(request.body)
		if str(data['player_id']) == str('""'):
			player_id = 0
		else:
			player_id = data['player_id']
		# player_id  = str(data['player_id'])
		GlobalDict = {}
		GlobalList = []
		# if userType in allowed_users:
		if player_id:
			squadEx = Squad.objects.filter(player_id = player_id, hide_fix = "f").exists()
			app_device_token = data['app_device_token']
			useExists = UsersToken.objects.filter(app_device_token = app_device_token,player_id= player_id).exists()
			if useExists:
				if squadEx:
					squad = Fixtures.objects.raw( "select fx.id, sqd.fixture_id, fx.fixture, fx.fixture_date, fx.fixture_time, fx.fixture_venue, fx.fixture_confirm, fx.created_at, fx.created_by, fx.club_id, sqd.squad_confirm, sqd.status_id from \"LocalTeamsApp_squad\" as sqd inner join \"LocalTeamsApp_fixtures\" as fx on sqd.fixture_id = fx.id where player_id = %s AND fx.fixture_visibility = 't' AND sqd.hide_fix = 'f' order by fx.fixture_date desc, fx.resend_at desc, fx.created_at desc",[player_id] )
					for sq in squad:
						if str(sq.fixture_confirm) == "True":
							GlobalDict["is_confirm"] = "1"
							# GlobalDict["player_list"] = PlayersList
						else:
							GlobalDict["is_confirm"] = "0"
							# GlobalDict["player_list"] = []
							
						GlobalDict['fixture_id'] = str(sq.fixture_id)
						GlobalDict['status_id'] = str(sq.status_id)
						GlobalDict['fixture']= sq.fixture
						fix_date = str(datetime.datetime.strftime(datetime.datetime.strptime(str(sq.fixture_date), "%Y-%m-%d"), "%d.%m.%Y"))
						GlobalDict['fixture_date']= str(fix_date)
						fix_time = datetime.datetime.strptime(str(sq.fixture_time), '%H:%M:%S').strftime('%I:%M %p')
						GlobalDict['fixture_time']= str(fix_time)
						GlobalDict['fixture_venue']= str(sq.fixture_venue)
						
						fixture_created_at = str(sq.resend_at).split(" ")
						fix_created_date = str(datetime.datetime.strftime(datetime.datetime.strptime(str(fixture_created_at[0]), "%Y-%m-%d"), "%d.%m.%Y"))
						fix_created_time = datetime.datetime.strptime(str(fixture_created_at[1].split(".")[0]), '%H:%M:%S').strftime('%I:%M %p')

						GlobalDict['fixture_created_date']=fix_created_date 
						GlobalDict['fixture_created_time']=fix_created_time
						
						club_id = sq.club_id
						clubE = Clubs.objects.filter(id = club_id).exists()
						if clubE:
							club_data = Clubs.objects.get(id = club_id)
							GlobalDict['club_name'] = club_data.club_name
							rgb_color = str(club_data.strip_colour)
							# hex_color = rgb2hex(rgb_color)
							if rgb_color:
								hex_color = rgb2hex(rgb_color)
							else:
								hex_color = str("#939496")
							GlobalDict['strip_color'] = hex_color
							if club_data.logo_photo:
								GlobalDict['club_logo'] = club_data.logo_photo.url
							else:
								GlobalDict['club_logo'] = defaultLogo
						else:
							GlobalDict['club_logo'] = ""
							GlobalDict['club_name'] = ""
							GlobalDict['strip_color'] = ""
							
						GlobalList.append(GlobalDict)
						GlobalDict = {}
					GlobalDict1 = {}
					GlobalDict1['status'] = "1"
					GlobalDict1['msg'] = "Notification for player"
					GlobalDict1['notifications_player'] = GlobalList
				else:
					GlobalDict1 = {}
					GlobalDict['fixture_id'] = ""
					GlobalDict['fixture']= ""
					GlobalDict['fixture_date']= ""
					GlobalDict['fixture_time']= ""
					GlobalDict['fixture_venue']= ""
					GlobalList.append(GlobalDict)
					GlobalDict1['status'] = "0"
					GlobalDict1['msg'] = "Notification blank"
					GlobalDict1['notifications_player'] = GlobalList
			elif squadEx == False:
				GlobalDict1 = {}
				GlobalDict['fixture_id'] = ""
				GlobalDict['fixture']= ""
				GlobalDict['fixture_date']= ""
				GlobalDict['fixture_time']= ""
				GlobalDict['fixture_venue']= ""
				GlobalList.append(GlobalDict)
				GlobalDict1['status'] = "0"
				GlobalDict1['msg'] = "Notification blank"
				GlobalDict1['notifications_player'] = GlobalList
			else:
				GlobalDict1 = {}
				GlobalDict1['status'] = "2"
				GlobalDict1['msg'] = "App device token doesn't match...."
		else:
			GlobalDict1 = {}
			GlobalDict['fixture_id'] = ""
			GlobalDict['fixture']= ""
			GlobalDict['fixture_date']= ""
			GlobalDict['fixture_time']= ""
			GlobalDict['fixture_venue']= ""
			GlobalList.append(GlobalDict)
			GlobalDict1['status'] = "0"
			GlobalDict1['msg'] = "The mobile number not registered as player..."
			GlobalDict1['notifications_player'] = GlobalList
		# else:
			# GlobalDict1 = {}
			# GlobalDict['fixture_id'] = ""
			# GlobalDict['fixture']= ""
			# GlobalDict['fixture_date']= ""
			# GlobalDict['fixture_time']= ""
			# GlobalDict['fixture_venue']= ""
			# GlobalList.append(GlobalDict)
			# GlobalDict1['status'] = "0"
			# GlobalDict1['msg'] = "Notification blank"
			# GlobalDict1['notifications_player'] = GlobalList
		
		return JsonResponse(GlobalDict1 , safe = False)
@csrf_exempt	
def fixDetails_web(request):
	if request.method == "POST":
		GlobalDict = {}
		data = json.loads(request.body)
		fixture_id  = data['fixture_id']
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			fix = Fixtures.objects.get(id = fixture_id)
			GlobalDict['status']= str(1)
			GlobalDict['msg']= "Fixture details for player"
			GlobalDict['fixture']= fix.fixture
			fix_date = str(datetime.datetime.strftime(datetime.datetime.strptime(str(fix.fixture_date), "%Y-%m-%d"), "%d.%m.%Y"))
			fix_time = str(datetime.datetime.strptime(str(str(fix.fixture_time).split(".")[0]), '%H:%M:%S').strftime('%I:%M %p'))
			GlobalDict['fixture_date']= str(fix_date)
			GlobalDict['fixture_time']= str(fix_time)
			GlobalDict['fixture_venue']= str(fix.fixture_venue)
			if str(fix.fixture_confirm) == "True":
				PlayerDict = {}
				PlayersList = []
				squad_data = Squad.objects.filter(fixture_id = fixture_id, squad_confirm = 't')
				for player in squad_data:
					player_info = Players.objects.get(id = player.player_id)
					PlayerDict["player_id"] = str(player_info.id)
					PlayerDict["player_name"] = player_info.player_name
					if player_info.player_img:
						player_img = player_info.player_img.url
					else:
						player_img = defaultPlayer
					PlayerDict["player_img"] = str(player_img)
					PlayerDict['player_position_id'] =  str(player_info.player_position_id)
					PlayerDict['player_position'] =  str(player_info.player_position).capitalize()
					PlayersList.append(PlayerDict)
					PlayerDict = {}
				GlobalDict["is_confirm"] = "1"
				GlobalDict["player_list"] = PlayersList
			else:
				GlobalDict["is_confirm"] = "0"
				GlobalDict["player_list"] = []
			return JsonResponse(GlobalDict , safe = False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] = "App device token doesn't match...."
			GlobalDict['fixture']= ""
			GlobalDict['fixture_date']= ""
			GlobalDict['fixture_time']= ""
			GlobalDict['fixture_venue']= ""
			return JsonResponse(GlobalDict, safe = False)
	
@csrf_exempt
def playerResponse_web(request):
	if request.method == "POST":
		# get_data = {}
		data = json.loads(request.body)
		player_id = data['player_id']
		GlobalDict = {}
		userTokens = []
		plE = Players.objects.filter(id = player_id).exists()
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token = app_device_token,usertype = userType).exists()
		if useExists:
			if plE :
				pl = Players.objects.get(id = player_id)
				club_team_id = pl.club_team_id
				club_team = ClubTeams.objects.get(id = club_team_id)
				manager_id = club_team.club_manager_id
				fixture_id = data['fixture_id']
				fix = Fixtures.objects.get(id = fixture_id)
				club = Clubs.objects.get(id = fix.club_id)
				usersData = Users.objects.get(id = club.users_id)
				webToken = usersData.browser_token
				user_token_uE = UsersToken.objects.filter(users_id = club.users_id).exists()
				user_token_tm = UsersToken.objects.filter(manager_id = manager_id)
				if user_token_uE:
					user_token_u = UsersToken.objects.filter(users_id = club.users_id)[0]
					userTokens.append(user_token_u.id)
				else:
					user_token_u = ""
					
				if fix.created_by_m:
					user_token_mE = UsersToken.objects.filter(manager_id = fix.created_by_m).exists()
				else:
					user_token_mE = False
					
				if user_token_mE:
					user_token_m = UsersToken.objects.get(manager_id = fix.created_by_m)
					userTokens.append(user_token_m.id)
				else:
					user_token_m = ""
				
				if user_token_tm.exists():
					ply_tm_man = user_token_tm[0]
					userTokens.append(ply_tm_man.id)
				else:
					ply_tm_man = ""
				
				if userTokens:
					userTokens = list(set(userTokens))
					not_cnt = len(userTokens)
				else:
					not_cnt = 0	
				
				availablity_status = data['availablity_status']
				rgb_color = str(club.strip_colour)
				if rgb_color:
					hex_color = rgb2hex(rgb_color)
				else:
					hex_color = str("#939496")
				stE = Status.objects.filter(id = availablity_status).exists()
				if stE :
					ply = Squad.objects.get(fixture_id = fixture_id,player_id = player_id)
					ply.status_id = availablity_status
					ply.save()
					player = Players.objects.get(id = player_id)
					
					click_action = "https://local-teams.com/clubNotif/"+str(fix.id)
					
					notiEx = Notifications.objects.filter(users = club.users, fixture = fix, player = player).exists()
					
					message_body = player.player_name+" is "+str(ply.status)+" for "+fix.fixture
					if notiEx:
						notif = Notifications.objects.get(users = club.users, fixture = fix, player = player)
						notif.player_status = str(ply.status)
						notif.read = 0
						notif.updated_at = now()
						notif.save()
					else:
						notification = Notifications(users = club.users, fixture = fix, player = player, player_status = str(ply.status), created_at = now(), updated_at = now())
						notification.save()
					if webToken is not None:
						web_noti_data = {}
						web_noti_data["player_name"] = player.player_name
						web_noti_data["player_status"] = str(ply.status)
						web_noti_data["fixture_name"] = fix.fixture
						web_noti_data["fixture_id"] = str(fix.id)
						web_noti_data["click_action"] = click_action
						web_noti_data["message_body"] = message_body
						push_notification_web(web_noti_data,webToken)
					
					if not_cnt == 0:
						GlobalDict['status'] = "1"
						GlobalDict['msg'] = "Your response has been sent successfully"
					else:
						for id in userTokens:
							user_token = UsersToken.objects.get(id = id)
							token_hex = user_token.device_token
							source_device = user_token.source_device
							usertype = user_token.usertype
							usertoken = user_token.id
							get_data = {}
							get_data["fixture_name"] = fix.fixture
							get_data["body"] = message_body
							get_data["click_action"] = "rv"
							get_data["tag"] = "review"
							get_data["tab"] = "rv"
							get_data["strip_color"] = hex_color
							get_data["club_name"] = club.club_name
							get_data["id"] = str(fix.id)
							get_data["kick_off"] = ""
							get_data["match_details"] = ""
							get_data["fixture_date"] = ""
							get_data["userType"] = usertype						
							get_data["usertoken"] = usertoken
							# if sound:
								# get_data["sound"] = sound
							# else:
								# get_data["sound"] = "Default"
							if source_device == "i":
								push_notification_ios_web(get_data,token_hex)
							else:
								push_notification_android_web(get_data,token_hex)
						GlobalDict['status'] = "1"
						GlobalDict['msg'] = "Your response has been sent successfully"
				else:
					GlobalDict['status'] = "0"
					GlobalDict['msg'] = "Please select valid response"
			else:
				GlobalDict['status'] = "0"
				GlobalDict['msg'] = "Invalid player id"
		else: 
				GlobalDict['status'] = "2"
				GlobalDict['msg'] = "App device token doesn't match...."
		# return HttpResponse(p)
		return JsonResponse(GlobalDict, safe = False)
		
		
@csrf_exempt
def playerResponseClub_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		fixture_id = data['fixture_id']
		squadList =[]
		# FixE= Squad.objects.filter(fixture_id = fixture_id, fixture_visibility = "t").exists()
		FixE = Fixtures.objects.filter(id = fixture_id, fixture_visibility = "t").exists()
		GlobalDict={}
		GlobalDict1={}
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			if FixE:
				Fix = Fixtures.objects.get(id = fixture_id, fixture_visibility = "t")
				if str(Fix.fixture_confirm) == "True":
					GlobalDict1['is_confirm'] = "1"
				else:
					GlobalDict1['is_confirm'] = "0"
				squad = Squad.objects.filter(fixture = Fix)
				
				for rec in squad:
					player = Players.objects.get(id = rec.player_id)
					userstokenE = UsersToken.objects.filter(player = player).exists()
					if player.player_img:
						prof_pic = 'pic_folder/'+str(player.player_img)
					else:
						prof_pic = defaultPlayer
					GlobalDict['player_id']= str(player.id)
					GlobalDict['player_img']= prof_pic
					GlobalDict['player_name']= player.player_name
					GlobalDict['player_position_id']= str(player.player_position_id)
					GlobalDict['player_position']= str(player.player_position).capitalize()
					# GlobalDict['player_status']= str(rec.status) if userstokenE else "4"
					if rec.status_id not in [1,2]:
						if userstokenE:
							GlobalDict['player_status']	= str(rec.status)
						else:
							GlobalDict['player_status']	= "4"
					else:
						GlobalDict['player_status'] = str(rec.status)
					squadList.append(GlobalDict)
					GlobalDict ={}
					
				GlobalDict1['resend'] = str(Fix.resend)
				GlobalDict1['status'] = "1"
				GlobalDict1['msg'] = "Squad"
				GlobalDict1['squad'] =squadList
				return JsonResponse(GlobalDict1,safe=False)
			else:
				GlobalDict['player_id']= ""
				GlobalDict['player_img']= ""
				GlobalDict['player_name']= ""
				GlobalDict['player_position_id']= ""
				GlobalDict['player_position']= ""
				GlobalDict['player_status']= ""
				squadList.append(GlobalDict)
				GlobalDict = {}
				GlobalDict['resend'] = "False"
				GlobalDict['status'] = "0"
				GlobalDict['msg'] =  "No squad for this fixture"
				GlobalDict['squad'] = squadList
				return JsonResponse(GlobalDict,safe=False)
		else:
			GlobalDict['player_id']= ""
			GlobalDict['player_img']= ""
			GlobalDict['player_name']= ""
			GlobalDict['player_position_id']= ""
			GlobalDict['player_position']= ""
			GlobalDict['player_status']= ""
			squadList.append(GlobalDict)
			GlobalDict = {}
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
			GlobalDict['squad'] = squadList
			return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def resendMsg_web(request):
	if request.method == "POST" :
		# get_data = {}
		data = json.loads(request.body)
		GlobalDict = {}
		resendData = []
		fixture_id = data['fixture_id']
		player = data['player_id']
		app_device_token = data['app_device_token']
		userType = data['user_type']
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			for pl in player:
				usertokenE= UsersToken.objects.filter(player_id = pl).exists()
				if  usertokenE:
					sqd = Squad.objects.get(player_id = pl, fixture_id = fixture_id, hide_fix = "f")
					sqd.resend = True
					sqd.save()
					Fix = Fixtures.objects.get(id = fixture_id, fixture_visibility = "t")
					Fix.resend = True
					Fix.resend_at = str(datetime.datetime.now())
					Fix.save()
					resendData.append("Resend Status - "+str(Fix.resend)+" Resend At - "+str(Fix.resend_at))
					usertoken = UsersToken.objects.get(player_id = pl)
					# sound = usertoken.sound
					source = usertoken.source_device
					token_hex = usertoken.device_token
					club = Clubs.objects.get(id = Fix.club_id)
					rgb_color = str(club.strip_colour)
					if rgb_color:
						hex_color = rgb2hex(rgb_color)
					else:
						hex_color = str("#939496")
						
					if token_hex:
						kick_off = str(datetime.datetime.strftime( datetime.datetime.strptime(str(Fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))+" @ "+str(datetime.datetime.strftime( datetime.datetime.strptime(str(Fix.fixture_time), "%H:%M:%S"), "%I:%M %p") )
						get_data = {}
						get_data["id"] = str(Fix.id)
						get_data["userType"] = usertoken.usertype
						get_data["fixture_name"] = Fix.fixture
						get_data["body"] = "Please confirm your availability for "+Fix.fixture
						get_data["kick_off"] = kick_off
						get_data["fixture_date"] = str(datetime.datetime.strftime( datetime.datetime.strptime(str(Fix.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
						get_data["match_details"] = Fix.fixture_venue
						get_data["click_action"] = "av"
						get_data["tag"] = "availablity"
						get_data["tab"] = "av"
						get_data["strip_color"] = hex_color
						get_data["club_name"] = club.club_name
						get_data["usertoken"] = usertoken.id
						
						# if sound:
							# get_data["sound"] = sound
						# else:
							# get_data["sound"] = "Default"
						if source == "i":
							data1 = push_notification_ios_web(get_data,token_hex)
						else :
							data1 = push_notification_android_web(get_data,token_hex)
						
			GlobalDict['status'] = "1"
			GlobalDict['resend_status'] = resendData
			GlobalDict['msg'] =  "Notification resent to players who haven’t responded"
			return JsonResponse(GlobalDict, safe= False)
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
			return JsonResponse(GlobalDict, safe= False)
			
@csrf_exempt
def notificationTone_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usertype = data['user_type']
		source = data['source']
		club_admin_id = data['club_admin_id']
		manager_id = data['manager_id']
		player_id = data['player_id']
		app_device_token = data['app_device_token']
		notification_tone = data['notification_tone']
		userE = UsersToken.objects.filter(app_device_token = app_device_token, usertype = usertype, source_device = source).exists()
		GlobalDict = {}
		if userE:
			usersData = UsersToken.objects.get(app_device_token = app_device_token, usertype = usertype, source_device = source)
			usersData.users_id = club_admin_id
			usersData.manager_id = manager_id
			usersData.player_id = player_id
			usersData.notification_tone = notification_tone
			usersData.save()
			GlobalDict['status'] = "1"
			GlobalDict['msg'] =  "Notification tone saved"
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe= False)
		

@csrf_exempt
def logOut_web(request):
	if request.method == "POST" :
		data = json.loads(request.body)
		usertype = data['user_type']
		source = data['source']
		club_admin_id = data['club_admin_id']
		manager_id = data['manager_id']
		player_id = data['player_id']

		if club_admin_id != "":
			userE = UsersToken.objects.filter(users_id = club_admin_id, usertype = usertype, source_device = source).exists()
			if userE:
				userstoken = UsersToken.objects.filter(usertype = usertype, source_device = source)
				for usertoken in userstoken:
					# usertoken.device_token = ""
					# usertoken.app_device_token = ""
					# usertoken.is_notification_block = 0
					usertoken.delete()

		if manager_id != "":
			userE = UsersToken.objects.filter(manager_id = manager_id, usertype = usertype, source_device = source).exists()
			if userE:
				userstoken = UsersToken.objects.filter(usertype = usertype, source_device = source)
				for usertoken in userstoken:
					# usertoken.device_token = ""
					# usertoken.app_device_token = ""
					# usertoken.is_notification_block = 0
					usertoken.delete()

		if player_id != "":
			userE = UsersToken.objects.filter(player_id = player_id,usertype = usertype, source_device = source).exists()
			if userE:
				userstoken = UsersToken.objects.filter(usertype = usertype, source_device = source)
				for usertoken in userstoken:
					# usertoken.device_token = "" 
					# usertoken.app_device_token = ""
					# usertoken.is_notification_block = 0
					usertoken.delete()

	return JsonResponse({"msg":"Loogged out..."}, safe= False)

@csrf_exempt
def reset_badge_count_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usertype = data['user_type']
		source = data['source']
		app_device_token = data['app_device_token']
		if 'badge_count' in data:
			badge_count = data['badge_count']
		else:
			badge_count = None
		
		if 'availability_count' in data:
			av_count = data['availability_count']
		else:
			av_count = None
		
		if 'review_count' in data:
			rv_count = data['review_count']
		else:
			rv_count = None
		
		userE = UsersToken.objects.filter(app_device_token = app_device_token, usertype = usertype, source_device = source).exists()
		GlobalDict = {}
		if userE:
			msg = ""
			usersData = UsersToken.objects.get(app_device_token = app_device_token, usertype = usertype, source_device = source)
			if badge_count:
				usersData.badge_count = badge_count
				msg =  "Badge count "+str(badge_count)+" saved"

			if av_count:
				usersData.av_count = av_count
				msg =  "Availability count "+str(av_count)+" saved"

			if rv_count:
				usersData.rv_count = rv_count
				msg =  "Review count "+str(rv_count)+" saved"
			
			usersData.save()
			availability_count = usersData.av_count
			review_count = usersData.rv_count
			
			GlobalDict['status'] = "1"
			GlobalDict['availability_count'] = int(availability_count)
			GlobalDict['review_count'] = int(review_count)
			GlobalDict['msg'] = msg
		else:
			GlobalDict['status'] = "2"
			GlobalDict['availability_count'] = 0
			GlobalDict['review_count'] = 0
			GlobalDict['msg'] =  "App device token doesn't match...."
		return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def final_squad_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usertype = data['user_type']
		source = data['source']
		app_device_token = data['app_device_token']
		fixture_id = data['fixture_id']
		players = data['players_id']
		userE = UsersToken.objects.filter(app_device_token = app_device_token, usertype = usertype, source_device = source).exists()
		GlobalDict = {}
		
		fix_data = Fixtures.objects.get(id = fixture_id, fixture_visibility = "t")
		fix_data.fixture_confirm = True
		fix_data.resend_at = str(datetime.datetime.now())
		fix_data.save()
		for pl in players:
			sqd = Squad.objects.get(player_id = pl, fixture_id = fix_data)
			sqd.squad_confirm = True
			sqd.save()
			
		if userE:
			squad_data = Squad.objects.filter(fixture_id = fixture_id, status_id = 1)
			for data in squad_data:
				usertokenE = UsersToken.objects.filter(player = data.player_id).exists()
				if usertokenE:
					usertoken = UsersToken.objects.get(player = data.player_id)
					source = usertoken.source_device
					token_hex = usertoken.device_token
					club = Clubs.objects.get(id = fix_data.club_id)
					rgb_color = str(club.strip_colour)
					if rgb_color:
						hex_color = rgb2hex(rgb_color)
					else:
						hex_color = str("#939496")
					if token_hex:
						fixture_date = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix_data.fixture_date), "%Y-%m-%d") , "%d.%m.%Y"))
						fixture_time = str(datetime.datetime.strftime( datetime.datetime.strptime(str(fix_data.fixture_time), "%H:%M:%S"), "%I:%M %p") )
						kick_off = fixture_date +" @ "+ fixture_time
						get_data = {}
						get_data["id"] = str(fix_data.id)
						get_data["userType"] = usertoken.usertype
						get_data["fixture_name"] = fix_data.fixture
						get_data["body"] = "Confirmed Players for " + fix_data.fixture
						get_data["kick_off"] = kick_off
						get_data["fixture_date"] = fixture_date
						# get_data["fixture_time"] = fixture_time
						get_data["match_details"] = fix_data.fixture_venue
						get_data["click_action"] = "av"
						get_data["tag"] = "availablity"
						get_data["tab"] = "av"
						get_data["strip_color"] = hex_color
						get_data["club_name"] = club.club_name
						get_data["usertoken"] = usertoken.id
						if str(data.player_id) in players:
							get_data["is_confirm"] = "1"
						else:
							get_data["is_confirm"] = "0"
						if source == "i":
							data1 = push_notification_ios_web(get_data,token_hex)
						else :
							data1 = push_notification_android_web(get_data,token_hex)
			GlobalDict['status'] = "1"
			GlobalDict['msg'] =  "Match day squad has been confirmed"
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
		
		return JsonResponse(GlobalDict, safe= False)

@csrf_exempt
def get_final_players_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usertype = data['user_type']
		app_device_token = data['app_device_token']
		fixture_id = data['fixture_id']
		userE = UsersToken.objects.filter(app_device_token = app_device_token, usertype = usertype).exists()
		if userE:
			squad_data = Squad.objects.filter(fixture_id = fixture_id, squad_confirm = 't')
			GlobalDict = {}
			PlayersList = []
			PlayerDict = {}
			for player in squad_data:
				player_info = Players.objects.get(id = player.player_id)
				PlayerDict["player_id"] = str(player_info.id)
				# PlayerDict["player_name"] = str(player_info.player_name)
				PlayerDict["player_name"] = player_info.player_name
				if player_info.player_img:
					player_img = player_info.player_img.url
				else:
					player_img = defaultPlayer
				PlayerDict["player_img"] = str(player_img)
				PlayerDict['player_position_id'] =  str(player_info.player_position_id)
				PlayerDict['player_position'] =  str(player_info.player_position).capitalize()
				PlayersList.append(PlayerDict)
				PlayerDict = {}
			GlobalDict["player_list"] = PlayersList
			GlobalDict['status'] = "1"
			GlobalDict['msg'] =  "Confirmed players"
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
		
		return JsonResponse(GlobalDict, safe= False)

@csrf_exempt	
def push_notification_ios_web(get_data,token_hex):
	if token_hex:
		if "usertoken" in get_data:
			if get_data["usertoken"] != "":
				usertoken = UsersToken.objects.get(id = get_data["usertoken"])
				tab = get_data["tab"]
				if tab == "av":
					av_count = usertoken.av_count+1
					usertoken.av_count = av_count
				if tab == "rv":
					rv_count = usertoken.rv_count+1
					usertoken.rv_count = rv_count
				badge = usertoken.badge_count+1
				usertoken.badge_count = badge
				usertoken.save()
				is_notification_block = usertoken.is_notification_block
			else:
				badge = 0
				is_notification_block = 0
		else:
			badge = 0
			is_notification_block = 0
		# badge = ""
		if is_notification_block == 0:
			alert = {}
			sound = "default"
			if 'is_confirm' not in get_data:
				get_data['is_confirm'] = "0"
			alert = get_data
			identifier = random.getrandbits(32)
			payload = Payload(alert=alert,sound=sound, badge = badge)
			# payload = Payload(alert=alert,sound=sound)
			
			# apns_enhanced_p = APNs(use_sandbox=False, cert_file='/local-teams/LocalTeams/LocalTeamsApp/static/cert/localteams_prod.pem', enhanced=True)
			apns_enhanced_p = APNs(use_sandbox=False, cert_file='/local-teams/LocalTeams/LocalTeamsApp/static/cert/LocalTeams_Prod.pem', enhanced=True)
			apns_enhanced_p.gateway_server.send_notification(token_hex, payload, identifier=identifier)
			apns_enhanced_p.gateway_server.force_close()
			print apns_enhanced_p.gateway_server
			
			# apns_enhanced = APNs(use_sandbox=True, cert_file='/local-teams/LocalTeams/LocalTeamsApp/static/cert/localteams_dev.pem', enhanced=True)
			apns_enhanced = APNs(use_sandbox=True, cert_file='/local-teams/LocalTeams/LocalTeamsApp/static/cert/LocalTeams_Dev.pem', enhanced=True)
			apns_enhanced.gateway_server.send_notification(token_hex, payload, identifier=identifier)
			apns_enhanced.gateway_server.force_close()
			
			print apns_enhanced.gateway_server
		return True
	else:
		return False
	
@csrf_exempt		
def push_notification_android_web(get_data,token_hex):
	api_key=settings.FCM_API_KEY
	response = {}
	if token_hex:
		if "usertoken" in get_data:
			if get_data["usertoken"] != "":
				usertoken = UsersToken.objects.get(id = get_data["usertoken"])
				is_notification_block = usertoken.is_notification_block
			else:
				is_notification_block = 0
		else:
			is_notification_block = 0
			
		if is_notification_block == 0:
			push_service = FCMNotification(api_key = api_key )
			if 'is_confirm' in get_data:
				data_message = { "club_name":get_data["club_name"],"strip_color":get_data["strip_color"],"tab":get_data["tab"],"fixture_id":get_data["id"],"kick_off":get_data["kick_off"],"match_details":get_data["match_details"],"fixture_name":get_data["fixture_name"],"fixture_date":get_data["fixture_date"], "is_confirm":get_data["is_confirm"] }
			else:
				data_message = { "club_name":get_data["club_name"],"strip_color":get_data["strip_color"],"tab":get_data["tab"],"fixture_id":get_data["id"],"kick_off":get_data["kick_off"],"match_details":get_data["match_details"],"fixture_name":get_data["fixture_name"],"fixture_date":get_data["fixture_date"], "is_confirm":"0"  }
			
				# registration_id = token_hex,
			result = push_service.notify_single_device(
				registration_id = token_hex,
				message_body = get_data["body"],
				message_title = "LocalTeams",
				message_icon = "ic_local_teams_notification",
				sound = "Default",
				data_message = data_message,
				click_action = get_data["click_action"],
				color = "#4ed71b",
				tag = str(get_data['tag']))
			# result = push_service.notify_single_device(registration_id=token_hex, message_body=message_body)
			print result
			response["status"] = result["success"]
		else:
			response["status"] = True
	else:
		response["status"] = False
	return response

@csrf_exempt		
def push_notification_web(get_data,token_hex):
	api_key=settings.FCM_API_KEY
	push_service = FCMNotification(api_key = api_key )
	title = "Local Teams"
	icon = "https://local-teams.com/static/images/icon/favicon.ico"
	result = push_service.notify_single_device(registration_id=token_hex, message_body=get_data["message_body"], message_title=title, message_icon=icon, data_message = get_data, click_action = get_data["click_action"])
	return result

@csrf_exempt
def reset_device_token_web(request):
	if request.method == "POST":
		data = json.loads(request.body)
		userType = data['user_type']
		app_device_token = data['app_device_token']
		token = data['token']
		GlobalDict = {}
		useExists = UsersToken.objects.filter(app_device_token =app_device_token,usertype = userType).exists()
		if useExists:
			usertoken = UsersToken.objects.get(app_device_token =app_device_token,usertype = userType)
			usertoken.device_token = token
			usertoken.save()
			GlobalDict['status'] = "1"
			GlobalDict['msg'] =  "Device token updated successfully..."
		else:
			GlobalDict['status'] = "2"
			GlobalDict['msg'] =  "App device token doesn't match...."
			
		return JsonResponse(GlobalDict, safe= False)