import datetime
from django import template
# from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='noti_date')
def noti_date(value):
	return str(datetime.datetime.strftime(datetime.datetime.strptime(str(value), "%Y-%m-%d"), "%d.%m.%Y"))
	
@register.filter(name='capfirst')
def capfirst(value):
	return str(value[0].upper())+str(value[1:].lower())
	
@register.filter(name='splitOnVs')
def splitOnVs(value,arg):
	try:
		if arg == 0:
			return value.split(' vs ')[0].strip()
		elif arg == 1:
			return value.split(' vs ')[-1].strip()
		else:
			return value
	except IndexError:
		return value
	except:
		return value